/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	This file was auto-generated to contain all exported functions in The Forge.
	All functions containing 'API_INTERFACE' and are located in the 'Renderer' directory
	are added automatically upon compilation.

	Script is located at TheForge/Tools/AutoGenerateRendererAPI.py

	Note : All manual changes to these files will be overwritten
	If a function that you want is missing, please make sure the header file in which it is
	declared is located in the 'Renderer' directory and that you re-build The Forge.
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "IRenderer.h"
#include "../../Common_3/OS/Interfaces/ILogManager.h"

void(*addBuffer)(Renderer* pRenderer, const BufferDesc* pDesc, Buffer** pp_buffer);
namespace d3d12 { extern void addBuffer(Renderer* pRenderer, const BufferDesc* pDesc, Buffer** pp_buffer); } 
namespace vk { extern void addBuffer(Renderer* pRenderer, const BufferDesc* pDesc, Buffer** pp_buffer); } 
namespace mtl { extern void addBuffer(Renderer* pRenderer, const BufferDesc* pDesc, Buffer** pp_buffer); } 

void(*removeBuffer)(Renderer* pRenderer, Buffer* pBuffer);
namespace d3d12 { extern void removeBuffer(Renderer* pRenderer, Buffer* pBuffer); } 
namespace vk { extern void removeBuffer(Renderer* pRenderer, Buffer* pBuffer); } 
namespace mtl { extern void removeBuffer(Renderer* pRenderer, Buffer* pBuffer); } 

void(*addTexture)(Renderer* pRenderer, const TextureDesc* pDesc, Texture** ppTexture);
namespace d3d12 { extern void addTexture(Renderer* pRenderer, const TextureDesc* pDesc, Texture** ppTexture); } 
namespace vk { extern void addTexture(Renderer* pRenderer, const TextureDesc* pDesc, Texture** ppTexture); } 
namespace mtl { extern void addTexture(Renderer* pRenderer, const TextureDesc* pDesc, Texture** ppTexture); } 

void(*removeTexture)(Renderer* pRenderer, Texture* pTexture);
namespace d3d12 { extern void removeTexture(Renderer* pRenderer, Texture* pTexture); } 
namespace vk { extern void removeTexture(Renderer* pRenderer, Texture* pTexture); } 
namespace mtl { extern void removeTexture(Renderer* pRenderer, Texture* pTexture); } 

void(*mapBuffer)(Renderer* pRenderer, Buffer* pBuffer, ReadRange* pRange);
namespace d3d12 { extern void mapBuffer(Renderer* pRenderer, Buffer* pBuffer, ReadRange* pRange); } 
namespace vk { extern void mapBuffer(Renderer* pRenderer, Buffer* pBuffer, ReadRange* pRange); } 
namespace mtl { extern void mapBuffer(Renderer* pRenderer, Buffer* pBuffer, ReadRange* pRange); } 

void(*unmapBuffer)(Renderer* pRenderer, Buffer* pBuffer);
namespace d3d12 { extern void unmapBuffer(Renderer* pRenderer, Buffer* pBuffer); } 
namespace vk { extern void unmapBuffer(Renderer* pRenderer, Buffer* pBuffer); } 
namespace mtl { extern void unmapBuffer(Renderer* pRenderer, Buffer* pBuffer); } 

void(*cmdUpdateBuffer)(Cmd* pCmd, uint64_t srcOffset, uint64_t dstOffset, uint64_t size, Buffer* pSrcBuffer, Buffer* pBuffer);
namespace d3d12 { extern void cmdUpdateBuffer(Cmd* pCmd, uint64_t srcOffset, uint64_t dstOffset, uint64_t size, Buffer* pSrcBuffer, Buffer* pBuffer); } 
namespace vk { extern void cmdUpdateBuffer(Cmd* pCmd, uint64_t srcOffset, uint64_t dstOffset, uint64_t size, Buffer* pSrcBuffer, Buffer* pBuffer); } 
namespace mtl { extern void cmdUpdateBuffer(Cmd* pCmd, uint64_t srcOffset, uint64_t dstOffset, uint64_t size, Buffer* pSrcBuffer, Buffer* pBuffer); } 

void(*cmdUpdateSubresources)(Cmd* pCmd, uint32_t numSubresources, SubresourceDataDesc* pSubresources, Buffer* pIntermediate, Texture* pTexture);
namespace d3d12 { extern void cmdUpdateSubresources(Cmd* pCmd, uint32_t numSubresources, SubresourceDataDesc* pSubresources, Buffer* pIntermediate, Texture* pTexture); } 
namespace vk { extern void cmdUpdateSubresources(Cmd* pCmd, uint32_t numSubresources, SubresourceDataDesc* pSubresources, Buffer* pIntermediate, Texture* pTexture); } 
namespace mtl { extern void cmdUpdateSubresources(Cmd* pCmd, uint32_t numSubresources, SubresourceDataDesc* pSubresources, Buffer* pIntermediate, Texture* pTexture); } 

void(*compileShader)(Renderer* pRenderer, ShaderTarget target, ShaderStage stage, const char* fileName, uint32_t codeSize, const char* code, uint32_t macroCount, ShaderMacro* pMacros, void* (*allocator)(size_t a), uint32_t* pByteCodeSize, char** ppByteCode, const char* pEntryPoint);
namespace d3d12 { extern void compileShader(Renderer* pRenderer, ShaderTarget target, ShaderStage stage, const char* fileName, uint32_t codeSize, const char* code, uint32_t macroCount, ShaderMacro* pMacros, void* (*allocator)(size_t a), uint32_t* pByteCodeSize, char** ppByteCode, const char* pEntryPoint); } 

const RendererShaderDefinesDesc(*get_renderer_shaderdefines)(Renderer* pRenderer);
namespace d3d12 { extern const RendererShaderDefinesDesc get_renderer_shaderdefines(Renderer* pRenderer); } 
namespace vk { extern const RendererShaderDefinesDesc get_renderer_shaderdefines(Renderer* pRenderer); } 
namespace mtl { extern const RendererShaderDefinesDesc get_renderer_shaderdefines(Renderer* pRenderer); } 

void(*initRenderer)(const char* app_name, const RendererDesc* p_settings, Renderer** ppRenderer);
namespace d3d12 { extern void initRenderer(const char* app_name, const RendererDesc* p_settings, Renderer** ppRenderer); } 
namespace vk { extern void initRenderer(const char* app_name, const RendererDesc* p_settings, Renderer** ppRenderer); } 
namespace mtl { extern void initRenderer(const char* app_name, const RendererDesc* p_settings, Renderer** ppRenderer); } 

void(*removeRenderer)(Renderer* pRenderer);
namespace d3d12 { extern void removeRenderer(Renderer* pRenderer); } 
namespace vk { extern void removeRenderer(Renderer* pRenderer); } 
namespace mtl { extern void removeRenderer(Renderer* pRenderer); } 

void(*addFence)(Renderer* pRenderer, Fence** pp_fence);
namespace d3d12 { extern void addFence(Renderer* pRenderer, Fence** pp_fence); } 
namespace vk { extern void addFence(Renderer* pRenderer, Fence** pp_fence); } 
namespace mtl { extern void addFence(Renderer* pRenderer, Fence** pp_fence); } 

void(*removeFence)(Renderer* pRenderer, Fence* p_fence);
namespace d3d12 { extern void removeFence(Renderer* pRenderer, Fence* p_fence); } 
namespace vk { extern void removeFence(Renderer* pRenderer, Fence* p_fence); } 
namespace mtl { extern void removeFence(Renderer* pRenderer, Fence* p_fence); } 

void(*addSemaphore)(Renderer* pRenderer, Semaphore** pp_semaphore);
namespace d3d12 { extern void addSemaphore(Renderer* pRenderer, Semaphore** pp_semaphore); } 
namespace vk { extern void addSemaphore(Renderer* pRenderer, Semaphore** pp_semaphore); } 
namespace mtl { extern void addSemaphore(Renderer* pRenderer, Semaphore** pp_semaphore); } 

void(*removeSemaphore)(Renderer* pRenderer, Semaphore* p_semaphore);
namespace d3d12 { extern void removeSemaphore(Renderer* pRenderer, Semaphore* p_semaphore); } 
namespace vk { extern void removeSemaphore(Renderer* pRenderer, Semaphore* p_semaphore); } 
namespace mtl { extern void removeSemaphore(Renderer* pRenderer, Semaphore* p_semaphore); } 

void(*addQueue)(Renderer* pRenderer, QueueDesc* pQDesc, Queue** ppQueue);
namespace d3d12 { extern void addQueue(Renderer* pRenderer, QueueDesc* pQDesc, Queue** ppQueue); } 
namespace vk { extern void addQueue(Renderer* pRenderer, QueueDesc* pQDesc, Queue** ppQueue); } 
namespace mtl { extern void addQueue(Renderer* pRenderer, QueueDesc* pQDesc, Queue** ppQueue); } 

void(*removeQueue)(Queue* pQueue);
namespace d3d12 { extern void removeQueue(Queue* pQueue); } 
namespace vk { extern void removeQueue(Queue* pQueue); } 
namespace mtl { extern void removeQueue(Queue* pQueue); } 

void(*addSwapChain)(Renderer* pRenderer, const SwapChainDesc* p_desc, SwapChain** pp_swap_chain);
namespace d3d12 { extern void addSwapChain(Renderer* pRenderer, const SwapChainDesc* p_desc, SwapChain** pp_swap_chain); } 
namespace vk { extern void addSwapChain(Renderer* pRenderer, const SwapChainDesc* p_desc, SwapChain** pp_swap_chain); } 
namespace mtl { extern void addSwapChain(Renderer* pRenderer, const SwapChainDesc* p_desc, SwapChain** pp_swap_chain); } 

void(*removeSwapChain)(Renderer* pRenderer, SwapChain* p_swap_chain);
namespace d3d12 { extern void removeSwapChain(Renderer* pRenderer, SwapChain* p_swap_chain); } 
namespace vk { extern void removeSwapChain(Renderer* pRenderer, SwapChain* p_swap_chain); } 
namespace mtl { extern void removeSwapChain(Renderer* pRenderer, SwapChain* p_swap_chain); } 

void(*addCmdPool)(Renderer* pRenderer, Queue* p_queue, bool transient, CmdPool** pp_CmdPool);
namespace d3d12 { extern void addCmdPool(Renderer* pRenderer, Queue* p_queue, bool transient, CmdPool** pp_CmdPool); } 
namespace vk { extern void addCmdPool(Renderer* pRenderer, Queue* p_queue, bool transient, CmdPool** pp_CmdPool); } 
namespace mtl { extern void addCmdPool(Renderer* pRenderer, Queue* p_queue, bool transient, CmdPool** pp_CmdPool); } 

void(*removeCmdPool)(Renderer* pRenderer, CmdPool* p_CmdPool);
namespace d3d12 { extern void removeCmdPool(Renderer* pRenderer, CmdPool* p_CmdPool); } 
namespace vk { extern void removeCmdPool(Renderer* pRenderer, CmdPool* p_CmdPool); } 
namespace mtl { extern void removeCmdPool(Renderer* pRenderer, CmdPool* p_CmdPool); } 

void(*addCmd)(CmdPool* p_CmdPool, bool secondary, Cmd** pp_cmd);
namespace d3d12 { extern void addCmd(CmdPool* p_CmdPool, bool secondary, Cmd** pp_cmd); } 
namespace vk { extern void addCmd(CmdPool* p_CmdPool, bool secondary, Cmd** pp_cmd); } 
namespace mtl { extern void addCmd(CmdPool* p_CmdPool, bool secondary, Cmd** pp_cmd); } 

void(*removeCmd)(CmdPool* p_CmdPool, Cmd* p_cmd);
namespace d3d12 { extern void removeCmd(CmdPool* p_CmdPool, Cmd* p_cmd); } 
namespace vk { extern void removeCmd(CmdPool* p_CmdPool, Cmd* p_cmd); } 
namespace mtl { extern void removeCmd(CmdPool* p_CmdPool, Cmd* p_cmd); } 

void(*addCmd_n)(CmdPool* p_CmdPool, bool secondary, uint32_t cmd_count, Cmd*** ppp_cmd);
namespace d3d12 { extern void addCmd_n(CmdPool* p_CmdPool, bool secondary, uint32_t cmd_count, Cmd*** ppp_cmd); } 
namespace vk { extern void addCmd_n(CmdPool* p_CmdPool, bool secondary, uint32_t cmd_count, Cmd*** ppp_cmd); } 
namespace mtl { extern void addCmd_n(CmdPool* p_CmdPool, bool secondary, uint32_t cmd_count, Cmd*** ppp_cmd); } 

void(*removeCmd_n)(CmdPool* p_CmdPool, uint32_t cmd_count, Cmd** pp_cmd);
namespace d3d12 { extern void removeCmd_n(CmdPool* p_CmdPool, uint32_t cmd_count, Cmd** pp_cmd); } 
namespace vk { extern void removeCmd_n(CmdPool* p_CmdPool, uint32_t cmd_count, Cmd** pp_cmd); } 
namespace mtl { extern void removeCmd_n(CmdPool* p_CmdPool, uint32_t cmd_count, Cmd** pp_cmd); } 

void(*addRenderTarget)(Renderer* pRenderer, const RenderTargetDesc* p_desc, RenderTarget** pp_render_target);
namespace d3d12 { extern void addRenderTarget(Renderer* pRenderer, const RenderTargetDesc* p_desc, RenderTarget** pp_render_target); } 
namespace vk { extern void addRenderTarget(Renderer* pRenderer, const RenderTargetDesc* p_desc, RenderTarget** pp_render_target); } 
namespace mtl { extern void addRenderTarget(Renderer* pRenderer, const RenderTargetDesc* p_desc, RenderTarget** pp_render_target); } 

void(*removeRenderTarget)(Renderer* pRenderer, RenderTarget* p_render_target);
namespace d3d12 { extern void removeRenderTarget(Renderer* pRenderer, RenderTarget* p_render_target); } 
namespace vk { extern void removeRenderTarget(Renderer* pRenderer, RenderTarget* p_render_target); } 
namespace mtl { extern void removeRenderTarget(Renderer* pRenderer, RenderTarget* p_render_target); } 

void(*addSampler)(Renderer* pRenderer, const SamplerDesc* pDesc, Sampler** pp_sampler);
namespace d3d12 { extern void addSampler(Renderer* pRenderer, const SamplerDesc* pDesc, Sampler** pp_sampler); } 
namespace vk { extern void addSampler(Renderer* pRenderer, const SamplerDesc* pDesc, Sampler** pp_sampler); } 
namespace mtl { extern void addSampler(Renderer* pRenderer, const SamplerDesc* pDesc, Sampler** pp_sampler); } 

void(*removeSampler)(Renderer* pRenderer, Sampler* p_sampler);
namespace d3d12 { extern void removeSampler(Renderer* pRenderer, Sampler* p_sampler); } 
namespace vk { extern void removeSampler(Renderer* pRenderer, Sampler* p_sampler); } 
namespace mtl { extern void removeSampler(Renderer* pRenderer, Sampler* p_sampler); } 

void(*addShaderBinary)(Renderer* pRenderer, const BinaryShaderDesc* p_desc, Shader** p_shader_program);
namespace d3d12 { extern void addShaderBinary(Renderer* pRenderer, const BinaryShaderDesc* p_desc, Shader** p_shader_program); } 
namespace vk { extern void addShaderBinary(Renderer* pRenderer, const BinaryShaderDesc* p_desc, Shader** p_shader_program); } 
namespace mtl { extern void addShaderBinary(Renderer* pRenderer, const BinaryShaderDesc* p_desc, Shader** p_shader_program); } 

void(*removeShader)(Renderer* pRenderer, Shader* p_shader_program);
namespace d3d12 { extern void removeShader(Renderer* pRenderer, Shader* p_shader_program); } 
namespace vk { extern void removeShader(Renderer* pRenderer, Shader* p_shader_program); } 
namespace mtl { extern void removeShader(Renderer* pRenderer, Shader* p_shader_program); } 

void(*addRootSignature)(Renderer* pRenderer, const RootSignatureDesc* pRootDesc, RootSignature** pp_root_signature);
namespace d3d12 { extern void addRootSignature(Renderer* pRenderer, const RootSignatureDesc* pRootDesc, RootSignature** pp_root_signature); } 
namespace vk { extern void addRootSignature(Renderer* pRenderer, const RootSignatureDesc* pRootDesc, RootSignature** pp_root_signature); } 
namespace mtl { extern void addRootSignature(Renderer* pRenderer, const RootSignatureDesc* pRootDesc, RootSignature** pp_root_signature); } 

void(*removeRootSignature)(Renderer* pRenderer, RootSignature* pRootSignature);
namespace d3d12 { extern void removeRootSignature(Renderer* pRenderer, RootSignature* pRootSignature); } 
namespace vk { extern void removeRootSignature(Renderer* pRenderer, RootSignature* pRootSignature); } 
namespace mtl { extern void removeRootSignature(Renderer* pRenderer, RootSignature* pRootSignature); } 

void(*addPipeline)(Renderer* pRenderer, const PipelineDesc* p_pipeline_settings, Pipeline** pp_pipeline);
namespace d3d12 { extern void addPipeline(Renderer* pRenderer, const PipelineDesc* p_pipeline_settings, Pipeline** pp_pipeline); } 
namespace vk { extern void addPipeline(Renderer* pRenderer, const PipelineDesc* p_pipeline_settings, Pipeline** pp_pipeline); } 
namespace mtl { extern void addPipeline(Renderer* pRenderer, const PipelineDesc* p_pipeline_settings, Pipeline** pp_pipeline); } 

void(*addComputePipeline)(Renderer* pRenderer, const ComputePipelineDesc* p_pipeline_settings, Pipeline** p_pipeline);
namespace d3d12 { extern void addComputePipeline(Renderer* pRenderer, const ComputePipelineDesc* p_pipeline_settings, Pipeline** p_pipeline); } 
namespace vk { extern void addComputePipeline(Renderer* pRenderer, const ComputePipelineDesc* p_pipeline_settings, Pipeline** p_pipeline); } 
namespace mtl { extern void addComputePipeline(Renderer* pRenderer, const ComputePipelineDesc* p_pipeline_settings, Pipeline** p_pipeline); } 

void(*removePipeline)(Renderer* pRenderer, Pipeline* p_pipeline);
namespace d3d12 { extern void removePipeline(Renderer* pRenderer, Pipeline* p_pipeline); } 
namespace vk { extern void removePipeline(Renderer* pRenderer, Pipeline* p_pipeline); } 
namespace mtl { extern void removePipeline(Renderer* pRenderer, Pipeline* p_pipeline); } 

void(*addDescriptorBinder)(Renderer* pRenderer, const DescriptorBinderDesc* p_desc, DescriptorBinder** pp_descriptor_binder);
namespace d3d12 { extern void addDescriptorBinder(Renderer* pRenderer, const DescriptorBinderDesc* p_desc, DescriptorBinder** pp_descriptor_binder); } 
namespace vk { extern void addDescriptorBinder(Renderer* pRenderer, const DescriptorBinderDesc* p_desc, DescriptorBinder** pp_descriptor_binder); } 
namespace mtl { extern void addDescriptorBinder(Renderer* pRenderer, const DescriptorBinderDesc* p_desc, DescriptorBinder** pp_descriptor_binder); } 

void(*removeDescriptorBinder)(Renderer* pRenderer, DescriptorBinder* p_descriptor_binder);
namespace d3d12 { extern void removeDescriptorBinder(Renderer* pRenderer, DescriptorBinder* p_descriptor_binder); } 
namespace vk { extern void removeDescriptorBinder(Renderer* pRenderer, DescriptorBinder* p_descriptor_binder); } 
namespace mtl { extern void removeDescriptorBinder(Renderer* pRenderer, DescriptorBinder* p_descriptor_binder); } 

void(*addBlendState)(Renderer* pRenderer, const BlendStateDesc* pDesc, BlendState** ppBlendState);
namespace d3d12 { extern void addBlendState(Renderer* pRenderer, const BlendStateDesc* pDesc, BlendState** ppBlendState); } 
namespace vk { extern void addBlendState(Renderer* pRenderer, const BlendStateDesc* pDesc, BlendState** ppBlendState); } 
namespace mtl { extern void addBlendState(Renderer* pRenderer, const BlendStateDesc* pDesc, BlendState** ppBlendState); } 

void(*removeBlendState)(BlendState* pBlendState);
namespace d3d12 { extern void removeBlendState(BlendState* pBlendState); } 
namespace vk { extern void removeBlendState(BlendState* pBlendState); } 
namespace mtl { extern void removeBlendState(BlendState* pBlendState); } 

void(*addDepthState)(Renderer* pRenderer, const DepthStateDesc* pDesc, DepthState** ppDepthState);
namespace d3d12 { extern void addDepthState(Renderer* pRenderer, const DepthStateDesc* pDesc, DepthState** ppDepthState); } 
namespace vk { extern void addDepthState(Renderer* pRenderer, const DepthStateDesc* pDesc, DepthState** ppDepthState); } 
namespace mtl { extern void addDepthState(Renderer* pRenderer, const DepthStateDesc* pDesc, DepthState** ppDepthState); } 

void(*removeDepthState)(DepthState* pDepthState);
namespace d3d12 { extern void removeDepthState(DepthState* pDepthState); } 
namespace vk { extern void removeDepthState(DepthState* pDepthState); } 
namespace mtl { extern void removeDepthState(DepthState* pDepthState); } 

void(*addRasterizerState)(Renderer* pRenderer, const RasterizerStateDesc* pDesc, RasterizerState** ppRasterizerState);
namespace d3d12 { extern void addRasterizerState(Renderer* pRenderer, const RasterizerStateDesc* pDesc, RasterizerState** ppRasterizerState); } 
namespace vk { extern void addRasterizerState(Renderer* pRenderer, const RasterizerStateDesc* pDesc, RasterizerState** ppRasterizerState); } 
namespace mtl { extern void addRasterizerState(Renderer* pRenderer, const RasterizerStateDesc* pDesc, RasterizerState** ppRasterizerState); } 

void(*removeRasterizerState)(RasterizerState* pRasterizerState);
namespace d3d12 { extern void removeRasterizerState(RasterizerState* pRasterizerState); } 
namespace vk { extern void removeRasterizerState(RasterizerState* pRasterizerState); } 
namespace mtl { extern void removeRasterizerState(RasterizerState* pRasterizerState); } 

void(*beginCmd)(Cmd* p_cmd);
namespace d3d12 { extern void beginCmd(Cmd* p_cmd); } 
namespace vk { extern void beginCmd(Cmd* p_cmd); } 
namespace mtl { extern void beginCmd(Cmd* p_cmd); } 

void(*endCmd)(Cmd* p_cmd);
namespace d3d12 { extern void endCmd(Cmd* p_cmd); } 
namespace vk { extern void endCmd(Cmd* p_cmd); } 
namespace mtl { extern void endCmd(Cmd* p_cmd); } 

void(*cmdBindRenderTargets)(Cmd* p_cmd, uint32_t render_target_count, RenderTarget** pp_render_targets, RenderTarget* p_depth_stencil, const LoadActionsDesc* loadActions, uint32_t* pColorArraySlices, uint32_t* pColorMipSlices, uint32_t depthArraySlice, uint32_t depthMipSlice);
namespace d3d12 { extern void cmdBindRenderTargets(Cmd* p_cmd, uint32_t render_target_count, RenderTarget** pp_render_targets, RenderTarget* p_depth_stencil, const LoadActionsDesc* loadActions, uint32_t* pColorArraySlices, uint32_t* pColorMipSlices, uint32_t depthArraySlice, uint32_t depthMipSlice); } 
namespace vk { extern void cmdBindRenderTargets(Cmd* p_cmd, uint32_t render_target_count, RenderTarget** pp_render_targets, RenderTarget* p_depth_stencil, const LoadActionsDesc* loadActions, uint32_t* pColorArraySlices, uint32_t* pColorMipSlices, uint32_t depthArraySlice, uint32_t depthMipSlice); } 
namespace mtl { extern void cmdBindRenderTargets(Cmd* p_cmd, uint32_t render_target_count, RenderTarget** pp_render_targets, RenderTarget* p_depth_stencil, const LoadActionsDesc* loadActions, uint32_t* pColorArraySlices, uint32_t* pColorMipSlices, uint32_t depthArraySlice, uint32_t depthMipSlice); } 

void(*cmdSetViewport)(Cmd* p_cmd, float x, float y, float width, float height, float min_depth, float max_depth);
namespace d3d12 { extern void cmdSetViewport(Cmd* p_cmd, float x, float y, float width, float height, float min_depth, float max_depth); } 
namespace vk { extern void cmdSetViewport(Cmd* p_cmd, float x, float y, float width, float height, float min_depth, float max_depth); } 
namespace mtl { extern void cmdSetViewport(Cmd* p_cmd, float x, float y, float width, float height, float min_depth, float max_depth); } 

void(*cmdSetScissor)(Cmd* p_cmd, uint32_t x, uint32_t y, uint32_t width, uint32_t height);
namespace d3d12 { extern void cmdSetScissor(Cmd* p_cmd, uint32_t x, uint32_t y, uint32_t width, uint32_t height); } 
namespace vk { extern void cmdSetScissor(Cmd* p_cmd, uint32_t x, uint32_t y, uint32_t width, uint32_t height); } 
namespace mtl { extern void cmdSetScissor(Cmd* p_cmd, uint32_t x, uint32_t y, uint32_t width, uint32_t height); } 

void(*cmdBindPipeline)(Cmd* p_cmd, Pipeline* p_pipeline);
namespace d3d12 { extern void cmdBindPipeline(Cmd* p_cmd, Pipeline* p_pipeline); } 
namespace vk { extern void cmdBindPipeline(Cmd* p_cmd, Pipeline* p_pipeline); } 
namespace mtl { extern void cmdBindPipeline(Cmd* p_cmd, Pipeline* p_pipeline); } 

void(*cmdBindDescriptors)(Cmd* pCmd, DescriptorBinder* pDescriptorBinder, uint32_t numDescriptors, DescriptorData* pDescParams);
namespace d3d12 { extern void cmdBindDescriptors(Cmd* pCmd, DescriptorBinder* pDescriptorBinder, uint32_t numDescriptors, DescriptorData* pDescParams); } 
namespace vk { extern void cmdBindDescriptors(Cmd* pCmd, DescriptorBinder* pDescriptorBinder, uint32_t numDescriptors, DescriptorData* pDescParams); } 
namespace mtl { extern void cmdBindDescriptors(Cmd* pCmd, DescriptorBinder* pDescriptorBinder, uint32_t numDescriptors, DescriptorData* pDescParams); } 

void(*cmdBindIndexBuffer)(Cmd* p_cmd, Buffer* p_buffer, uint64_t offset);
namespace d3d12 { extern void cmdBindIndexBuffer(Cmd* p_cmd, Buffer* p_buffer, uint64_t offset); } 
namespace vk { extern void cmdBindIndexBuffer(Cmd* p_cmd, Buffer* p_buffer, uint64_t offset); } 
namespace mtl { extern void cmdBindIndexBuffer(Cmd* p_cmd, Buffer* p_buffer, uint64_t offset); } 

void(*cmdBindVertexBuffer)(Cmd* p_cmd, uint32_t buffer_count, Buffer** pp_buffers, uint64_t* pOffsets);
namespace d3d12 { extern void cmdBindVertexBuffer(Cmd* p_cmd, uint32_t buffer_count, Buffer** pp_buffers, uint64_t* pOffsets); } 
namespace vk { extern void cmdBindVertexBuffer(Cmd* p_cmd, uint32_t buffer_count, Buffer** pp_buffers, uint64_t* pOffsets); } 
namespace mtl { extern void cmdBindVertexBuffer(Cmd* p_cmd, uint32_t buffer_count, Buffer** pp_buffers, uint64_t* pOffsets); } 

void(*cmdDraw)(Cmd* p_cmd, uint32_t vertex_count, uint32_t first_vertex);
namespace d3d12 { extern void cmdDraw(Cmd* p_cmd, uint32_t vertex_count, uint32_t first_vertex); } 
namespace vk { extern void cmdDraw(Cmd* p_cmd, uint32_t vertex_count, uint32_t first_vertex); } 
namespace mtl { extern void cmdDraw(Cmd* p_cmd, uint32_t vertex_count, uint32_t first_vertex); } 

void(*cmdDrawInstanced)(Cmd* pCmd, uint32_t vertexCount, uint32_t firstVertex, uint32_t instanceCount, uint32_t firstInstance);
namespace d3d12 { extern void cmdDrawInstanced(Cmd* pCmd, uint32_t vertexCount, uint32_t firstVertex, uint32_t instanceCount, uint32_t firstInstance); } 
namespace vk { extern void cmdDrawInstanced(Cmd* pCmd, uint32_t vertexCount, uint32_t firstVertex, uint32_t instanceCount, uint32_t firstInstance); } 
namespace mtl { extern void cmdDrawInstanced(Cmd* pCmd, uint32_t vertexCount, uint32_t firstVertex, uint32_t instanceCount, uint32_t firstInstance); } 

void(*cmdDrawIndexed)(Cmd* p_cmd, uint32_t index_count, uint32_t first_index, uint32_t first_vertex);
namespace d3d12 { extern void cmdDrawIndexed(Cmd* p_cmd, uint32_t index_count, uint32_t first_index, uint32_t first_vertex); } 
namespace vk { extern void cmdDrawIndexed(Cmd* p_cmd, uint32_t index_count, uint32_t first_index, uint32_t first_vertex); } 
namespace mtl { extern void cmdDrawIndexed(Cmd* p_cmd, uint32_t index_count, uint32_t first_index, uint32_t first_vertex); } 

void(*cmdDrawIndexedInstanced)(Cmd* pCmd, uint32_t indexCount, uint32_t firstIndex, uint32_t instanceCount, uint32_t firstVertex, uint32_t firstInstance);
namespace d3d12 { extern void cmdDrawIndexedInstanced(Cmd* pCmd, uint32_t indexCount, uint32_t firstIndex, uint32_t instanceCount, uint32_t firstVertex, uint32_t firstInstance); } 
namespace vk { extern void cmdDrawIndexedInstanced(Cmd* pCmd, uint32_t indexCount, uint32_t firstIndex, uint32_t instanceCount, uint32_t firstVertex, uint32_t firstInstance); } 
namespace mtl { extern void cmdDrawIndexedInstanced(Cmd* pCmd, uint32_t indexCount, uint32_t firstIndex, uint32_t instanceCount, uint32_t firstVertex, uint32_t firstInstance); } 

void(*cmdDispatch)(Cmd* p_cmd, uint32_t group_count_x, uint32_t group_count_y, uint32_t group_count_z);
namespace d3d12 { extern void cmdDispatch(Cmd* p_cmd, uint32_t group_count_x, uint32_t group_count_y, uint32_t group_count_z); } 
namespace vk { extern void cmdDispatch(Cmd* p_cmd, uint32_t group_count_x, uint32_t group_count_y, uint32_t group_count_z); } 
namespace mtl { extern void cmdDispatch(Cmd* p_cmd, uint32_t group_count_x, uint32_t group_count_y, uint32_t group_count_z); } 

void(*cmdResourceBarrier)(Cmd* p_cmd, uint32_t buffer_barrier_count, BufferBarrier* p_buffer_barriers, uint32_t texture_barrier_count, TextureBarrier* p_texture_barriers, bool batch);
namespace d3d12 { extern void cmdResourceBarrier(Cmd* p_cmd, uint32_t buffer_barrier_count, BufferBarrier* p_buffer_barriers, uint32_t texture_barrier_count, TextureBarrier* p_texture_barriers, bool batch); } 
namespace vk { extern void cmdResourceBarrier(Cmd* p_cmd, uint32_t buffer_barrier_count, BufferBarrier* p_buffer_barriers, uint32_t texture_barrier_count, TextureBarrier* p_texture_barriers, bool batch); } 
namespace mtl { extern void cmdResourceBarrier(Cmd* p_cmd, uint32_t buffer_barrier_count, BufferBarrier* p_buffer_barriers, uint32_t texture_barrier_count, TextureBarrier* p_texture_barriers, bool batch); } 

void(*cmdSynchronizeResources)(Cmd* p_cmd, uint32_t buffer_count, Buffer** p_buffers, uint32_t texture_count, Texture** p_textures, bool batch);
namespace d3d12 { extern void cmdSynchronizeResources(Cmd* p_cmd, uint32_t buffer_count, Buffer** p_buffers, uint32_t texture_count, Texture** p_textures, bool batch); } 
namespace vk { extern void cmdSynchronizeResources(Cmd* p_cmd, uint32_t buffer_count, Buffer** p_buffers, uint32_t texture_count, Texture** p_textures, bool batch); } 
namespace mtl { extern void cmdSynchronizeResources(Cmd* p_cmd, uint32_t buffer_count, Buffer** p_buffers, uint32_t texture_count, Texture** p_textures, bool batch); } 

void(*cmdFlushBarriers)(Cmd* p_cmd);
namespace d3d12 { extern void cmdFlushBarriers(Cmd* p_cmd); } 
namespace vk { extern void cmdFlushBarriers(Cmd* p_cmd); } 
namespace mtl { extern void cmdFlushBarriers(Cmd* p_cmd); } 

void(*acquireNextImage)(Renderer* pRenderer, SwapChain* p_swap_chain, Semaphore* p_signal_semaphore, Fence* p_fence, uint32_t* p_image_index);
namespace d3d12 { extern void acquireNextImage(Renderer* pRenderer, SwapChain* p_swap_chain, Semaphore* p_signal_semaphore, Fence* p_fence, uint32_t* p_image_index); } 
namespace vk { extern void acquireNextImage(Renderer* pRenderer, SwapChain* p_swap_chain, Semaphore* p_signal_semaphore, Fence* p_fence, uint32_t* p_image_index); } 
namespace mtl { extern void acquireNextImage(Renderer* pRenderer, SwapChain* p_swap_chain, Semaphore* p_signal_semaphore, Fence* p_fence, uint32_t* p_image_index); } 

void(*queueSubmit)(Queue* p_queue, uint32_t cmd_count, Cmd** pp_cmds, Fence* pFence, uint32_t wait_semaphore_count, Semaphore** pp_wait_semaphores, uint32_t signal_semaphore_count, Semaphore** pp_signal_semaphores);
namespace d3d12 { extern void queueSubmit(Queue* p_queue, uint32_t cmd_count, Cmd** pp_cmds, Fence* pFence, uint32_t wait_semaphore_count, Semaphore** pp_wait_semaphores, uint32_t signal_semaphore_count, Semaphore** pp_signal_semaphores); } 
namespace vk { extern void queueSubmit(Queue* p_queue, uint32_t cmd_count, Cmd** pp_cmds, Fence* pFence, uint32_t wait_semaphore_count, Semaphore** pp_wait_semaphores, uint32_t signal_semaphore_count, Semaphore** pp_signal_semaphores); } 
namespace mtl { extern void queueSubmit(Queue* p_queue, uint32_t cmd_count, Cmd** pp_cmds, Fence* pFence, uint32_t wait_semaphore_count, Semaphore** pp_wait_semaphores, uint32_t signal_semaphore_count, Semaphore** pp_signal_semaphores); } 

void(*queuePresent)(Queue* p_queue, SwapChain* p_swap_chain, uint32_t swap_chain_image_index, uint32_t wait_semaphore_count, Semaphore** pp_wait_semaphores);
namespace d3d12 { extern void queuePresent(Queue* p_queue, SwapChain* p_swap_chain, uint32_t swap_chain_image_index, uint32_t wait_semaphore_count, Semaphore** pp_wait_semaphores); } 
namespace vk { extern void queuePresent(Queue* p_queue, SwapChain* p_swap_chain, uint32_t swap_chain_image_index, uint32_t wait_semaphore_count, Semaphore** pp_wait_semaphores); } 
namespace mtl { extern void queuePresent(Queue* p_queue, SwapChain* p_swap_chain, uint32_t swap_chain_image_index, uint32_t wait_semaphore_count, Semaphore** pp_wait_semaphores); } 

void(*waitQueueIdle)(Queue* p_queue);
namespace d3d12 { extern void waitQueueIdle(Queue* p_queue); } 
namespace vk { extern void waitQueueIdle(Queue* p_queue); } 
namespace mtl { extern void waitQueueIdle(Queue* p_queue); } 

void(*getFenceStatus)(Renderer* pRenderer, Fence* p_fence, FenceStatus* p_fence_status);
namespace d3d12 { extern void getFenceStatus(Renderer* pRenderer, Fence* p_fence, FenceStatus* p_fence_status); } 
namespace vk { extern void getFenceStatus(Renderer* pRenderer, Fence* p_fence, FenceStatus* p_fence_status); } 
namespace mtl { extern void getFenceStatus(Renderer* pRenderer, Fence* p_fence, FenceStatus* p_fence_status); } 

void(*waitForFences)(Renderer* pRenderer, uint32_t fence_count, Fence** pp_fences);
namespace d3d12 { extern void waitForFences(Renderer* pRenderer, uint32_t fence_count, Fence** pp_fences); } 
namespace vk { extern void waitForFences(Renderer* pRenderer, uint32_t fence_count, Fence** pp_fences); } 
namespace mtl { extern void waitForFences(Renderer* pRenderer, uint32_t fence_count, Fence** pp_fences); } 

void(*toggleVSync)(Renderer* pRenderer, SwapChain** ppSwapchain);
namespace d3d12 { extern void toggleVSync(Renderer* pRenderer, SwapChain** ppSwapchain); } 
namespace vk { extern void toggleVSync(Renderer* pRenderer, SwapChain** ppSwapchain); } 
namespace mtl { extern void toggleVSync(Renderer* pRenderer, SwapChain** ppSwapchain); } 

bool(*isImageFormatSupported)(ImageFormat::Enum format);
namespace d3d12 { extern bool isImageFormatSupported(ImageFormat::Enum format); } 
namespace vk { extern bool isImageFormatSupported(ImageFormat::Enum format); } 
namespace mtl { extern bool isImageFormatSupported(ImageFormat::Enum format); } 

ImageFormat::Enum(*getRecommendedSwapchainFormat)(bool hintHDR);
namespace d3d12 { extern ImageFormat::Enum getRecommendedSwapchainFormat(bool hintHDR); } 
namespace vk { extern ImageFormat::Enum getRecommendedSwapchainFormat(bool hintHDR); } 
namespace mtl { extern ImageFormat::Enum getRecommendedSwapchainFormat(bool hintHDR); } 

void(*addIndirectCommandSignature)(Renderer* pRenderer, const CommandSignatureDesc* p_desc, CommandSignature** ppCommandSignature);
namespace d3d12 { extern void addIndirectCommandSignature(Renderer* pRenderer, const CommandSignatureDesc* p_desc, CommandSignature** ppCommandSignature); } 
namespace vk { extern void addIndirectCommandSignature(Renderer* pRenderer, const CommandSignatureDesc* p_desc, CommandSignature** ppCommandSignature); } 
namespace mtl { extern void addIndirectCommandSignature(Renderer* pRenderer, const CommandSignatureDesc* p_desc, CommandSignature** ppCommandSignature); } 

void(*removeIndirectCommandSignature)(Renderer* pRenderer, CommandSignature* pCommandSignature);
namespace d3d12 { extern void removeIndirectCommandSignature(Renderer* pRenderer, CommandSignature* pCommandSignature); } 
namespace vk { extern void removeIndirectCommandSignature(Renderer* pRenderer, CommandSignature* pCommandSignature); } 
namespace mtl { extern void removeIndirectCommandSignature(Renderer* pRenderer, CommandSignature* pCommandSignature); } 

void(*cmdExecuteIndirect)(Cmd* pCmd, CommandSignature* pCommandSignature, uint maxCommandCount, Buffer* pIndirectBuffer, uint64_t bufferOffset, Buffer* pCounterBuffer, uint64_t counterBufferOffset);
namespace d3d12 { extern void cmdExecuteIndirect(Cmd* pCmd, CommandSignature* pCommandSignature, uint maxCommandCount, Buffer* pIndirectBuffer, uint64_t bufferOffset, Buffer* pCounterBuffer, uint64_t counterBufferOffset); } 
namespace vk { extern void cmdExecuteIndirect(Cmd* pCmd, CommandSignature* pCommandSignature, uint maxCommandCount, Buffer* pIndirectBuffer, uint64_t bufferOffset, Buffer* pCounterBuffer, uint64_t counterBufferOffset); } 
namespace mtl { extern void cmdExecuteIndirect(Cmd* pCmd, CommandSignature* pCommandSignature, uint maxCommandCount, Buffer* pIndirectBuffer, uint64_t bufferOffset, Buffer* pCounterBuffer, uint64_t counterBufferOffset); } 

void(*getTimestampFrequency)(Queue* pQueue, double* pFrequency);
namespace d3d12 { extern void getTimestampFrequency(Queue* pQueue, double* pFrequency); } 
namespace vk { extern void getTimestampFrequency(Queue* pQueue, double* pFrequency); } 
namespace mtl { extern void getTimestampFrequency(Queue* pQueue, double* pFrequency); } 

void(*addQueryHeap)(Renderer* pRenderer, const QueryHeapDesc* pDesc, QueryHeap** ppQueryHeap);
namespace d3d12 { extern void addQueryHeap(Renderer* pRenderer, const QueryHeapDesc* pDesc, QueryHeap** ppQueryHeap); } 
namespace vk { extern void addQueryHeap(Renderer* pRenderer, const QueryHeapDesc* pDesc, QueryHeap** ppQueryHeap); } 
namespace mtl { extern void addQueryHeap(Renderer* pRenderer, const QueryHeapDesc* pDesc, QueryHeap** ppQueryHeap); } 

void(*removeQueryHeap)(Renderer* pRenderer, QueryHeap* pQueryHeap);
namespace d3d12 { extern void removeQueryHeap(Renderer* pRenderer, QueryHeap* pQueryHeap); } 
namespace vk { extern void removeQueryHeap(Renderer* pRenderer, QueryHeap* pQueryHeap); } 
namespace mtl { extern void removeQueryHeap(Renderer* pRenderer, QueryHeap* pQueryHeap); } 

void(*cmdBeginQuery)(Cmd* pCmd, QueryHeap* pQueryHeap, QueryDesc* pQuery);
namespace d3d12 { extern void cmdBeginQuery(Cmd* pCmd, QueryHeap* pQueryHeap, QueryDesc* pQuery); } 
namespace vk { extern void cmdBeginQuery(Cmd* pCmd, QueryHeap* pQueryHeap, QueryDesc* pQuery); } 
namespace mtl { extern void cmdBeginQuery(Cmd* pCmd, QueryHeap* pQueryHeap, QueryDesc* pQuery); } 

void(*cmdEndQuery)(Cmd* pCmd, QueryHeap* pQueryHeap, QueryDesc* pQuery);
namespace d3d12 { extern void cmdEndQuery(Cmd* pCmd, QueryHeap* pQueryHeap, QueryDesc* pQuery); } 
namespace vk { extern void cmdEndQuery(Cmd* pCmd, QueryHeap* pQueryHeap, QueryDesc* pQuery); } 
namespace mtl { extern void cmdEndQuery(Cmd* pCmd, QueryHeap* pQueryHeap, QueryDesc* pQuery); } 

void(*cmdResolveQuery)(Cmd* pCmd, QueryHeap* pQueryHeap, Buffer* pReadbackBuffer, uint32_t startQuery, uint32_t queryCount);
namespace d3d12 { extern void cmdResolveQuery(Cmd* pCmd, QueryHeap* pQueryHeap, Buffer* pReadbackBuffer, uint32_t startQuery, uint32_t queryCount); } 
namespace vk { extern void cmdResolveQuery(Cmd* pCmd, QueryHeap* pQueryHeap, Buffer* pReadbackBuffer, uint32_t startQuery, uint32_t queryCount); } 
namespace mtl { extern void cmdResolveQuery(Cmd* pCmd, QueryHeap* pQueryHeap, Buffer* pReadbackBuffer, uint32_t startQuery, uint32_t queryCount); } 

void(*calculateMemoryStats)(Renderer* pRenderer, char** stats);
namespace d3d12 { extern void calculateMemoryStats(Renderer* pRenderer, char** stats); } 
namespace vk { extern void calculateMemoryStats(Renderer* pRenderer, char** stats); } 
namespace mtl { extern void calculateMemoryStats(Renderer* pRenderer, char** stats); } 

void(*freeMemoryStats)(Renderer* pRenderer, char* stats);
namespace d3d12 { extern void freeMemoryStats(Renderer* pRenderer, char* stats); } 
namespace vk { extern void freeMemoryStats(Renderer* pRenderer, char* stats); } 
namespace mtl { extern void freeMemoryStats(Renderer* pRenderer, char* stats); } 

void(*cmdBeginDebugMarker)(Cmd* pCmd, float r, float g, float b, const char* pName);
namespace d3d12 { extern void cmdBeginDebugMarker(Cmd* pCmd, float r, float g, float b, const char* pName); } 
namespace vk { extern void cmdBeginDebugMarker(Cmd* pCmd, float r, float g, float b, const char* pName); } 
namespace mtl { extern void cmdBeginDebugMarker(Cmd* pCmd, float r, float g, float b, const char* pName); } 

void(*cmdEndDebugMarker)(Cmd* pCmd);
namespace d3d12 { extern void cmdEndDebugMarker(Cmd* pCmd); } 
namespace vk { extern void cmdEndDebugMarker(Cmd* pCmd); } 
namespace mtl { extern void cmdEndDebugMarker(Cmd* pCmd); } 

void(*cmdAddDebugMarker)(Cmd* pCmd, float r, float g, float b, const char* pName);
namespace d3d12 { extern void cmdAddDebugMarker(Cmd* pCmd, float r, float g, float b, const char* pName); } 
namespace vk { extern void cmdAddDebugMarker(Cmd* pCmd, float r, float g, float b, const char* pName); } 
namespace mtl { extern void cmdAddDebugMarker(Cmd* pCmd, float r, float g, float b, const char* pName); } 

void(*setBufferName)(Renderer* pRenderer, Buffer* pBuffer, const char* pName);
namespace d3d12 { extern void setBufferName(Renderer* pRenderer, Buffer* pBuffer, const char* pName); } 
namespace vk { extern void setBufferName(Renderer* pRenderer, Buffer* pBuffer, const char* pName); } 
namespace mtl { extern void setBufferName(Renderer* pRenderer, Buffer* pBuffer, const char* pName); } 

void(*setTextureName)(Renderer* pRenderer, Texture* pTexture, const char* pName);
namespace d3d12 { extern void setTextureName(Renderer* pRenderer, Texture* pTexture, const char* pName); } 
namespace vk { extern void setTextureName(Renderer* pRenderer, Texture* pTexture, const char* pName); } 
namespace mtl { extern void setTextureName(Renderer* pRenderer, Texture* pTexture, const char* pName); } 

bool loadRendererFunctions(RendererApi api)
{
#if defined(DIRECT3D12)
	if (api == RENDERER_API_D3D12)
	{
		addBuffer = d3d12::addBuffer;
		removeBuffer = d3d12::removeBuffer;
		addTexture = d3d12::addTexture;
		removeTexture = d3d12::removeTexture;
		mapBuffer = d3d12::mapBuffer;
		unmapBuffer = d3d12::unmapBuffer;
		cmdUpdateBuffer = d3d12::cmdUpdateBuffer;
		cmdUpdateSubresources = d3d12::cmdUpdateSubresources;
		compileShader = d3d12::compileShader;
		get_renderer_shaderdefines = d3d12::get_renderer_shaderdefines;
		initRenderer = d3d12::initRenderer;
		removeRenderer = d3d12::removeRenderer;
		addFence = d3d12::addFence;
		removeFence = d3d12::removeFence;
		addSemaphore = d3d12::addSemaphore;
		removeSemaphore = d3d12::removeSemaphore;
		addQueue = d3d12::addQueue;
		removeQueue = d3d12::removeQueue;
		addSwapChain = d3d12::addSwapChain;
		removeSwapChain = d3d12::removeSwapChain;
		addCmdPool = d3d12::addCmdPool;
		removeCmdPool = d3d12::removeCmdPool;
		addCmd = d3d12::addCmd;
		removeCmd = d3d12::removeCmd;
		addCmd_n = d3d12::addCmd_n;
		removeCmd_n = d3d12::removeCmd_n;
		addRenderTarget = d3d12::addRenderTarget;
		removeRenderTarget = d3d12::removeRenderTarget;
		addSampler = d3d12::addSampler;
		removeSampler = d3d12::removeSampler;
		addShaderBinary = d3d12::addShaderBinary;
		removeShader = d3d12::removeShader;
		addRootSignature = d3d12::addRootSignature;
		removeRootSignature = d3d12::removeRootSignature;
		addPipeline = d3d12::addPipeline;
		addComputePipeline = d3d12::addComputePipeline;
		removePipeline = d3d12::removePipeline;
		addDescriptorBinder = d3d12::addDescriptorBinder;
		removeDescriptorBinder = d3d12::removeDescriptorBinder;
		addBlendState = d3d12::addBlendState;
		removeBlendState = d3d12::removeBlendState;
		addDepthState = d3d12::addDepthState;
		removeDepthState = d3d12::removeDepthState;
		addRasterizerState = d3d12::addRasterizerState;
		removeRasterizerState = d3d12::removeRasterizerState;
		beginCmd = d3d12::beginCmd;
		endCmd = d3d12::endCmd;
		cmdBindRenderTargets = d3d12::cmdBindRenderTargets;
		cmdSetViewport = d3d12::cmdSetViewport;
		cmdSetScissor = d3d12::cmdSetScissor;
		cmdBindPipeline = d3d12::cmdBindPipeline;
		cmdBindDescriptors = d3d12::cmdBindDescriptors;
		cmdBindIndexBuffer = d3d12::cmdBindIndexBuffer;
		cmdBindVertexBuffer = d3d12::cmdBindVertexBuffer;
		cmdDraw = d3d12::cmdDraw;
		cmdDrawInstanced = d3d12::cmdDrawInstanced;
		cmdDrawIndexed = d3d12::cmdDrawIndexed;
		cmdDrawIndexedInstanced = d3d12::cmdDrawIndexedInstanced;
		cmdDispatch = d3d12::cmdDispatch;
		cmdResourceBarrier = d3d12::cmdResourceBarrier;
		cmdSynchronizeResources = d3d12::cmdSynchronizeResources;
		cmdFlushBarriers = d3d12::cmdFlushBarriers;
		acquireNextImage = d3d12::acquireNextImage;
		queueSubmit = d3d12::queueSubmit;
		queuePresent = d3d12::queuePresent;
		waitQueueIdle = d3d12::waitQueueIdle;
		getFenceStatus = d3d12::getFenceStatus;
		waitForFences = d3d12::waitForFences;
		toggleVSync = d3d12::toggleVSync;
		isImageFormatSupported = d3d12::isImageFormatSupported;
		getRecommendedSwapchainFormat = d3d12::getRecommendedSwapchainFormat;
		addIndirectCommandSignature = d3d12::addIndirectCommandSignature;
		removeIndirectCommandSignature = d3d12::removeIndirectCommandSignature;
		cmdExecuteIndirect = d3d12::cmdExecuteIndirect;
		getTimestampFrequency = d3d12::getTimestampFrequency;
		addQueryHeap = d3d12::addQueryHeap;
		removeQueryHeap = d3d12::removeQueryHeap;
		cmdBeginQuery = d3d12::cmdBeginQuery;
		cmdEndQuery = d3d12::cmdEndQuery;
		cmdResolveQuery = d3d12::cmdResolveQuery;
		calculateMemoryStats = d3d12::calculateMemoryStats;
		freeMemoryStats = d3d12::freeMemoryStats;
		cmdBeginDebugMarker = d3d12::cmdBeginDebugMarker;
		cmdEndDebugMarker = d3d12::cmdEndDebugMarker;
		cmdAddDebugMarker = d3d12::cmdAddDebugMarker;
		setBufferName = d3d12::setBufferName;
		setTextureName = d3d12::setTextureName;
		return true;
	}
#endif
#if defined(VULKAN)
	if (api == RENDERER_API_VULKAN)
	{
		addBuffer = vk::addBuffer;
		removeBuffer = vk::removeBuffer;
		addTexture = vk::addTexture;
		removeTexture = vk::removeTexture;
		mapBuffer = vk::mapBuffer;
		unmapBuffer = vk::unmapBuffer;
		cmdUpdateBuffer = vk::cmdUpdateBuffer;
		cmdUpdateSubresources = vk::cmdUpdateSubresources;
		get_renderer_shaderdefines = vk::get_renderer_shaderdefines;
		initRenderer = vk::initRenderer;
		removeRenderer = vk::removeRenderer;
		addFence = vk::addFence;
		removeFence = vk::removeFence;
		addSemaphore = vk::addSemaphore;
		removeSemaphore = vk::removeSemaphore;
		addQueue = vk::addQueue;
		removeQueue = vk::removeQueue;
		addSwapChain = vk::addSwapChain;
		removeSwapChain = vk::removeSwapChain;
		addCmdPool = vk::addCmdPool;
		removeCmdPool = vk::removeCmdPool;
		addCmd = vk::addCmd;
		removeCmd = vk::removeCmd;
		addCmd_n = vk::addCmd_n;
		removeCmd_n = vk::removeCmd_n;
		addRenderTarget = vk::addRenderTarget;
		removeRenderTarget = vk::removeRenderTarget;
		addSampler = vk::addSampler;
		removeSampler = vk::removeSampler;
		addShaderBinary = vk::addShaderBinary;
		removeShader = vk::removeShader;
		addRootSignature = vk::addRootSignature;
		removeRootSignature = vk::removeRootSignature;
		addPipeline = vk::addPipeline;
		addComputePipeline = vk::addComputePipeline;
		removePipeline = vk::removePipeline;
		addDescriptorBinder = vk::addDescriptorBinder;
		removeDescriptorBinder = vk::removeDescriptorBinder;
		addBlendState = vk::addBlendState;
		removeBlendState = vk::removeBlendState;
		addDepthState = vk::addDepthState;
		removeDepthState = vk::removeDepthState;
		addRasterizerState = vk::addRasterizerState;
		removeRasterizerState = vk::removeRasterizerState;
		beginCmd = vk::beginCmd;
		endCmd = vk::endCmd;
		cmdBindRenderTargets = vk::cmdBindRenderTargets;
		cmdSetViewport = vk::cmdSetViewport;
		cmdSetScissor = vk::cmdSetScissor;
		cmdBindPipeline = vk::cmdBindPipeline;
		cmdBindDescriptors = vk::cmdBindDescriptors;
		cmdBindIndexBuffer = vk::cmdBindIndexBuffer;
		cmdBindVertexBuffer = vk::cmdBindVertexBuffer;
		cmdDraw = vk::cmdDraw;
		cmdDrawInstanced = vk::cmdDrawInstanced;
		cmdDrawIndexed = vk::cmdDrawIndexed;
		cmdDrawIndexedInstanced = vk::cmdDrawIndexedInstanced;
		cmdDispatch = vk::cmdDispatch;
		cmdResourceBarrier = vk::cmdResourceBarrier;
		cmdSynchronizeResources = vk::cmdSynchronizeResources;
		cmdFlushBarriers = vk::cmdFlushBarriers;
		acquireNextImage = vk::acquireNextImage;
		queueSubmit = vk::queueSubmit;
		queuePresent = vk::queuePresent;
		waitQueueIdle = vk::waitQueueIdle;
		getFenceStatus = vk::getFenceStatus;
		waitForFences = vk::waitForFences;
		toggleVSync = vk::toggleVSync;
		isImageFormatSupported = vk::isImageFormatSupported;
		getRecommendedSwapchainFormat = vk::getRecommendedSwapchainFormat;
		addIndirectCommandSignature = vk::addIndirectCommandSignature;
		removeIndirectCommandSignature = vk::removeIndirectCommandSignature;
		cmdExecuteIndirect = vk::cmdExecuteIndirect;
		getTimestampFrequency = vk::getTimestampFrequency;
		addQueryHeap = vk::addQueryHeap;
		removeQueryHeap = vk::removeQueryHeap;
		cmdBeginQuery = vk::cmdBeginQuery;
		cmdEndQuery = vk::cmdEndQuery;
		cmdResolveQuery = vk::cmdResolveQuery;
		calculateMemoryStats = vk::calculateMemoryStats;
		freeMemoryStats = vk::freeMemoryStats;
		cmdBeginDebugMarker = vk::cmdBeginDebugMarker;
		cmdEndDebugMarker = vk::cmdEndDebugMarker;
		cmdAddDebugMarker = vk::cmdAddDebugMarker;
		setBufferName = vk::setBufferName;
		setTextureName = vk::setTextureName;
		return true;
	}
#endif
#if defined(METAL)
	if (api == RENDERER_API_METAL)
	{
		addBuffer = mtl::addBuffer;
		removeBuffer = mtl::removeBuffer;
		addTexture = mtl::addTexture;
		removeTexture = mtl::removeTexture;
		mapBuffer = mtl::mapBuffer;
		unmapBuffer = mtl::unmapBuffer;
		cmdUpdateBuffer = mtl::cmdUpdateBuffer;
		cmdUpdateSubresources = mtl::cmdUpdateSubresources;
		get_renderer_shaderdefines = mtl::get_renderer_shaderdefines;
		initRenderer = mtl::initRenderer;
		removeRenderer = mtl::removeRenderer;
		addFence = mtl::addFence;
		removeFence = mtl::removeFence;
		addSemaphore = mtl::addSemaphore;
		removeSemaphore = mtl::removeSemaphore;
		addQueue = mtl::addQueue;
		removeQueue = mtl::removeQueue;
		addSwapChain = mtl::addSwapChain;
		removeSwapChain = mtl::removeSwapChain;
		addCmdPool = mtl::addCmdPool;
		removeCmdPool = mtl::removeCmdPool;
		addCmd = mtl::addCmd;
		removeCmd = mtl::removeCmd;
		addCmd_n = mtl::addCmd_n;
		removeCmd_n = mtl::removeCmd_n;
		addRenderTarget = mtl::addRenderTarget;
		removeRenderTarget = mtl::removeRenderTarget;
		addSampler = mtl::addSampler;
		removeSampler = mtl::removeSampler;
		addShaderBinary = mtl::addShaderBinary;
		removeShader = mtl::removeShader;
		addRootSignature = mtl::addRootSignature;
		removeRootSignature = mtl::removeRootSignature;
		addPipeline = mtl::addPipeline;
		addComputePipeline = mtl::addComputePipeline;
		removePipeline = mtl::removePipeline;
		addDescriptorBinder = mtl::addDescriptorBinder;
		removeDescriptorBinder = mtl::removeDescriptorBinder;
		addBlendState = mtl::addBlendState;
		removeBlendState = mtl::removeBlendState;
		addDepthState = mtl::addDepthState;
		removeDepthState = mtl::removeDepthState;
		addRasterizerState = mtl::addRasterizerState;
		removeRasterizerState = mtl::removeRasterizerState;
		beginCmd = mtl::beginCmd;
		endCmd = mtl::endCmd;
		cmdBindRenderTargets = mtl::cmdBindRenderTargets;
		cmdSetViewport = mtl::cmdSetViewport;
		cmdSetScissor = mtl::cmdSetScissor;
		cmdBindPipeline = mtl::cmdBindPipeline;
		cmdBindDescriptors = mtl::cmdBindDescriptors;
		cmdBindIndexBuffer = mtl::cmdBindIndexBuffer;
		cmdBindVertexBuffer = mtl::cmdBindVertexBuffer;
		cmdDraw = mtl::cmdDraw;
		cmdDrawInstanced = mtl::cmdDrawInstanced;
		cmdDrawIndexed = mtl::cmdDrawIndexed;
		cmdDrawIndexedInstanced = mtl::cmdDrawIndexedInstanced;
		cmdDispatch = mtl::cmdDispatch;
		cmdResourceBarrier = mtl::cmdResourceBarrier;
		cmdSynchronizeResources = mtl::cmdSynchronizeResources;
		cmdFlushBarriers = mtl::cmdFlushBarriers;
		acquireNextImage = mtl::acquireNextImage;
		queueSubmit = mtl::queueSubmit;
		queuePresent = mtl::queuePresent;
		waitQueueIdle = mtl::waitQueueIdle;
		getFenceStatus = mtl::getFenceStatus;
		waitForFences = mtl::waitForFences;
		toggleVSync = mtl::toggleVSync;
		isImageFormatSupported = mtl::isImageFormatSupported;
		getRecommendedSwapchainFormat = mtl::getRecommendedSwapchainFormat;
		addIndirectCommandSignature = mtl::addIndirectCommandSignature;
		removeIndirectCommandSignature = mtl::removeIndirectCommandSignature;
		cmdExecuteIndirect = mtl::cmdExecuteIndirect;
		getTimestampFrequency = mtl::getTimestampFrequency;
		addQueryHeap = mtl::addQueryHeap;
		removeQueryHeap = mtl::removeQueryHeap;
		cmdBeginQuery = mtl::cmdBeginQuery;
		cmdEndQuery = mtl::cmdEndQuery;
		cmdResolveQuery = mtl::cmdResolveQuery;
		calculateMemoryStats = mtl::calculateMemoryStats;
		freeMemoryStats = mtl::freeMemoryStats;
		cmdBeginDebugMarker = mtl::cmdBeginDebugMarker;
		cmdEndDebugMarker = mtl::cmdEndDebugMarker;
		cmdAddDebugMarker = mtl::cmdAddDebugMarker;
		setBufferName = mtl::setBufferName;
		setTextureName = mtl::setTextureName;
		return true;
	}
#endif

	return false;
}

void unloadRendererFunctions()
{
}
