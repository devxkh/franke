#pragma once

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	This file was auto-generated to contain all exported functions in The Forge.
	All functions containing 'API_INTERFACE' and are located in the 'Renderer' directory
	are added automatically upon compilation.

	Script is located at TheForge/Tools/AutoGenerateRendererAPI.py

	Note : All manual changes to these files will be overwritten
	If a function that you want is missing, please make sure the header file in which it is
	declared is located in the 'Renderer' directory and that you re-build The Forge.
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

extern void(*addBuffer)(Renderer* pRenderer, const BufferDesc* pDesc, Buffer** pp_buffer);
extern void(*removeBuffer)(Renderer* pRenderer, Buffer* pBuffer);
extern void(*addTexture)(Renderer* pRenderer, const TextureDesc* pDesc, Texture** ppTexture);
extern void(*removeTexture)(Renderer* pRenderer, Texture* pTexture);
extern void(*mapBuffer)(Renderer* pRenderer, Buffer* pBuffer, ReadRange* pRange);
extern void(*unmapBuffer)(Renderer* pRenderer, Buffer* pBuffer);
extern void(*cmdUpdateBuffer)(Cmd* pCmd, uint64_t srcOffset, uint64_t dstOffset, uint64_t size, Buffer* pSrcBuffer, Buffer* pBuffer);
extern void(*cmdUpdateSubresources)(Cmd* pCmd, uint32_t numSubresources, SubresourceDataDesc* pSubresources, Buffer* pIntermediate, Texture* pTexture);
extern void(*compileShader)(Renderer* pRenderer, ShaderTarget target, ShaderStage stage, const char* fileName, uint32_t codeSize, const char* code, uint32_t macroCount, ShaderMacro* pMacros, void* (*allocator)(size_t a), uint32_t* pByteCodeSize, char** ppByteCode, const char* pEntryPoint);
extern const RendererShaderDefinesDesc(*get_renderer_shaderdefines)(Renderer* pRenderer);
extern void(*initRenderer)(const char* app_name, const RendererDesc* p_settings, Renderer** ppRenderer);
extern void(*removeRenderer)(Renderer* pRenderer);
extern void(*addFence)(Renderer* pRenderer, Fence** pp_fence);
extern void(*removeFence)(Renderer* pRenderer, Fence* p_fence);
extern void(*addSemaphore)(Renderer* pRenderer, Semaphore** pp_semaphore);
extern void(*removeSemaphore)(Renderer* pRenderer, Semaphore* p_semaphore);
extern void(*addQueue)(Renderer* pRenderer, QueueDesc* pQDesc, Queue** ppQueue);
extern void(*removeQueue)(Queue* pQueue);
extern void(*addSwapChain)(Renderer* pRenderer, const SwapChainDesc* p_desc, SwapChain** pp_swap_chain);
extern void(*removeSwapChain)(Renderer* pRenderer, SwapChain* p_swap_chain);
extern void(*addCmdPool)(Renderer* pRenderer, Queue* p_queue, bool transient, CmdPool** pp_CmdPool);
extern void(*removeCmdPool)(Renderer* pRenderer, CmdPool* p_CmdPool);
extern void(*addCmd)(CmdPool* p_CmdPool, bool secondary, Cmd** pp_cmd);
extern void(*removeCmd)(CmdPool* p_CmdPool, Cmd* p_cmd);
extern void(*addCmd_n)(CmdPool* p_CmdPool, bool secondary, uint32_t cmd_count, Cmd*** ppp_cmd);
extern void(*removeCmd_n)(CmdPool* p_CmdPool, uint32_t cmd_count, Cmd** pp_cmd);
extern void(*addRenderTarget)(Renderer* pRenderer, const RenderTargetDesc* p_desc, RenderTarget** pp_render_target);
extern void(*removeRenderTarget)(Renderer* pRenderer, RenderTarget* p_render_target);
extern void(*addSampler)(Renderer* pRenderer, const SamplerDesc* pDesc, Sampler** pp_sampler);
extern void(*removeSampler)(Renderer* pRenderer, Sampler* p_sampler);
extern void(*addShaderBinary)(Renderer* pRenderer, const BinaryShaderDesc* p_desc, Shader** p_shader_program);
extern void(*removeShader)(Renderer* pRenderer, Shader* p_shader_program);
extern void(*addRootSignature)(Renderer* pRenderer, const RootSignatureDesc* pRootDesc, RootSignature** pp_root_signature);
extern void(*removeRootSignature)(Renderer* pRenderer, RootSignature* pRootSignature);
extern void(*addPipeline)(Renderer* pRenderer, const PipelineDesc* p_pipeline_settings, Pipeline** pp_pipeline);
extern void(*addComputePipeline)(Renderer* pRenderer, const ComputePipelineDesc* p_pipeline_settings, Pipeline** p_pipeline);
extern void(*removePipeline)(Renderer* pRenderer, Pipeline* p_pipeline);
extern void(*addDescriptorBinder)(Renderer* pRenderer, const DescriptorBinderDesc* p_desc, DescriptorBinder** pp_descriptor_binder);
extern void(*removeDescriptorBinder)(Renderer* pRenderer, DescriptorBinder* p_descriptor_binder);
extern void(*addBlendState)(Renderer* pRenderer, const BlendStateDesc* pDesc, BlendState** ppBlendState);
extern void(*removeBlendState)(BlendState* pBlendState);
extern void(*addDepthState)(Renderer* pRenderer, const DepthStateDesc* pDesc, DepthState** ppDepthState);
extern void(*removeDepthState)(DepthState* pDepthState);
extern void(*addRasterizerState)(Renderer* pRenderer, const RasterizerStateDesc* pDesc, RasterizerState** ppRasterizerState);
extern void(*removeRasterizerState)(RasterizerState* pRasterizerState);
extern void(*beginCmd)(Cmd* p_cmd);
extern void(*endCmd)(Cmd* p_cmd);
extern void(*cmdBindRenderTargets)(Cmd* p_cmd, uint32_t render_target_count, RenderTarget** pp_render_targets, RenderTarget* p_depth_stencil, const LoadActionsDesc* loadActions, uint32_t* pColorArraySlices, uint32_t* pColorMipSlices, uint32_t depthArraySlice, uint32_t depthMipSlice);
extern void(*cmdSetViewport)(Cmd* p_cmd, float x, float y, float width, float height, float min_depth, float max_depth);
extern void(*cmdSetScissor)(Cmd* p_cmd, uint32_t x, uint32_t y, uint32_t width, uint32_t height);
extern void(*cmdBindPipeline)(Cmd* p_cmd, Pipeline* p_pipeline);
extern void(*cmdBindDescriptors)(Cmd* pCmd, DescriptorBinder* pDescriptorBinder, uint32_t numDescriptors, DescriptorData* pDescParams);
extern void(*cmdBindIndexBuffer)(Cmd* p_cmd, Buffer* p_buffer, uint64_t offset);
extern void(*cmdBindVertexBuffer)(Cmd* p_cmd, uint32_t buffer_count, Buffer** pp_buffers, uint64_t* pOffsets);
extern void(*cmdDraw)(Cmd* p_cmd, uint32_t vertex_count, uint32_t first_vertex);
extern void(*cmdDrawInstanced)(Cmd* pCmd, uint32_t vertexCount, uint32_t firstVertex, uint32_t instanceCount, uint32_t firstInstance);
extern void(*cmdDrawIndexed)(Cmd* p_cmd, uint32_t index_count, uint32_t first_index, uint32_t first_vertex);
extern void(*cmdDrawIndexedInstanced)(Cmd* pCmd, uint32_t indexCount, uint32_t firstIndex, uint32_t instanceCount, uint32_t firstVertex, uint32_t firstInstance);
extern void(*cmdDispatch)(Cmd* p_cmd, uint32_t group_count_x, uint32_t group_count_y, uint32_t group_count_z);
extern void(*cmdResourceBarrier)(Cmd* p_cmd, uint32_t buffer_barrier_count, BufferBarrier* p_buffer_barriers, uint32_t texture_barrier_count, TextureBarrier* p_texture_barriers, bool batch);
extern void(*cmdSynchronizeResources)(Cmd* p_cmd, uint32_t buffer_count, Buffer** p_buffers, uint32_t texture_count, Texture** p_textures, bool batch);
extern void(*cmdFlushBarriers)(Cmd* p_cmd);
extern void(*acquireNextImage)(Renderer* pRenderer, SwapChain* p_swap_chain, Semaphore* p_signal_semaphore, Fence* p_fence, uint32_t* p_image_index);
extern void(*queueSubmit)(Queue* p_queue, uint32_t cmd_count, Cmd** pp_cmds, Fence* pFence, uint32_t wait_semaphore_count, Semaphore** pp_wait_semaphores, uint32_t signal_semaphore_count, Semaphore** pp_signal_semaphores);
extern void(*queuePresent)(Queue* p_queue, SwapChain* p_swap_chain, uint32_t swap_chain_image_index, uint32_t wait_semaphore_count, Semaphore** pp_wait_semaphores);
extern void(*waitQueueIdle)(Queue* p_queue);
extern void(*getFenceStatus)(Renderer* pRenderer, Fence* p_fence, FenceStatus* p_fence_status);
extern void(*waitForFences)(Renderer* pRenderer, uint32_t fence_count, Fence** pp_fences);
extern void(*toggleVSync)(Renderer* pRenderer, SwapChain** ppSwapchain);
extern bool(*isImageFormatSupported)(ImageFormat::Enum format);
extern ImageFormat::Enum(*getRecommendedSwapchainFormat)(bool hintHDR);
extern void(*addIndirectCommandSignature)(Renderer* pRenderer, const CommandSignatureDesc* p_desc, CommandSignature** ppCommandSignature);
extern void(*removeIndirectCommandSignature)(Renderer* pRenderer, CommandSignature* pCommandSignature);
extern void(*cmdExecuteIndirect)(Cmd* pCmd, CommandSignature* pCommandSignature, uint maxCommandCount, Buffer* pIndirectBuffer, uint64_t bufferOffset, Buffer* pCounterBuffer, uint64_t counterBufferOffset);
extern void(*getTimestampFrequency)(Queue* pQueue, double* pFrequency);
extern void(*addQueryHeap)(Renderer* pRenderer, const QueryHeapDesc* pDesc, QueryHeap** ppQueryHeap);
extern void(*removeQueryHeap)(Renderer* pRenderer, QueryHeap* pQueryHeap);
extern void(*cmdBeginQuery)(Cmd* pCmd, QueryHeap* pQueryHeap, QueryDesc* pQuery);
extern void(*cmdEndQuery)(Cmd* pCmd, QueryHeap* pQueryHeap, QueryDesc* pQuery);
extern void(*cmdResolveQuery)(Cmd* pCmd, QueryHeap* pQueryHeap, Buffer* pReadbackBuffer, uint32_t startQuery, uint32_t queryCount);
extern void(*calculateMemoryStats)(Renderer* pRenderer, char** stats);
extern void(*freeMemoryStats)(Renderer* pRenderer, char* stats);
extern void(*cmdBeginDebugMarker)(Cmd* pCmd, float r, float g, float b, const char* pName);
extern void(*cmdEndDebugMarker)(Cmd* pCmd);
extern void(*cmdAddDebugMarker)(Cmd* pCmd, float r, float g, float b, const char* pName);
extern void(*setBufferName)(Renderer* pRenderer, Buffer* pBuffer, const char* pName);
extern void(*setTextureName)(Renderer* pRenderer, Texture* pTexture, const char* pName);

extern bool loadRendererFunctions(RendererApi api);
extern void unloadRendererFunctions();
