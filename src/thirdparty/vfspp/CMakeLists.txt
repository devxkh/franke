message("/src/thirdparty/vfspp start")

#add_subdirectory(src/mbedtls/include)

add_library(vfspp STATIC 

    include/CFileInfo.h
    include/CMemoryFile.h
    include/CMemoryFileSystem.h
    include/CNativeFile.h
    include/CNativeFileSystem.h
    include/CVirtualFileSystem.h
    include/CZipFile.h
    include/CZipFileSystem.h
    include/VFS.h
    include/IFile.h
    include/IFileSystem.h

    src/CFileInfo.cpp
    src/CMemoryFile.cpp
    src/CMemoryFileSystem.cpp
    src/CNativeFile.cpp
    src/CNativeFileSystem.cpp
    src/CStringUtilsVFS.cpp
    src/CVirtualFileSystem.cpp
    src/CZipFile.cpp
    src/CZipFileSystem.cpp
    src/miniz.c

	#private
    src/CStringUtilsVFS.h
    src/miniz.h
)

# add header search folders
include_directories(
	include)

#
# ----- add lib to solution folder -----
set_property(TARGET vfspp PROPERTY FOLDER "thirdparty/vfspp")
#

#
# ----- set dll export flag ------------
#
set_target_properties(vfspp PROPERTIES DEFINE_SYMBOL FE_EXPORT)


set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++11 -stdlib=libc++ -fno-rtti -fno-exceptions -g -Wall")




