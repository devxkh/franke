
message("/src/thirdparty/yojimbo start")


#add_subdirectory(src/mbedtls/include)
add_subdirectory(src/mbedtls/library)
add_subdirectory(src/libsodium)

add_library(yojimbo STATIC 
			src/yojimbo.cpp src/yojimbo.h
			src/tlsf/tlsf.c src/tlsf/tlsf.h
			src/reliable.io/reliable.c src/reliable.io/reliable.h
			src/netcode.io/netcode.c src/netcode.io/netcode.h
			)

# add header search folders
include_directories(
	src/mbedtls/include
	src/netcode.io
	src/reliable.io
	src/libsodium/include/
	src/libsodium/include/sodium)

#
# ----- add lib to solution folder -----
set_property(TARGET yojimbo PROPERTY FOLDER "thirdparty/yojimbo")
#

#
# ----- set dll export flag ------------
#
set_target_properties(yojimbo PROPERTIES DEFINE_SYMBOL FE_EXPORT)


#
# add dependencies
#
target_link_libraries(yojimbo 
			libsodium
			mbedtls
			mbedx509
			mbedcrypto
			)



