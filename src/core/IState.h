#pragma once

#include "EngineConfig.h"
#include <memory>

namespace FE
{
	class IApp;

	class IState
	{
	public:
		IState(IApp& app, bool replace = true)
			: app(app),
			m_replacing(replace),
			m_breaked(false)
		{

		}

		virtual ~IState() {};

		virtual void pause() = 0;
		virtual void resume() = 0;

		virtual void update(float deltaTime) = 0;
		virtual void draw() = 0;

		std::unique_ptr<IState> next()
		{
			return std::move(m_next);
		}

		bool isReplacing()
		{
			return m_replacing;
		}
		
		IApp& app;
		std::unique_ptr<IState> m_next;

	protected:

		bool m_replacing;
		bool m_breaked; //State sleeping

	};

} // namespace XE