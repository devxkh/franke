#pragma once

#include "EngineConfig.h"
#include "IState.h"
#include <vector>
#include <memory>
#include <stack>
#include <chrono>

namespace FE
{
	class IMiddleware;

	class Engine
	{
	public:

		FE_API void run(std::unique_ptr< IState > state);
		FE_API void setNextState(std::unique_ptr<IState> state);
		FE_API void nextState();
		FE_API void lastState();
		FE_API void draw();

		FE_API IState* getCurrentState() {
			return m_states.top().get();
		}

		template < typename T >
		std::unique_ptr< T > build(Engine& engine, bool replace = true);

		FE_API Engine();

		FE_API ~Engine();

		FE_API void init();

		FE_API void update();

		FE_API void quit();

		bool m_isRunning = true;
		bool m_resume =  false;
		bool m_isInitialized = false;

		std::vector<std::unique_ptr<IMiddleware>> middlewares;
		std::stack< std::unique_ptr< IState > > m_states;

	private:

		std::chrono::steady_clock::time_point m_timeStart;
	};


	template < typename T >
	std::unique_ptr< T > Engine::build(Engine& engine, bool replace)
	{
		return std::move(std::unique_ptr< T >(new T(engine, replace)));
	}
}