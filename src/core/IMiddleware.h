#pragma once

#include "EngineConfig.h"

namespace FE
{

	class FE_API IMiddleware
	{

	public:
		virtual ~IMiddleware() {}

		virtual void init() = 0;

		virtual void update(float delta) = 0;

		virtual void destroy() = 0;
	};

}