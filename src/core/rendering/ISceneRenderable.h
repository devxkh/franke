#pragma once

#include "../EngineConfig.h"

namespace FE
{
	class GraphicsRenderer;

	class FE_API  ISceneRenderable {
	public:
		virtual void updateUniformBuffer() = 0;
		virtual void drawUniformBuffer(GraphicsRenderer* gr, Cmd* cmd) = 0;
		virtual bool load() = 0;
		virtual bool init() = 0;

		//--------------------------------------------------------------------------------------------
		// INIT FUNCTIONS
		//--------------------------------------------------------------------------------------------
		virtual void CreateRasterizerStates(Renderer* pRenderer) = 0;
		virtual void CreateDepthStates(Renderer* pRenderer) = 0;
		virtual void CreateBlendStates() = 0;
		virtual void CreateSamplers(Renderer* pRenderer) = 0;

		virtual void CreateShaders(Renderer* pRenderer) = 0;
		virtual void CreateRootSignatures(Renderer* pRenderer) = 0;

		virtual void CreatePBRMaps() = 0;
		virtual void LoadModels() = 0;
		virtual void LoadTextures() = 0;
		virtual void CreateResources() = 0;
		virtual void CreateUniformBuffers() = 0;

		virtual void InitializeUniformBuffers() = 0;

		//--------------------------------------------------------------------------------------------
		// LOAD FUNCTIONS
		//--------------------------------------------------------------------------------------------
		virtual void CreatePipelines(Renderer* pRenderer, SwapChain*	pSwapChain, RenderTarget* pRenderTargetDepth) = 0;
		virtual void DestroyPipelines() = 0;


		virtual void DestroyUniformBuffers() = 0;
		virtual void DestroyResources() = 0;
		virtual void DestroyTextures() = 0;
		virtual void DestroyModels() = 0;
		virtual void DestroyPBRMaps() = 0;

		virtual void DestroyRootSignatures() = 0;
		virtual void DestroyShaders() = 0;

		virtual void DestroySamplers() = 0;
		virtual void DestroyBlendStates() = 0;
		virtual void DestroyDepthStates() = 0;
		virtual void DestroyRasterizerStates() = 0;
	
		virtual void update(float deltatime, mat4& viewMat, mat4& projMat) = 0;

		tinystl::string mName;	
		mat4 mTranslationMat;
		mat4 mScaleMat;
		mat4 mSharedMat;	


		bool isVisible = true;
		bool isLoaded = false;
	};

}