#pragma once

#include "../EngineConfig.h"
#include "../IResource.h"
#include <string>

namespace FE
{
	class IApp;

	class FE_API IRenderableScene : public IResource
	{
	public:

		virtual void update(float deltaTime) = 0;


	protected:


	};

} // namespace FE