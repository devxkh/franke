#pragma once

#include "../EngineConfig.h"

namespace FE
{
	class  MaterialData
	{
	public:
		MaterialData() {}

		const char *MaterialName;
		float emmissiveFactor[3] = { 0.0f, 0.0f, 0.0f };
		float baseColourFactor[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float metallicFactor = 1.0f;
		float roughnessFactor = 1.0f;

		int Pbrmetallicroughness_Basecolortexture;
		int Pbrmetallicroughness_Metallicroughnesstexture;
		int Normaltexture;
		int Occlusiontexture;
		int Emissivetexture;
	};

	class  TextureData
	{
	public:
		TextureData() {}

		const void *pSysMem;
		size_t dataSize;
		unsigned int imgWidth;
		unsigned int imgHeight;
		unsigned int idx;
		unsigned int type;
	};

	class  TransformData
	{
	public:
		TransformData()
		{
			rotation[0] = rotation[1] = rotation[2] = rotation[3] = 0.0;
			translation[0] = translation[1] = translation[2] = 0.0;
			scale[0] = scale[1] = scale[2] = 1.0;
		}

		double rotation[4];
		double translation[3];
		double scale[3];
		double matrix[16];

		bool hasMatrix;
	};

	class  SceneNodeData
	{
	public:
		SceneNodeData() {}

		const char *Name;
		bool isMesh;
		int nodeIndex;
		int parentIndex;
	};

	struct  SubresourceData
	{
		const void *pSysMem;
		unsigned int SysMemPitch;
		unsigned int SysMemSlicePitch;
		unsigned int accessorIdx;
	};

	struct  BufferDesc
	{
		size_t ByteWidth;
		unsigned int BindFlags;
		unsigned int CPUAccessFlags;
		unsigned int MiscFlags;
		size_t StructureByteStride;
		const char *BufferContentType;
		size_t Count;
	};

	class  BufferData
	{
	public:
		BufferData();
		SubresourceData subresource;
		BufferDesc desc;
	};


}