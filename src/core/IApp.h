#pragma once

#include "../thirdparty/theforge/Common_3/ThirdParty/OpenSource/TinySTL/string.h"

#include "EngineConfig.h"

#include "IInputSystem.h"
#include "ResourceCache.h"

#include "IState.h"
#include <vector>
#include <memory>
#include <stack>
#include <chrono>
#include "IMiddleware.h"

typedef struct WindowsDesc;

namespace FE
{

	class FE_API IApp
	{
	public:
		IApp(std::unique_ptr<IInputSystem> inputSystem)
			: mInputSystem(std::move(inputSystem))
		{
		}

		virtual ~IApp()
		{		
		}
		
		virtual bool Init() = 0;
		virtual void Exit();

		virtual bool Load() = 0;
		virtual void Unload() = 0;

		virtual void Update(float deltaTime);

		virtual void Draw() = 0;

		virtual tinystl::string GetName() = 0;

		void run(std::unique_ptr< IState > state);
		void setNextState(std::unique_ptr<IState> state);
		void nextState();
		void lastState();
		void draw();

		IState* getCurrentState() {
			return mStates.top().get();
		}

		struct Settings
		{
			/// Window width
			int32_t mWidth = -1;
			/// Window height
			int32_t mHeight = -1;
			/// Set to true if fullscreen mode has been requested
			bool	mFullScreen = false;
			/// Set to true if app wants to use an external window
			bool	mExternalWindow = false;
		} mSettings;

		WindowsDesc* pWindow;
		tinystl::string mCommandLine;

		std::unique_ptr<IInputSystem> mInputSystem;

		std::vector<std::unique_ptr<IMiddleware>> mMiddlewares;
		std::stack<std::unique_ptr<IState>> mStates;

		ResourceCache mResourceCache;

		bool mResume = false;

		template < typename T >
		std::unique_ptr< T > build(IApp& engine, bool replace = true);

	private:

		std::chrono::steady_clock::time_point mTimeStart;
	};


	template < typename T >
	std::unique_ptr< T > IApp::build(IApp& engine, bool replace)
	{
		return std::move(std::unique_ptr< T >(new T(engine, replace)));
	}
}

#if defined(_DURANGO)
#define DEFINE_APPLICATION_MAIN(appClass)					   \
extern int DurangoMain(int argc, char** argv, IApp* app);	   \
																\
int main(int argc, char** argv)								 \
{															   \
	appClass app;											   \
	return DurangoMain(argc, argv, &app);					   \
}
#elif defined(_WIN32)
#define DEFINE_APPLICATION_MAIN(appClass)					   \
extern int WindowsMain(int argc, char** argv, FE::IApp* app);	   \
																\
int main(int argc, char** argv)								 \
{															   \
	appClass app;											   \
	return WindowsMain(argc, argv, &app);					   \
}
#elif defined(TARGET_IOS)
#define DEFINE_APPLICATION_MAIN(appClass)					   \
extern int iOSMain(int argc, char** argv, IApp* app);		   \
																\
int main(int argc, char** argv)								 \
{															   \
	appClass app;											   \
	return iOSMain(argc, argv, &app);						   \
}
#elif defined(__APPLE__)
#define DEFINE_APPLICATION_MAIN(appClass)					   \
extern int macOSMain(int argc, const char** argv, IApp* app);   \
																\
int main(int argc, const char* argv[])						\
{															   \
	appClass app;											   \
	return macOSMain(argc, argv, &app);						 \
}
#elif defined(__ANDROID__)
#define DEFINE_APPLICATION_MAIN(appClass)					   \
extern int AndroidMain(void* param, IApp* app);				 \
																\
void android_main(struct android_app* param)					\
{															   \
	appClass app;											   \
	AndroidMain(param, &app);								   \
}

#elif defined(__linux__)
#define DEFINE_APPLICATION_MAIN(appClass)					   \
extern int LinuxMain(int argc, char** argv, IApp* app);		 \
																\
int main(int argc, char** argv)								 \
{															   \
	appClass app;											   \
	return LinuxMain(argc, argv, &app);						 \
}
#else
#endif
