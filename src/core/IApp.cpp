#include "IApp.h"



#include <chrono>

namespace FE
{
	void IApp::Update(float deltaTime) {

		/*auto delta_time = std::chrono::steady_clock::now() - mTimeStart;
		mTimeStart = std::chrono::steady_clock::now();
		float deltaTime = static_cast<float>(std::chrono::duration_cast<std::chrono::nanoseconds>(delta_time).count());*/

		mStates.top()->update(deltaTime);

		for each (const std::unique_ptr<IMiddleware>& middleware in mMiddlewares)
		{
			middleware->update(deltaTime);
		}
	}

	void IApp::run(std::unique_ptr< IState > state) {

		mStates.push(std::move(state));	
	}

	void IApp::setNextState(std::unique_ptr<IState> state) {

		mStates.top()->m_next = std::move(state);
	}

	void IApp::nextState()  {
		if (mResume)
		{
			// cleanup the current state
			if (!mStates.empty())
			{
				mStates.pop();
			}

			// resume previous state
			if (!mStates.empty())
			{
				mStates.top()->resume();
			}

			mResume = false;
		}

		// there needs to be a state
		if (!mStates.empty())
		{
			std::unique_ptr<IState> temp = mStates.top()->next();

			// only change states if there's a next one existing
			if (temp != nullptr)
			{
				// replace the running state
				if (temp->isReplacing())
					mStates.pop();
				// pause the running state
				else
					mStates.top()->pause();

				mStates.push(std::move(temp));
			}
		}

	}

	void IApp::lastState() {

		mResume = true;

	}

	void IApp::draw() {

		mStates.top()->draw();
	}


	void IApp::Exit() {

	}

}
