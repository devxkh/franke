#pragma once

#include "EngineConfig.h"

#include <stdint.h>
#include <cstdlib>

#ifdef _WINDOWS

typedef struct tagMSG MSG;

#elif defined __ANDROID__

struct AInputEvent;

#elif defined(__linux__)

union _XEvent;
typedef _XEvent XEvent;

#endif

namespace FE
{

	class IInputSystem 
	{

	public:
		struct InputPoint {
			float PosX;
			float PosY;
			float DeltaX;
			float DeltaY;
		};

		// mandatory eventids used by system
		enum InputEvent {
			// ------ mandatory eventids used by system --------
			UNKNOWN,

			// Exit application
			EXIT,

			//	Maps to left click of mouse 
			POINT_MOVE,
			POINT_MOVEDX,
			POINT_MOVEDY,

			TOGGLE_FULLSCREEN,
			COUNT
		};

		virtual ~IInputSystem() { }

		virtual void init(uint32_t width, uint32_t height) = 0;

		virtual void update() = 0;

		virtual void destroy() = 0;

		virtual bool isTriggered(uint16_t inputEventId) = 0;

		virtual bool isActive(uint16_t inputEventId) = 0;

		virtual void updateSize(uint32_t width, uint32_t height) = 0;

		virtual void textInputEnabled(bool enabled) = 0;
		
		virtual char getNextCharacter() = 0;
		
		virtual void getPoint(InputPoint& point) = 0;
		
		virtual void setPointCapture(bool mouseCapture) = 0;

		virtual bool isMouseCaptured() = 0;

#ifdef _WINDOWS
		virtual void handleMessage(MSG& msg) = 0;
#elif defined __ANDROID__
		virtual int32_t handleMessage(AInputEvent* msg) = 0;
#elif defined(__linux__)
		virtual void handleMessage(XEvent& msg) = 0;
#endif
	};

}