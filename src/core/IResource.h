#pragma once

#include "EngineConfig.h"
#include "../core/rendering/ISceneAssetData.h"

#include <string>

namespace FE
{

	class IResource
	{
	public:
		IResource() {}
		IResource(const IResource&) = delete;
		IResource& operator=(const IResource&) = delete;

		virtual void load(uint32_t id) = 0;

		virtual void transformCallback(const TransformData& data) = 0;
		virtual void sceneNodeCallback(const SceneNodeData& data) = 0;
		virtual void textureCallback(const TextureData& data) = 0;
		virtual void materialCallback(const MaterialData& data) = 0;
		virtual void bufferCallback(const BufferData& data) = 0;

		uint8_t type;
		uint32_t id;

		std::string name;

	protected:


	};

} // namespace FE