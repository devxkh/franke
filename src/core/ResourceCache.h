#pragma once

#include "EngineConfig.h"
#include "IResource.h"

#include <string>

#include <unordered_map>
#include <assert.h>
#include <memory>

namespace FE
{
	enum ResourceType : uint8_t
	{
		RT_Unkown,
		RT_Scene,
		RT_Mesh,
		RT_Material,
		RT_Texture,
		RT_Shader,
		RT_Audio,
		RT_AI
	};


	class FE_API ResourceCache {

	public:

		ResourceCache();
		~ResourceCache()
		{
			resources.clear();
		}

		ResourceCache(const ResourceCache&) = delete;
		ResourceCache& operator=(const ResourceCache&) = delete;

		void loadResourceFile(IResource* resource);

		void add(std::unique_ptr<IResource> resource)
		{
			resources[resource->type][resource->id] = std::move(resource);

		};

		template <class T> T* get(uint8_t type, uint32_t id)
		{
			//auto rp1 = resources[type];
			auto typePos = resources.find(type);
			if (typePos == resources.end()) {
				//handle the error
			}
			else {
				 auto typeMap = &typePos->second;

				auto pos = typeMap->find(id);
				if (pos == typeMap->end()) {
					//handle the error
				}
				else {
					auto test = pos->second.get();

					loadResourceFile(test);

					return static_cast<T*>(test);
				}
			}
			//	auto rp2 = rp1[id];



			return 0;

		}

	private:
		// type/ResourceId/IResource
		std::unordered_map <uint8_t, std::unordered_map<uint32_t, std::unique_ptr<IResource> >> resources;

	};

}