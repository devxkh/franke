
#pragma once

////////////////////////////////////////////////////////////
// Define portable import / export macros
////////////////////////////////////////////////////////////

#define FE_API_EXPORT  __declspec(dllexport)//extern c important for c# and c++ compatibility
#define FE_API_IMPORT  __declspec(dllimport) //extern c important for c# and c++ compatibility

#if defined(FE_EXPORT)
#define FE_API FE_API_EXPORT
#else
#define FE_API FE_API_IMPORT
#endif

// Disable annoying MSVC++ warning
#ifdef _MSC_VER
#pragma warning(disable: 4251)
#endif // _MSC_VER