#pragma once

#include "EngineConfig.h"
#include "IMiddleware.h"

namespace FE
{

	class FE_API IDAL
	{

	public:


		virtual void init() = 0;

		virtual void update(float delta) = 0;

		virtual void destroy() = 0;
	};

}