#pragma once

#include "../EngineConfig.h"

namespace FE
{
	class FE_API NonCopyable
	{
	protected:

		NonCopyable() {}

		~NonCopyable() {}

	private:

		NonCopyable(const NonCopyable&);

		NonCopyable& operator =(const NonCopyable&);
	};

} // namespace FE