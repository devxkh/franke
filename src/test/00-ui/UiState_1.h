#pragma once

#include "../../core/IState.h"

class UiState_1 : public FE::IState
{
public:
	UiState_1(FE::IApp& app, bool replace = true): FE::IState(app, replace)
	{
	}

	void init();


	bool isRunning = true;

	//~UiState_1();

	void pause() {};
	void resume() {};

	void update(float deltaTime);
	void draw() {};

};