
#include "../../core/engine.h"

#include "../00-ui/UiState_1.h"

//#include "../../middleware/network_fe/src/YojimboMiddleware.h"

#include <iostream>

//#include "../../middleware/yojimbo/src/yojimbo.h"
#include <signal.h>
#include <time.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <time.h>
#include <memory>
#include "../../middleware/renderer_fe/src/GraphicsRenderer.h"

//using namespace yojimbo;
//
//const uint64_t ProtocolId = 0x11223344556677ULL;
//
//const int ClientPort = 30000;
//const int ServerPort = 40000;
//
//inline int GetNumBitsForMessage(uint16_t sequence)
//{
//	static int messageBitsArray[] = { 1, 320, 120, 4, 256, 45, 11, 13, 101, 100, 84, 95, 203, 2, 3, 8, 512, 5, 3, 7, 50 };
//	const int modulus = sizeof(messageBitsArray) / sizeof(int);
//	const int index = sequence % modulus;
//	return messageBitsArray[index];
//}
//
//struct TestMessage : public Message
//{
//	uint16_t sequence;
//
//	TestMessage()
//	{
//		sequence = 0;
//	}
//
//	template <typename Stream> bool Serialize(Stream & stream)
//	{
//		serialize_bits(stream, sequence, 16);
//
//		int numBits = GetNumBitsForMessage(sequence);
//		int numWords = numBits / 32;
//		uint32_t dummy = 0;
//		for (int i = 0; i < numWords; ++i)
//			serialize_bits(stream, dummy, 32);
//		int numRemainderBits = numBits - numWords * 32;
//		if (numRemainderBits > 0)
//			serialize_bits(stream, dummy, numRemainderBits);
//
//		return true;
//	}
//
//	YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
//};
//
//struct TestBlockMessage : public BlockMessage
//{
//	uint16_t sequence;
//
//	TestBlockMessage()
//	{
//		sequence = 0;
//	}
//
//	template <typename Stream> bool Serialize(Stream & stream)
//	{
//		serialize_bits(stream, sequence, 16);
//		return true;
//	}
//
//	YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
//};
//
//struct TestSerializeFailOnReadMessage : public Message
//{
//	template <typename Stream> bool Serialize(Stream & /*stream*/)
//	{
//		return !Stream::IsReading;
//	}
//
//	YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
//};
//
//struct TestExhaustStreamAllocatorOnReadMessage : public Message
//{
//	template <typename Stream> bool Serialize(Stream & stream)
//	{
//		if (Stream::IsReading)
//		{
//			const int NumBuffers = 100;
//
//			void * buffers[NumBuffers];
//
//			memset(buffers, 0, sizeof(buffers));
//
//			for (int i = 0; i < NumBuffers; ++i)
//			{
//				buffers[i] = YOJIMBO_ALLOCATE(stream.GetAllocator(), 1024 * 1024);
//			}
//
//			for (int i = 0; i < NumBuffers; ++i)
//			{
//				YOJIMBO_FREE(stream.GetAllocator(), buffers[i]);
//			}
//		}
//
//		return true;
//	}
//
//	YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
//};
//
//enum TestMessageType
//{
//	TEST_MESSAGE,
//	TEST_BLOCK_MESSAGE,
//	TEST_SERIALIZE_FAIL_ON_READ_MESSAGE,
//	TEST_EXHAUST_STREAM_ALLOCATOR_ON_READ_MESSAGE,
//	NUM_TEST_MESSAGE_TYPES
//};
//
//YOJIMBO_MESSAGE_FACTORY_START(TestMessageFactory, NUM_TEST_MESSAGE_TYPES);
//YOJIMBO_DECLARE_MESSAGE_TYPE(TEST_MESSAGE, TestMessage);
//YOJIMBO_DECLARE_MESSAGE_TYPE(TEST_BLOCK_MESSAGE, TestBlockMessage);
//YOJIMBO_DECLARE_MESSAGE_TYPE(TEST_SERIALIZE_FAIL_ON_READ_MESSAGE, TestSerializeFailOnReadMessage);
//YOJIMBO_DECLARE_MESSAGE_TYPE(TEST_EXHAUST_STREAM_ALLOCATOR_ON_READ_MESSAGE, TestExhaustStreamAllocatorOnReadMessage);
//YOJIMBO_MESSAGE_FACTORY_FINISH();
//
//class TestAdapter : public Adapter
//{
//public:
//
//	MessageFactory * CreateMessageFactory(Allocator & allocator)
//	{
//		return YOJIMBO_NEW(allocator, TestMessageFactory, allocator);
//	}
//};
//
//static TestAdapter adapter;
//
//
//using namespace yojimbo;
//
//static volatile int quit = 0;
//
//void interrupt_handler(int /*dummy*/)
//{
//	quit = 1;
//}
//
//int ServerMain()
//{
//	printf("started server on port %d (insecure)\n", ServerPort);
//
//	double time = 100.0;
//
//	ClientServerConfig config;
//
//	uint8_t privateKey[KeyBytes];
//	memset(privateKey, 0, KeyBytes);
//
//	Server server(GetDefaultAllocator(), privateKey, Address("127.0.0.1", ServerPort), config, adapter, time);
//
//	server.Start(MaxClients);
//
//	char addressString[256];
//	server.GetAddress().ToString(addressString, sizeof(addressString));
//	printf("server address is %s\n", addressString);
//
//	const double deltaTime = 0.01f;
//
//	signal(SIGINT, interrupt_handler);
//
//	while (!quit)
//	{
//		server.SendPackets();
//
//		server.ReceivePackets();
//
//		time += deltaTime;
//
//		server.AdvanceTime(time);
//
//		if (!server.IsRunning())
//			break;
//
//		yojimbo_sleep(deltaTime);
//	}
//
//	server.Stop();
//
//	return 0;
//}
//
//
//int ClientServerMain()
//{
//	double time = 100.0;
//
//	ClientServerConfig config;
//
//	uint8_t privateKey[KeyBytes];
//	memset(privateKey, 0, KeyBytes);
//
//	printf("starting server on port %d\n", ServerPort);
//
//	Server server(GetDefaultAllocator(), privateKey, Address("127.0.0.1", ServerPort), config, adapter, time);
//
//	server.Start(MaxClients);
//
//	if (!server.IsRunning())
//		return 1;
//
//	printf("started server\n");
//
//	uint64_t clientId = 0;
//	random_bytes((uint8_t*)&clientId, 8);
//	printf("client id is %.16" PRIx64 "\n", clientId);
//
//	Client client(GetDefaultAllocator(), Address("0.0.0.0"), config, adapter, time);
//
//	Address serverAddress("127.0.0.1", ServerPort);
//
//	client.InsecureConnect(privateKey, clientId, serverAddress);
//
//	const double deltaTime = 0.1;
//
//	signal(SIGINT, interrupt_handler);
//
//	while (!quit)
//	{
//		server.SendPackets();
//		client.SendPackets();
//
//		server.ReceivePackets();
//		client.ReceivePackets();
//
//		time += deltaTime;
//
//		client.AdvanceTime(time);
//
//		if (client.IsDisconnected())
//			break;
//
//		time += deltaTime;
//
//		server.AdvanceTime(time);
//
//		yojimbo_sleep(deltaTime);
//	}
//
//	client.Disconnect();
//	server.Stop();
//
//	return 0;
//}


//int main() {
//
//	if (!InitializeYojimbo())
//	{
//		printf("error: failed to initialize Yojimbo!\n");
//		return 1;
//	}
//
//	yojimbo_log_level(YOJIMBO_LOG_LEVEL_INFO);
//
//	srand((unsigned int)time(NULL));
//
//	int result = ClientServerMain();
//
//	ShutdownYojimbo();
//
//
//	FE::Engine engine;
//
//	std::unique_ptr<YojimboMiddleware> mw1(new YojimboMiddleware());
//	engine.middlewares.push_back(std::move(mw1));
//	
//	engine.init();
//	engine.run(engine.build<UiState_1>(engine, true));
//
//	while (engine.m_isRunning)
//	{
//
//		engine.update();
//	}
//}
//tiny stl
#include "../../thirdparty/theforge/Common_3/ThirdParty/OpenSource/TinySTL/vector.h"
#include "../../thirdparty/theforge/Common_3/ThirdParty/OpenSource/TinySTL/string.h"

//Interfaces
#include "../../thirdparty/theforge/Common_3/OS/Interfaces/ICameraController.h"
//#include "../../thirdparty/theforge/Common_3/OS/Interfaces/IApp.h"
//#include "../../thirdparty/theforge/Common_3/OS/Interfaces/ILogManager.h"
//#include "../../thirdparty/theforge/Common_3/OS/Interfaces/IFileSystem.h"
//#include "../../thirdparty/theforge/Common_3/OS/Interfaces/ITimeManager.h"
//#include "../../thirdparty/theforge/Middleware_3/UI/AppUI.h"

//#include "../../thirdparty/theforge/Common_3/Renderer/ResourceLoader.h"

//#include "../../middleware/theforge/Middleware_3/Input/InputSystem.h"
//#include "../../middleware/theforge/Middleware_3/Input/InputMappings.h"
//Math
//#include "../../thirdparty/theforge/Common_3/OS/Math/MathTypes.h"

//#include "../../thirdparty/theforge/Common_3/OS/Interfaces/IMemoryManager.h"

#define MAX_PLANETS 20 // Does not affect test, just for allocating space in uniform block. Must match with shader.


/// Demo structures
struct PlanetInfoStruct
{
	uint mParentIndex;
	vec4 mColor;
	float mYOrbitSpeed; // Rotation speed around parent
	float mZOrbitSpeed;
	float mRotationSpeed; // Rotation speed around self
	mat4 mTranslationMat;
	mat4 mScaleMat;
	mat4 mSharedMat;	// Matrix to pass down to children
};

//struct UniformBlock
//{
//	mat4 mProjectView;
//	mat4 mToWorldMat[MAX_PLANETS];
//	vec4 mColor[MAX_PLANETS];
//
//	// Point Light Information
//	vec3 mLightPosition;
//	vec3 mLightColor;
//};

const int		   gSphereResolution = 30; // Increase for higher resolution spheres
const float		 gSphereDiameter = 0.5f;
const uint		  gNumPlanets = 11;	  // Sun, Mercury -> Neptune, Pluto, Moon
const uint		  gTimeOffset = 600000;   // For visually better starting locations
const float		 gRotSelfScale = 0.0004f;
const float		 gRotOrbitYScale = 0.001f;
const float		 gRotOrbitZScale = 0.00001f;


//RenderTarget*	   pDepthBuffer = NULL;

//Shader*			 pSphereShader = NULL;
//Buffer*			 pSphereVertexBuffer = NULL;
//Pipeline*		   pSpherePipeline = NULL;

#if defined(TARGET_IOS) || defined(__ANDROID__)
VirtualJoystickUI   gVirtualJoystick;
#endif

int					gNumberOfSpherePoints;
//PlanetInfoStruct	gPlanetInfoData[gNumPlanets];

//FileSystem		  gFileSystem;
//LogManager        gLogManager;


const char* pszBases[] =
{
	"../../franke/src/test/00-ui/", // FSR_BinShaders
	"../../franke/src/test/00-ui/", // FSR_SrcShaders
	"../../franke/data/assets/UnitTestResources/",		// FSR_Textures
	"../../franke/data/assets/UnitTestResources/",		// FSR_Meshes
	"../../franke/data/assets/UnitTestResources/",		// FSR_Builtin_Fonts
	"../../franke/data/assets/UnitTestResources/",		// FSR_GpuConfig
	"../../franke/data/assets/UnitTestResources/",		// FSR_Animtion
	"",													// FSR_OtherFiles
	"../../franke/src/thirdparty/theforge/Middleware_3/Text/",	// FSR_MIDDLEWARE_TEXT
	"../../franke/src/thirdparty/theforge/Middleware_3/UI/",	// FSR_MIDDLEWARE_UI
};


ICameraController*  pCameraController = NULL;

/// UI
//UIApp			   gAppUI;


#include "../../middleware/renderer_fe/src/Camera.h"
#include "../../core/IApp.h"
#include "../../middleware/input_fe/src/InputSystem.h"
#include "../engine_test/components/Controller.h"
#include "../engine_test/systems/ControllerSystem.hpp"
#include "../engine_test/core/Scene.h"
#include "../engine_test/components/Controller.h"
#include "../engine_test/components/CameraFree.h"

#include "../engine_test/systems/FreeLookCameraSystem.h"

bool					gToggleVSync = true;

//GuiComponent*		   pGui;

class UserInterface  : public FE::IApp
{
public:
	FE::GraphicsRenderer m_gr;
	FE::Scene mScene;
	FE::Entity* mPlayer1;

	UserInterface() :
		FE::IApp(std::unique_ptr<FE::InputSystem>(new FE::InputSystem())),
		m_gr(GetName()),
		mScene(*this, m_gr)
	{
	}

	~UserInterface() {

	}

	bool Init()
	{
		m_gr.pWindow = pWindow; // todo move to graphicsmanager/windowmgr?
		m_gr.m_settings.mWidth = mSettings.mWidth;
		m_gr.m_settings.mHeight = mSettings.mHeight;
				
	//	m_gr.m_ISceneItems.push_back(std::move(std::unique_ptr<SceneRenderable>(new SceneRenderable())));
		m_gr.m_cameras.push_back(std::move(std::unique_ptr<FE::Camera>(new FE::Camera("MainCamera", &m_gr))));

		m_gr.init();

		run(build<UiState_1>(*this, true));
		
		mPlayer1 = mScene.mECS->create();

		auto& camera = mPlayer1->assign<FE::CameraFreeComponent>(static_cast<FE::InputSystem*>(mInputSystem.get()));
		auto& controller = mPlayer1->assign<FE::ControllerComponent>(static_cast<FE::InputSystem*>(mInputSystem.get()));

		controller->createActionmap();
		controller->setBasicInputEvents();
		controller->mControlledEntity = mPlayer1;
		
		mScene.mECS->registerSystem(new FE::FreeLookCameraSystem());
				
		FE::CameraMotionParameters cmp{ 160.0f, 600.0f, 200.0f };
		vec3 camPos{ 48.0f, 48.0f, 20.0f };
		vec3 lookAt{ 0 };

		camera->moveTo(camPos);
		camera->lookAt(lookAt);
		requestMouseCapture(true);
		camera->setMotionParameters(cmp);

		mScene.load(0);

	//	pCameraController = createFpsCameraController(camPos, lookAt);
//		requestMouseCapture(true);

	//	pCameraController->setMotionParameters(cmp);




			//return false;


		//ShaderLoadDesc basicShader = {};
		//basicShader.mStages[0] = { "basic.vert", NULL, 0, FSR_SrcShaders };
		//basicShader.mStages[1] = { "basic.frag", NULL, 0, FSR_SrcShaders };

		//addShader(pRenderer, &basicShader, &pSphereShader);

		//RasterizerStateDesc sphereRasterizerStateDesc = {};
		//sphereRasterizerStateDesc.mCullMode = CULL_MODE_FRONT;
		//addRasterizerState(pRenderer, &sphereRasterizerStateDesc, &pSphereRast);

		//DepthStateDesc depthStateDesc = {};
		//depthStateDesc.mDepthTest = true;
		//depthStateDesc.mDepthWrite = true;
		//depthStateDesc.mDepthFunc = CMP_LEQUAL;
		//addDepthState(pRenderer, &depthStateDesc, &pDepth);

		//// Generate sphere vertex buffer
		//float* pSpherePoints;
		//generateSpherePoints(&pSpherePoints, &gNumberOfSpherePoints, gSphereResolution, gSphereDiameter);

		//uint64_t sphereDataSize = gNumberOfSpherePoints * sizeof(float);
		//BufferLoadDesc sphereVbDesc = {};
		//sphereVbDesc.mDesc.mDescriptors = DESCRIPTOR_TYPE_VERTEX_BUFFER;
		//sphereVbDesc.mDesc.mMemoryUsage = RESOURCE_MEMORY_USAGE_GPU_ONLY;
		//sphereVbDesc.mDesc.mSize = sphereDataSize;
		//sphereVbDesc.mDesc.mVertexStride = sizeof(float) * 6;
		//sphereVbDesc.pData = pSpherePoints;
		//sphereVbDesc.ppBuffer = &pSphereVertexBuffer;
		//addResource(&sphereVbDesc);

		//// Need to free memory;
		//conf_free(pSpherePoints);

		




		// Setup planets (Rotation speeds are relative to Earth's, some values randomly given)

		// Sun
		//gPlanetInfoData[0].mParentIndex = 0;
		//gPlanetInfoData[0].mYOrbitSpeed = 0; // Earth years for one orbit
		//gPlanetInfoData[0].mZOrbitSpeed = 0;
		//gPlanetInfoData[0].mRotationSpeed = 24.0f; // Earth days for one rotation
		//gPlanetInfoData[0].mTranslationMat = mat4::identity();
		//gPlanetInfoData[0].mScaleMat = mat4::scale(vec3(10.0f));
		//gPlanetInfoData[0].mColor = vec4(0.9f, 0.6f, 0.1f, 0.0f);

		//// Mercury
		//gPlanetInfoData[1].mParentIndex = 0;
		//gPlanetInfoData[1].mYOrbitSpeed = 0.5f;
		//gPlanetInfoData[1].mZOrbitSpeed = 0.0f;
		//gPlanetInfoData[1].mRotationSpeed = 58.7f;
		//gPlanetInfoData[1].mTranslationMat = mat4::translation(vec3(10.0f, 0, 0));
		//gPlanetInfoData[1].mScaleMat = mat4::scale(vec3(1.0f));
		//gPlanetInfoData[1].mColor = vec4(0.7f, 0.3f, 0.1f, 1.0f);

		//// Venus
		//gPlanetInfoData[2].mParentIndex = 0;
		//gPlanetInfoData[2].mYOrbitSpeed = 0.8f;
		//gPlanetInfoData[2].mZOrbitSpeed = 0.0f;
		//gPlanetInfoData[2].mRotationSpeed = 243.0f;
		//gPlanetInfoData[2].mTranslationMat = mat4::translation(vec3(20.0f, 0, 5));
		//gPlanetInfoData[2].mScaleMat = mat4::scale(vec3(2));
		//gPlanetInfoData[2].mColor = vec4(0.8f, 0.6f, 0.1f, 1.0f);

		//// Earth
		//gPlanetInfoData[3].mParentIndex = 0;
		//gPlanetInfoData[3].mYOrbitSpeed = 1.0f;
		//gPlanetInfoData[3].mZOrbitSpeed = 0.0f;
		//gPlanetInfoData[3].mRotationSpeed = 1.0f;
		//gPlanetInfoData[3].mTranslationMat = mat4::translation(vec3(30.0f, 0, 0));
		//gPlanetInfoData[3].mScaleMat = mat4::scale(vec3(4));
		//gPlanetInfoData[3].mColor = vec4(0.3f, 0.2f, 0.8f, 1.0f);

		//// Mars
		//gPlanetInfoData[4].mParentIndex = 0;
		//gPlanetInfoData[4].mYOrbitSpeed = 2.0f;
		//gPlanetInfoData[4].mZOrbitSpeed = 0.0f;
		//gPlanetInfoData[4].mRotationSpeed = 1.1f;
		//gPlanetInfoData[4].mTranslationMat = mat4::translation(vec3(40.0f, 0, 0));
		//gPlanetInfoData[4].mScaleMat = mat4::scale(vec3(3));
		//gPlanetInfoData[4].mColor = vec4(0.9f, 0.3f, 0.1f, 1.0f);

		//// Jupiter
		//gPlanetInfoData[5].mParentIndex = 0;
		//gPlanetInfoData[5].mYOrbitSpeed = 11.0f;
		//gPlanetInfoData[5].mZOrbitSpeed = 0.0f;
		//gPlanetInfoData[5].mRotationSpeed = 0.4f;
		//gPlanetInfoData[5].mTranslationMat = mat4::translation(vec3(50.0f, 0, 0));
		//gPlanetInfoData[5].mScaleMat = mat4::scale(vec3(8));
		//gPlanetInfoData[5].mColor = vec4(0.6f, 0.4f, 0.4f, 1.0f);

		//// Saturn
		//gPlanetInfoData[6].mParentIndex = 0;
		//gPlanetInfoData[6].mYOrbitSpeed = 29.4f;
		//gPlanetInfoData[6].mZOrbitSpeed = 0.0f;
		//gPlanetInfoData[6].mRotationSpeed = 0.5f;
		//gPlanetInfoData[6].mTranslationMat = mat4::translation(vec3(60.0f, 0, 0));
		//gPlanetInfoData[6].mScaleMat = mat4::scale(vec3(6));
		//gPlanetInfoData[6].mColor = vec4(0.7f, 0.7f, 0.5f, 1.0f);

		//// Uranus
		//gPlanetInfoData[7].mParentIndex = 0;
		//gPlanetInfoData[7].mYOrbitSpeed = 84.07f;
		//gPlanetInfoData[7].mZOrbitSpeed = 0.0f;
		//gPlanetInfoData[7].mRotationSpeed = 0.8f;
		//gPlanetInfoData[7].mTranslationMat = mat4::translation(vec3(70.0f, 0, 0));
		//gPlanetInfoData[7].mScaleMat = mat4::scale(vec3(7));
		//gPlanetInfoData[7].mColor = vec4(0.4f, 0.4f, 0.6f, 1.0f);

		//// Neptune
		//gPlanetInfoData[8].mParentIndex = 0;
		//gPlanetInfoData[8].mYOrbitSpeed = 164.81f;
		//gPlanetInfoData[8].mZOrbitSpeed = 0.0f;
		//gPlanetInfoData[8].mRotationSpeed = 0.9f;
		//gPlanetInfoData[8].mTranslationMat = mat4::translation(vec3(80.0f, 0, 0));
		//gPlanetInfoData[8].mScaleMat = mat4::scale(vec3(8));
		//gPlanetInfoData[8].mColor = vec4(0.5f, 0.2f, 0.9f, 1.0f);

		//// Pluto - Not a planet XDD
		//gPlanetInfoData[9].mParentIndex = 0;
		//gPlanetInfoData[9].mYOrbitSpeed = 247.7f;
		//gPlanetInfoData[9].mZOrbitSpeed = 1.0f;
		//gPlanetInfoData[9].mRotationSpeed = 7.0f;
		//gPlanetInfoData[9].mTranslationMat = mat4::translation(vec3(90.0f, 0, 0));
		//gPlanetInfoData[9].mScaleMat = mat4::scale(vec3(1.0f));
		//gPlanetInfoData[9].mColor = vec4(0.7f, 0.5f, 0.5f, 1.0f);

		//// Moon
		//gPlanetInfoData[10].mParentIndex = 3;
		//gPlanetInfoData[10].mYOrbitSpeed = 1.0f;
		//gPlanetInfoData[10].mZOrbitSpeed = 200.0f;
		//gPlanetInfoData[10].mRotationSpeed = 27.0f;
		//gPlanetInfoData[10].mTranslationMat = mat4::translation(vec3(5.0f, 0, 0));
		//gPlanetInfoData[10].mScaleMat = mat4::scale(vec3(1));
		//gPlanetInfoData[10].mColor = vec4(0.3f, 0.3f, 0.4f, 1.0f);
		

		
	//	GuiDesc guiDesc = {};
		/*pGui = gAppUI.AddGuiComponent(GetName(), &guiDesc);

#if !defined(TARGET_IOS) && !defined(_DURANGO)
		pGui->AddWidget(CheckboxWidget("Toggle VSync", &gToggleVSync));
#endif*/

		//CameraMotionParameters cmp{ 160.0f, 600.0f, 200.0f };
		//vec3 camPos{ 48.0f, 48.0f, 20.0f };
		//vec3 lookAt{ 0 };

		//pCameraController = createFpsCameraController(camPos, lookAt);
		//requestMouseCapture(true);

		//pCameraController->setMotionParameters(cmp);


		//InputSystem::RegisterInputEvent(cameraInputEvent);
		return true;
	}

	//void cameraInputEvent(const ButtonData* data)
	//{
	//	pCameraController->onInputEvent(data);
	//}

	void Exit()
	{
	/*	waitForFences(pGraphicsQueue, 1, &pRenderCompleteFences[gFrameIndex], true);

		destroyCameraController(pCameraController);

		removeDebugRendererInterface();

#if defined(TARGET_IOS) || defined(__ANDROID__)
		gVirtualJoystick.Exit();
#endif

		gAppUI.Exit();

		for (uint32_t i = 0; i < gImageCount; ++i)
		{
			removeResource(pProjViewUniformBuffer[i]);
			removeResource(pSkyboxUniformBuffer[i]);
		}
		removeResource(pSphereVertexBuffer);
		removeResource(pSkyBoxVertexBuffer);

		for (uint i = 0; i < 6; ++i)
			removeResource(pSkyBoxTextures[i]);

		removeSampler(pRenderer, pSamplerSkyBox);
		removeShader(pRenderer, pSphereShader);
		removeShader(pRenderer, pSkyBoxDrawShader);
		removeRootSignature(pRenderer, pRootSignature);

		removeDepthState(pDepth);
		removeRasterizerState(pSphereRast);
		removeRasterizerState(pSkyboxRast);

		for (uint32_t i = 0; i < gImageCount; ++i)
		{
			removeFence(pRenderer, pRenderCompleteFences[i]);
			removeSemaphore(pRenderer, pRenderCompleteSemaphores[i]);
		}
		removeSemaphore(pRenderer, pImageAcquiredSemaphore);

		removeCmd_n(pCmdPool, gImageCount, ppCmds);
		removeCmdPool(pRenderer, pCmdPool);

		removeResourceLoaderInterface(pRenderer);
		removeQueue(pGraphicsQueue);
		removeRenderer(pRenderer);*/
	}

	bool Load()
	{
		if(!m_gr.Load())
			return false;


		
//#if defined(TARGET_IOS) || defined(__ANDROID__)
//		if (!gVirtualJoystick.Load(pSwapChain->ppSwapchainRenderTargets[0], pDepthBuffer->mDesc.mFormat))
//			return false;
//#endif

		

		

		return true;
	}

	void Unload()
	{
		m_gr.Unload();
	/*	waitForFences(pGraphicsQueue, gImageCount, pRenderCompleteFences, true);

		gAppUI.Unload();

#if defined(TARGET_IOS) || defined(__ANDROID__)
		gVirtualJoystick.Unload();
#endif

		removePipeline(pRenderer, pSkyBoxDrawPipeline);
		removePipeline(pRenderer, pSpherePipeline);

		removeSwapChain(pRenderer, pSwapChain);
		removeRenderTarget(pRenderer, pDepthBuffer);*/
	}

	void Update(float deltaTime)
	{
		IApp::Update(deltaTime);

		if (mInputSystem->isTriggered(15))
			std::cout << "MouseButton0" << std::endl;

		if (mInputSystem->isTriggered(16))
			std::cout << "MouseButton1" << std::endl;


		if (mInputSystem->isTriggered(12))
			std::cout << "KEy6" << std::endl;
		if (mInputSystem->isTriggered(10))
		{
			std::cout << "Key2" << std::endl;
			FE::InputSystem::InputPoint point;
			mInputSystem->getPoint(point);
			std::cout << "Pointx: " << point.PosX << " Pointy: " << point.PosY << std::endl;
			std::cout << "PointDeltax: " << point.DeltaX << " PointDeltay: " << point.DeltaY << std::endl;

		}

		auto TextChar = mInputSystem->getNextCharacter();
		if(TextChar)
			std::cout << TextChar;

		
#if !defined(TARGET_IOS) && !defined(_DURANGO)
		if (m_gr.pSwapChain->mDesc.mEnableVsync != gToggleVSync)
		{
			m_gr.toggleVSync();
		}
#endif

		/************************************************************************/
		// Input
		/************************************************************************/
		/*if (getKeyDown(KEY_BUTTON_X))
		{
			RecenterCameraView(170.0f);
		}
*/
		mScene.mECS->tick(deltaTime);

	//#_____________________________________________________	pCameraController->update(deltaTime);
		/************************************************************************/
		// Scene Update
		/************************************************************************/
		static float currentTime = 0.0f;
		currentTime += deltaTime * 1000.0f;

		// update camera with time
		mat4 viewMat = mPlayer1->get<FE::CameraFreeComponent>()->getViewMatrix(); //pCameraController->getViewMatrix();

		const float aspectInverse = (float)m_gr.m_settings.mHeight / (float)m_gr.m_settings.mWidth;
		const float horizontal_fov = PI / 2.0f;
		mat4 projMat = mat4::perspective(horizontal_fov, aspectInverse, 0.1f, 1000.0f);
	//	gUniformData.mProjectView = projMat * viewMat;

		for each (auto& camera in m_gr.m_cameras)
		{
			camera->update(deltaTime, viewMat, projMat);
		}
		for each (auto& items in m_gr.m_ISceneItems)
		{
			items->update(deltaTime, viewMat, projMat);
		}


		// update planet transformations
		for (int i = 0; i < gNumPlanets; i++)
		{
			mat4 rotSelf, rotOrbitY, rotOrbitZ, trans, scale, parentMat;
			rotSelf = rotOrbitY = rotOrbitZ = trans = scale = parentMat = mat4::identity();
			/*if (gPlanetInfoData[i].mRotationSpeed > 0.0f)
				rotSelf = mat4::rotationY(gRotSelfScale * (currentTime + gTimeOffset) / gPlanetInfoData[i].mRotationSpeed);
			if (gPlanetInfoData[i].mYOrbitSpeed > 0.0f)
				rotOrbitY = mat4::rotationY(gRotOrbitYScale * (currentTime + gTimeOffset) / gPlanetInfoData[i].mYOrbitSpeed);
			if (gPlanetInfoData[i].mZOrbitSpeed > 0.0f)
				rotOrbitZ = mat4::rotationZ(gRotOrbitZScale * (currentTime + gTimeOffset) / gPlanetInfoData[i].mZOrbitSpeed);
			if (gPlanetInfoData[i].mParentIndex > 0)
				parentMat = gPlanetInfoData[gPlanetInfoData[i].mParentIndex].mSharedMat;*/

			/*trans = gPlanetInfoData[i].mTranslationMat;
			scale = gPlanetInfoData[i].mScaleMat;*/

			/*gPlanetInfoData[i].mSharedMat = parentMat * rotOrbitY * trans;*/
		//	gUniformData.mToWorldMat[i] = parentMat * rotOrbitY * rotOrbitZ * trans * rotSelf * scale;
			//gUniformData.mColor[i] = gPlanetInfoData[i].mColor;
		}

	
		/************************************************************************/
		/************************************************************************/

		m_gr.update(deltaTime);

	}

	void Draw()
	{
		m_gr.draw();
	}

	tinystl::string GetName()
	{
		return "01_Transformations";
	}



	void RecenterCameraView(float maxDistance, vec3 lookAt = vec3(0))
	{
		vec3 p = pCameraController->getViewPosition();
		vec3 d = p - lookAt;

		float lenSqr = lengthSqr(d);
		if (lenSqr > (maxDistance * maxDistance))
		{
			d *= (maxDistance / sqrtf(lenSqr));
		}

		p = d + lookAt;
		pCameraController->moveTo(p);
		pCameraController->lookAt(lookAt);
	}
	
};

DEFINE_APPLICATION_MAIN(UserInterface)