#pragma once

#include "../core/EngineConfig.h"
#include "../ecs/ECS.h"

namespace FE
{
	class FE_API FreeLookCameraSystem : public EntitySystem
	{
	public:
		void tick(World* world, float deltaTime);
	};
}