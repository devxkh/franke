#include "FreeLookCameraSystem.h"

#include "../components/CameraFree.h"

namespace FE
{
	void FreeLookCameraSystem::tick(World* world, float deltaTime)
	{
		world->each<CameraFreeComponent>([&](Entity* ent, ComponentHandle<CameraFreeComponent> camera) {

			vec3 test(0,20,0);
			//camera->viewPosition += camera->velocity * deltaTime;
			camera->viewPosition = test;
			camera->velocity = vec3{ 0 };
		});
	}
}