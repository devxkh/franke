#pragma once

#include "../middleware/input_fe/src/InputSystem.h"



namespace FE
{
	class Entity;
	struct BoolEvent;
	struct FloatEvent;
	class InputSystem;

	enum FE_API InputEvent {

		// ------ mandatory eventids used by system --------
		UNKNOWN,

		// Exit application
		EXIT,

		//	Maps to left click of mouse 
		POINT_MOVE,
		POINT_MOVEDX,
		POINT_MOVEDY,
		TOGGLE_FULLSCREEN,



		//---------------------------------------
		//--------- Custom Events ---------------
		//---------------------------------------

		//	Maps to right click of mouse 
		POINT_MOVE2,

		// ---------- event counter ---------------
		COUNT
	};



	struct FE_API ControllerComponent
	{
	public:

		ControllerComponent(FE::InputSystem* inputsystem);

		~ControllerComponent();

		void onPointMoveStart(const BoolEvent& boolEvent);
		void onPointMoved(const FloatEvent& floatEvent);

		void setBasicInputEvents();

		void createActionmap();

		Entity* mControlledEntity;

		gainput::InputMap* mFloatInputMap;
		gainput::InputMap* mBoolInputMap;
		EventListener* mEventListeners;
		EventListener* mBoolEventListeners;

		InputSystem* mInputsystem;


	//	NetMsg::PlayerState state;
	};

} // ns XET
