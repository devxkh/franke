#pragma once

#include "../../core/EngineConfig.h"

#define _USE_MATH_DEFINES
#include <math.h>
#include "../../src/thirdparty/theforge/Common_3/OS/Math/MathTypes.h"

#include "../middleware/input_fe/src/InputSystem.h"
#include "../components/Controller.h"
#include <iostream>

namespace FE
{
#if !defined(__ANDROID__) && !defined(TARGET_IOS)
	static float k_mouseRotationSpeed = 0.01f;
#endif

	static const float k_mouseTranslationScale = 0.05f;
	static const float k_rotationSpeed = 0.003f;
	static const float k_xRotLimit = (float)(M_PI_2 - 0.1);

	struct FE_API CameraMotionParameters
	{
		float maxSpeed;
		float acceleration; // only used with binary inputs such as keypresses
		float braking; // also acceleration but orthogonal to the acceleration vector
	};

	class FE_API CameraFreeComponent
	{

	public:

		CameraFreeComponent::CameraFreeComponent(InputSystem* inputSystem) :
			viewRotation{ 0 },
			viewPosition{ 0 },
			velocity{ 0 },
			maxSpeed{ 1.0f },
			mInputSystem(inputSystem),
			mMoveCamera(false)
		{

		}

		vec3 moveVec{ 0 };

		void CameraFreeComponent::pointMoved(float x, float y) {
			std::cout << "x: " << x << " y: " << y << std::endl;

			if (!mMoveCamera)
				return;

			float deltaTime = 0.1;

			//Windows Fall Creators Update breaks this camera controller
			//  So only use this controller if we are running on macOS or before Fall Creators Update
			float rx = viewRotation.getX();
			float ry = viewRotation.getY();
			float interpolant = 1.75f;

			float newRx = rx;
			float newRy = ry;

			newRx = rx + y * 10; //(x * k_mouseRotationSpeed) * interpolant;
			newRy = ry + (x * -1) * 10; //+ (y * k_mouseRotationSpeed) * interpolant;
		//}

		//set new target view to interpolate to
			viewRotation = { newRx, newRy };


			//divide by length to normalize if necessary
			float lenS = lengthSqr(moveVec);
			//one reason the check with > 1.0 instead of 0.0 is to avoid
			//normalizing when joystick is not fully down.
			if (lenS > 1.0f)
				moveVec /= sqrtf(lenS);

			//create rotation matrix
			mat4 rot{ mat4::rotationYX(viewRotation.getY(), viewRotation.getX()) };


			vec3 accelVec = (rot * moveVec).getXYZ();
			//divide by length to normalize if necessary
			//this determines the directional of acceleration, should be normalized.
			lenS = lengthSqr(accelVec);
			if (lenS > 1.0f)
				accelVec /= sqrtf(lenS);

			// the acceleration vector should still be unit length.
			//assert(fabs(1.0f - lengthSqr(accelVec)) < 0.001f);
			float currentInAccelDir = dot(accelVec, velocity);
			if (currentInAccelDir < 0)
				currentInAccelDir = 0;

			vec3 braking = (accelVec * currentInAccelDir) - velocity;
			float brakingLen = length(braking);
			if (brakingLen > (deceleration * deltaTime))
			{
				braking *= deceleration / brakingLen;
			}
			else
			{
				braking /= deltaTime;
			}

			accelVec = (accelVec * acceleration) + braking;
			vec3 newVelocity = velocity + (accelVec * deltaTime);
			float nvLen = lengthSqr(newVelocity);
			if (nvLen > (maxSpeed * maxSpeed))
			{
				nvLen = sqrtf(nvLen);
				newVelocity *= (maxSpeed / nvLen);
			}

			moveVec = ((velocity + newVelocity) * .5f) * deltaTime;
		//TODO	viewPosition += moveVec;
			velocity = newVelocity;

			moveVec = { 0,0,0 };
		}

		void CameraFreeComponent::moveTo(const vec3& location)
		{
			viewPosition = location;
		}

		void CameraFreeComponent::lookAt(const vec3& lookAt)
		{
			vec3 lookDir = normalize(lookAt - viewPosition);

			float y = lookDir.getY();
			viewRotation.setX(-asinf(y));

			float x = lookDir.getX();
			float z = lookDir.getZ();
			float n = sqrtf((x*x) + (z*z));
			if (n > 0.01f)
			{
				// don't change the Y rotation if we're too close to vertical
				x /= n;
				z /= n;
				viewRotation.setY(atan2f(x, z));
			}
		}

		mat4 CameraFreeComponent::getViewMatrix() const
		{
			mat4 r{ mat4::rotationXY(-viewRotation.getX(), -viewRotation.getY()) };
			vec4 t = r * vec4(-viewPosition, 1.0f);
			r.setTranslation(t.getXYZ());
			return r;
		}

		void CameraFreeComponent::setMotionParameters(const CameraMotionParameters& cmp)
		{
			maxSpeed = cmp.maxSpeed;
		}

		InputSystem* mInputSystem;

		vec2 viewRotation;//We put viewRotation at first becuase viewPosition is 16 bytes aligned. We have vtable pointer 8 bytes + vec2(8 bytes). This avoids unnecessary padding.
		vec3 viewPosition;
		vec3 velocity;
		float maxSpeed;
		float acceleration;
		float deceleration;
		bool mMoveCamera;
	};
}