#include "Controller.h"


#include "CameraFree.h"
#include "../ecs/ECS.h"

namespace FE
{
	ControllerComponent::ControllerComponent(FE::InputSystem* inputsystem)
		:mInputsystem(inputsystem),
		mControlledEntity(NULL)
	{
		mBoolInputMap = mInputsystem->AddBoolInputMap();
		mFloatInputMap = mInputsystem->AddFloatInputMap();
		mEventListeners = mInputsystem->AddEventListeners();
		mBoolEventListeners = mInputsystem->AddBoolEventListeners();
	}

	ControllerComponent::~ControllerComponent() {

	}

	void ControllerComponent::createActionmap() {

		auto& keyboardDeviceId = mInputsystem->mKeyboardDeviceID;
		auto& mouseDeviceID = mInputsystem->mMouseDeviceID;

		mBoolInputMap->MapBool(EXIT, keyboardDeviceId, gainput::KeyEscape);
		mBoolInputMap->MapBool(POINT_MOVE, mouseDeviceID, gainput::MouseButton0);
		mBoolInputMap->MapBool(TOGGLE_FULLSCREEN, keyboardDeviceId, gainput::Key1);
		mBoolInputMap->MapBool(POINT_MOVE2, mouseDeviceID, gainput::MouseButton1);

		/*	gainput::SimultaneouslyDownGesture* sdg = mInputsystem->mInputManager.CreateAndGetDevice<gainput::SimultaneouslyDownGesture>();
			GAINPUT_ASSERT(sdg);
			sdg->AddButton(mouseDeviceID, gainput::MouseButtonLeft);
			sdg->AddButton(keyboardDeviceId, gainput::KeyShiftL);
			imap.MapBool(11, sdg->GetDeviceId(), gainput::SimultaneouslyDownTriggered);

			gainput::HoldGesture* hg = mInputsystem->mInputManager.CreateAndGetDevice<gainput::HoldGesture>();
			hg->Initialize(keyboardDeviceId, gainput::Key6, true, 1000);
			imap.MapBool(12, hg->GetDeviceId(), gainput::HoldTriggered);
*/

		mFloatInputMap->MapFloat(POINT_MOVEDX, mouseDeviceID, gainput::MouseAxisX);
		mFloatInputMap->MapFloat(POINT_MOVEDY, mouseDeviceID, gainput::MouseAxisY);

	}

	void ControllerComponent::onPointMoved(const FloatEvent& floatEvent)
	{
		if (mControlledEntity && mControlledEntity->has<CameraFreeComponent>())
		{
			ComponentHandle<CameraFreeComponent> camera = mControlledEntity->get<CameraFreeComponent>();
			camera->pointMoved(floatEvent.deltax, floatEvent.deltay);
		}

	}

	void ControllerComponent::onPointMoveStart(const BoolEvent& boolEvent)
	{
		if (mControlledEntity && mControlledEntity->has<CameraFreeComponent>())
		{
			ComponentHandle<CameraFreeComponent> camera = mControlledEntity->get<CameraFreeComponent>();
			if(boolEvent.active)
				camera->mMoveCamera = true; 
			if (boolEvent.pressed)
				camera->mMoveCamera = false;
		}

	}
	void ControllerComponent::setBasicInputEvents()
	{

		//controller.system.connect(FE::POINT_START, [this](XE::ActionContext context) { onPointStart(context); });
		mEventListeners->AddFloatHandler(POINT_MOVEDX, [&](const FloatEvent& floatEvent) {	onPointMoved(floatEvent); });
		mEventListeners->AddFloatHandler(POINT_MOVEDY, [&](const FloatEvent& floatEvent) {	onPointMoved(floatEvent); });
		mBoolEventListeners->AddBoolHandler(POINT_MOVE, [&](const BoolEvent& boolEvent) {	onPointMoveStart(boolEvent); });
	}
}