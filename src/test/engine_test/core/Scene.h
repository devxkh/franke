#pragma once


#include <cstdint>
#include "../ecs/ECS.h"
#include <string>
#include "../thirdparty/theforge/Common_3/ThirdParty/OpenSource/TinySTL/unordered_map.h"
#include "components/Body.h"

#include "../../../core/IApp.h"

#include "../middleware/renderer_fe/src/GraphicsRenderer.h"
#include "../middleware/renderer_fe/src/Renderable.h"
#include "../middleware/renderer_fe/src/RenderableScene.h"
#include "../middleware/renderer_fe/src/Mesh.h"
#include "../../../middleware/resource_fe/src/GLFTHandler.h"

//using namespace FE;
//
//struct Position
//{
//	Position(float x, float y) : x(x), y(y) {}
//	Position() : x(0.f), y(0.f) {}
//
//	float x;
//	float y;
//};
//
//struct Rotation
//{
//	Rotation(float angle) : angle(angle) {}
//	Rotation() : angle(0) {}
//	float angle;
//};
//
//
//class GravitySystem : public EntitySystem
//{
//public:
//	GravitySystem(float amount)
//	{
//		gravityAmount = amount;
//	}
//
//	virtual ~GravitySystem() {}
//
//	virtual void tick(World* world, float deltaTime) override
//	{
//		world->each<Position>([&](Entity* ent, ComponentHandle<Position> position) {
//			position->y += gravityAmount * deltaTime;
//		});
//	}
//
//private:
//	float gravityAmount;
//};
namespace FE
{
	class GraphicsRenderer;

	class Scene
	{
	public:
		Scene(IApp& app, GraphicsRenderer& gr)
			: mApp(app),
			mGraphicsRenderer(gr)
		{

			mECS = World::createWorld();
			//world->registerSystem(new GravitySystem(-9.8f));

			//Entity* ent = world->create();
			//ent->assign<Position>(0.f, 0.f); // assign() takes arguments and passes them to the constructor
			//ent->assign<Rotation>(35.f);

			//world->tick(deltaTime);


		}

		~Scene()
		{
			mECS->destroyWorld();
		}

		void load(uint16 id) {

			//position comes from flatbuffer json

			auto tile = mECS->create();
			auto body = tile->assign<BodyComponent>();

			auto renderable = std::unique_ptr<Renderable>(new Renderable());
			
			//create scene object
			auto rc = std::unique_ptr<RenderableScene>(new RenderableScene(renderable.get()));
			rc->id = 0;
			mApp.mResourceCache.add(std::move(rc));

			mGraphicsRenderer.m_ISceneItems.push_back(std::move(renderable)); // TODO per multlithreaded queue

			mApp.mResourceCache.get<RenderableScene>(ResourceType::RT_Scene,0);
			//create scene mesh object
		//	auto rMesh = mApp.mResourceCache.resource<FE::Mesh>(FE::ResourceType::RT_Scene, id);

			
			//load gltf file data into scene object
	//		GLTFHandler meshloader;
//			meshloader.PrintInfo("F:/Projekte/coop/_neu/franke/data/assets/BoxInterleaved.glb", mApp.mResourceCache);
		}

		GraphicsRenderer& mGraphicsRenderer;
		World* mECS;
		IApp& mApp;
	};

}