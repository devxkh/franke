#include "RenderableScene.h"


#include "../../../thirdparty/theforge/Common_3/Renderer/IRenderer.h"

//#include "../../../thirdparty/theforge/Common_3/OS/Interfaces/IMemoryManager.h"

#include "../../../thirdparty/theforge/Common_3/OS/Interfaces/IMemoryManager.h"

namespace
{
	
}

namespace FE
{

	RenderableScene::RenderableScene(Renderable* renderable)
		: mRenderable(renderable)
	{
		IResource::type = ResourceType::RT_Scene;
	}


	void RenderableScene::load(uint32_t id)
	{
	
		
	}

	void RenderableScene::update(float deltaTime) {

	}

	void RenderableScene::transformCallback(const TransformData& data) {
		
		// If we are handed a matrix, just apply that, otherwise break down into scale, rotate, translate
		// and generate the matrix from those..
		if (data.hasMatrix)
		{
			mat4 mat;
			mat[0][0] = data.matrix[0];
			mat[0][1] = data.matrix[1];
			mat[0][2] = data.matrix[2];
			mat[0][3] = data.matrix[3];
			mat[0][4] = data.matrix[4];
			mat[0][5] = data.matrix[5];
			mat[0][6] = data.matrix[6];
			mat[0][7] = data.matrix[7];
			mat[0][8] = data.matrix[8];
			mat[0][9] = data.matrix[9];
			mat[0][10] = data.matrix[10];
			mat[0][11] = data.matrix[11];
			mat[0][12] = data.matrix[12];
			mat[0][13] = data.matrix[13];
			mat[0][14] = data.matrix[14];
			mat[0][15] = data.matrix[15];

			mRenderable->mTranslationMat = mat;

			/*{
				data.matrix[0],
				data.matrix[1],
				data.matrix[2],
				data.matrix[3],
				data.matrix[4],
				data.matrix[5],
				data.matrix[6],
				data.matrix[7],
				data.matrix[8],
				data.matrix[9],
				data.matrix[10],
				data.matrix[11],
				data.matrix[12],
				data.matrix[13],
				data.matrix[14],
				data.matrix[15]
			};*/

			//	XMStoreFloat4x4(&BufferManager::Instance().MVPBuffer().BufferData().model, XMLoadFloat4x4(&mat));
		}
		else
		{
			//	_scale = { data.scale[0], data.scale[1], data.scale[2] };
			//	_translation = { data.translation[0], data.translation[1], data.translation[2] };

			//	// Using the conversion from right-handed coordinate system of OpenGL to left-handed coordinate
			//	// system of DirectX
			//	// q.x, q.y, -q.z, -q.w
			//	//
			//	_rotation = { data.rotation[0], data.rotation[1], -data.rotation[2], -data.rotation[3] };

			//	//XMVECTOR scale = { 1.0, 1.0, 1.0 };
			//	//XMVECTOR translation = { 0.0, 0.0, 0.0 };

			//	XMVECTOR ypr = { 0.0, 180.0, 0.0 };
			//	// generate a quaternion from angle for testing...
			//	XMVECTOR rotQuat = XMQuaternionRotationRollPitchYawFromVector(ypr);

			//	//auto matrix = XMMatrixRotationQuaternion(quat);

			//	// Create matrix from scale
			//	auto matrix = XMMatrixAffineTransformation(XMLoadFloat3(&_scale), XMLoadFloat3(&emptyVector), XMLoadFloat4(&_rotation), XMLoadFloat3(&_translation));

			//	// Prepare to pass the updated model matrix to the shader 
			//	XMStoreFloat4x4(&BufferManager::Instance().MVPBuffer().BufferData().model, matrix);
		}

	};

	void RenderableScene::sceneNodeCallback(const SceneNodeData& data) {
		if (data.isMesh)
		{
			mRenderable->pMeshes[data.nodeIndex] = conf_placement_new<MeshData>(conf_malloc(sizeof(MeshData)));
			currentNodeIndex = data.nodeIndex;
		}
	};

	void RenderableScene::textureCallback(const TextureData& data) {

		Texture pTextureSkybox;

	};

	void RenderableScene::materialCallback(const MaterialData& data) {

		data.MaterialName;

	};

	void RenderableScene::bufferCallback(const BufferData& data) {


		BufferLoadDesc bufferDesc = {};
		bufferDesc.mDesc.mMemoryUsage = RESOURCE_MEMORY_USAGE_GPU_ONLY;
		bufferDesc.mDesc.mSize = data.desc.ByteWidth;//sizeof(Vertex) * meshData->mVertexCount;
		bufferDesc.pData = data.subresource.pSysMem;

		MeshData* meshData = mRenderable->pMeshes[currentNodeIndex];

		if (data.desc.BufferContentType == std::string("POSITION") ||
			data.desc.BufferContentType == std::string("NORMAL") ||
			data.desc.BufferContentType == std::string("TEXCOORD_0"))
		{
			bufferDesc.mDesc.mDescriptors = DESCRIPTOR_TYPE_VERTEX_BUFFER;

			bufferDesc.mDesc.mVertexStride = data.desc.StructureByteStride;// sizeof(Vertex);

			bufferDesc.ppBuffer = &meshData->pVertexBuffer;
			addResource(&bufferDesc);

			meshData->mVertexCount = data.desc.Count;
		}
		else if (data.desc.BufferContentType == std::string("INDICES"))
		{
			bufferDesc.mDesc.mDescriptors = DESCRIPTOR_TYPE_INDEX_BUFFER;
			bufferDesc.mDesc.mFlags = BUFFER_CREATION_FLAG_OWN_MEMORY_BIT;
			bufferDesc.mDesc.mIndexType = INDEX_TYPE_UINT32;
			bufferDesc.ppBuffer = &meshData->pIndexBuffer;
			addResource(&bufferDesc);

			meshData->mIndexCount = data.desc.Count;
		}

	//	mRenderable->pMeshes.insert(mRenderable->pMeshes.end(), meshData);
	};

}
