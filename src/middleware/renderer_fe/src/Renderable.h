#pragma once 

#include "../../../core/EngineConfig.h"


#include "../../../thirdparty/theforge/Common_3/ThirdParty/OpenSource/TinySTL/vector.h"
#include "../../../thirdparty/theforge/Common_3/Renderer/IRenderer.h"
#include "../../../core/rendering/ISceneRenderable.h"
#include "RendererConfig.h"

#include <unordered_map>

#define MAX_PLANETS 20 // Does not affect test, just for allocating space in uniform block. Must match with shader.


namespace FE
{
	class GraphicsRenderer;

	struct UniformBlockPlane
	{
		mat4 mProjectView;
		mat4 mToWorldMat;
	};

	struct UniformBlock
	{
		mat4 mProjectView;
		mat4 mToWorldMat[MAX_PLANETS];
		vec4 mColor[MAX_PLANETS];

		// Point Light Information
		vec3 mLightPosition;
		vec3 mLightColor;
	};

	typedef struct MeshData
	{
		Buffer* pVertexBuffer = NULL;
		uint	mVertexCount = 0;
		Buffer* pIndexBuffer = NULL;
		uint	mIndexCount = 0;
	} MeshData;


	struct Renderable : ISceneRenderable {

	public:
		Renderable();

	//	DescriptorBinder* pDescriptorBinderPlane = NULL;
		Buffer*           pPlaneVertexBuffer = NULL;

		const char *Name;
		int nodeIndex;
		int parentIndex;

		mat4  mTranslationMat;
		mat4  mScaleMat;
		mat4  mSharedMat;    // Matrix to pass down to children
		//--------------------------------------------------------------------------------------------
		// MESHES
		//--------------------------------------------------------------------------------------------
		tinystl::unordered_map< uint8_t, MeshData*> pMeshes;

	//	UniformBlock		gUniformData;
	//	UniformBlock		gUniformDataSky;

		RootSignature*	  pRootSignature = NULL;
		Buffer*			 pVertexBuffer = NULL;
		Pipeline*		  pPipeline = NULL;
		RasterizerState* pRasterizerState = NULL;
		DepthState*		 pDepth = NULL;
		Sampler*			pSampler = NULL;
		tinystl::vector<Shader*> shaders;


		DescriptorBinder* pDescriptorBinderPlane = NULL;

		//--------------------------------------------------------------------------------------------
		// RENDERING PIPELINE DATA
		//--------------------------------------------------------------------------------------------
		Buffer*			pUniformBuffer[gImageCount] = { NULL };

		void updateUniformBuffer() { }
		void drawUniformBuffer(GraphicsRenderer* gr, Cmd* cmd);
		bool load() { return true; }
		bool init() { return true; }

		//--------------------------------------------------------------------------------------------
		// INIT FUNCTIONS
		//--------------------------------------------------------------------------------------------
		void CreateRasterizerStates(Renderer* pRenderer);
		void CreateDepthStates(Renderer* pRenderer);

		void CreateBlendStates() { } //TODO

		void CreateSamplers(Renderer* pRenderer);

		void CreateShaders(Renderer* pRenderer);

		void CreateRootSignatures(Renderer* pRenderer);

		void CreatePBRMaps() { }
		void LoadModels() { }
		void LoadTextures() { }
		void CreateResources() { }
		void CreateUniformBuffers();

		void InitializeUniformBuffers() { }

		//--------------------------------------------------------------------------------------------
		// LOAD FUNCTIONS
		//--------------------------------------------------------------------------------------------
		void CreatePipelines(Renderer* pRenderer, SwapChain*	pSwapChain, RenderTarget* pRenderTargetDepth);
		void DestroyPipelines() { }


		void DestroyUniformBuffers() { }
		void DestroyResources() { }
		void DestroyTextures() { }
		void DestroyModels() { }
		void DestroyPBRMaps() { }

		void DestroyRootSignatures() { }
		void DestroyShaders() { }

		void DestroySamplers() { }
		void DestroyBlendStates() { }
		void DestroyDepthStates() { }
		void DestroyRasterizerStates() { }

		void update(float deltatime, mat4& viewMat, mat4& projMat);
	};

}