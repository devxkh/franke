#pragma once

#include "../../../thirdparty/theforge/Common_3/Renderer/IRenderer.h"
#include "../../../thirdparty/theforge/Common_3/ThirdParty/OpenSource/TinySTL/vector.h"

#include "../../../core/EngineConfig.h"
#include "RendererConfig.h"

#define MAX_PLANETS 20 // Does not affect test, just for allocating space in uniform block. Must match with shader.




namespace FE
{
	class GraphicsRenderer;

	struct UniformBlockCamera
	{
		mat4 mProjectView;
		mat4 mToWorldMat[MAX_PLANETS];
		vec4 mColor[MAX_PLANETS];

		// Point Light Information
		vec3 mLightPosition;
		vec3 mLightColor;
	};

	struct PlanetInfoStruct
	{
		uint  mParentIndex;
		vec4  mColor;
		float mYOrbitSpeed;    // Rotation speed around parent
		float mZOrbitSpeed;
		float mRotationSpeed;    // Rotation speed around self
		mat4  mTranslationMat;
		mat4  mScaleMat;
		mat4  mSharedMat;    // Matrix to pass down to children
	};

	class FE_API Camera {
	public:
		
		GraphicsRenderer* m_grenderer;

		UniformBlockCamera		gUniformData;
		UniformBlockCamera		gUniformDataSky;

		RootSignature*	  pRootSignature = NULL;
		Shader*			 pSkyBoxDrawShader = NULL;
		Buffer*			 pSkyBoxVertexBuffer = NULL;
		Pipeline*		   pSkyBoxDrawPipeline = NULL;


		const uint     gNumPlanets = 11;        // Sun, Mercury -> Neptune, Pluto, Moon
		const int      gSphereResolution = 30;    // Increase for higher resolution spheres
		const float    gSphereDiameter = 0.5f;
		int              gNumberOfSpherePoints;

		const uint     gTimeOffset = 600000;    // For visually better starting locations
		const float    gRotSelfScale = 0.0004f;
		const float    gRotOrbitYScale = 0.001f;
		const float    gRotOrbitZScale = 0.00001f;


		DescriptorBinder* pSkyBoxDescriptorBinder = NULL;
		DescriptorBinder* pPlanetsDescriptorBinder = NULL;

		PlanetInfoStruct gPlanetInfoData[11];
		Shader*   pSphereShader = NULL;
		Buffer*   pSphereVertexBuffer = NULL;
		Pipeline* pSpherePipeline = NULL;

		RasterizerState* pSphereRast = NULL;

		Sampler*			pSamplerSkyBox = NULL;
		Texture*			pSkyBoxTextures[6];
		DepthState*		 pDepth = NULL;

		RasterizerState*	pSkyboxRast = NULL;


		Camera(const tinystl::string& name, GraphicsRenderer* gr);

		bool init();
		void update(float deltatime, mat4& viewMat, mat4& projMat);
		void draw();
		bool Load();
		void Unload();
		void CreateUniformBuffers();
		void updateUniformBuffer();
		void drawUniformBuffer();
		void CreateShaders();
		void CreateSamplers();
		void CreateRootSignatures(); 
		void LoadTextures(); 
		void CreateRasterizerStates();
		void CreateResources();

		//LOAD
		void CreatePipelines();

		tinystl::string					m_name;

		//--------------------------------------------------------------------------------------------
		// RENDERING PIPELINE DATA
		//--------------------------------------------------------------------------------------------
		Buffer* 		pProjViewUniformBuffer[gImageCount] = { NULL };
		Buffer*			pSkyboxUniformBuffer[gImageCount] = { NULL };

		//--------------------------------------------------------------------------------------------
		// UNIFORM DATA
		//--------------------------------------------------------------------------------------------
		UniformBlockCamera		gUniformDataCamera;
		UniformBlockCamera		gUniformDataCameraSkybox;


	};

}