#pragma once 

#include "../../../core/IResource.h"
#include "../../../core/EngineConfig.h"


#include "../../../thirdparty/theforge/Common_3/Renderer/IRenderer.h"



#include <unordered_map>
#include <assert.h>


#include <fstream>
#include <sstream>



namespace FE
{

	class FE_API Mesh : public IResource {

	public:
		Mesh();

		void load(uint32_t id);


	};

}