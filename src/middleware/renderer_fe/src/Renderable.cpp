#include "Renderable.h"

#include "../../../thirdparty/theforge/Common_3/Renderer/ResourceLoader.h"

//#include "../../../thirdparty/theforge/Common_3/OS/Interfaces/IMemoryManager.h"

#include "GraphicsRenderer.h"

namespace
{
	
}

namespace FE
{

	UniformBlockPlane gUniformDataPlane;

	Renderable::Renderable()
	{
		//Generate plane vertex buffer
		float planePoints[] = { -10.0f, 0.0f, -10.0f, 1.0f, 0.0f, 0.0f, -10.0f, 0.0f, 10.0f,  1.0f, 1.0f, 0.0f,
								10.0f,  0.0f, 10.0f,  1.0f, 1.0f, 1.0f, 10.0f,  0.0f, 10.0f,  1.0f, 1.0f, 1.0f,
								10.0f,  0.0f, -10.0f, 1.0f, 0.0f, 1.0f, -10.0f, 0.0f, -10.0f, 1.0f, 0.0f, 0.0f };

		uint64_t       planeDataSize = 6 * 6 * sizeof(float);
		BufferLoadDesc planeVbDesc = {};
		planeVbDesc.mDesc.mDescriptors = DESCRIPTOR_TYPE_VERTEX_BUFFER;
		planeVbDesc.mDesc.mMemoryUsage = RESOURCE_MEMORY_USAGE_GPU_ONLY;
		planeVbDesc.mDesc.mSize = planeDataSize;
		planeVbDesc.mDesc.mVertexStride = sizeof(float) * 6;
		planeVbDesc.pData = planePoints;
		planeVbDesc.ppBuffer = &pPlaneVertexBuffer;
		addResource(&planeVbDesc);
	}

	void Renderable::CreateRasterizerStates(Renderer* pRenderer) {

		RasterizerStateDesc rasterizerStateDesc = {};
		rasterizerStateDesc.mCullMode = CULL_MODE_FRONT;
		addRasterizerState(pRenderer, &rasterizerStateDesc, &pRasterizerState);
	}

	void Renderable::CreateDepthStates(Renderer* pRenderer)
	{
		DepthStateDesc depthStateDesc = {};
		depthStateDesc.mDepthTest = true;
		depthStateDesc.mDepthWrite = true;
		depthStateDesc.mDepthFunc = CMP_LEQUAL;
		addDepthState(pRenderer, &depthStateDesc, &pDepth);
	}

	void Renderable::CreateSamplers(Renderer* pRenderer) {
		SamplerDesc samplerDesc = {
			FILTER_LINEAR, FILTER_LINEAR, MIPMAP_MODE_NEAREST,
			ADDRESS_MODE_CLAMP_TO_EDGE, ADDRESS_MODE_CLAMP_TO_EDGE, ADDRESS_MODE_CLAMP_TO_EDGE
		};
		addSampler(pRenderer, &samplerDesc, &pSampler);
	}

	void Renderable::CreateShaders(Renderer* pRenderer) {

		// per shader
		{
			Shader*			 pShader = NULL;
			ShaderLoadDesc basicShader = {};

			basicShader.mStages[0] = { "plane.vert", NULL, 0, FSR_SrcShaders };
			basicShader.mStages[1] = { "plane.frag", NULL, 0, FSR_SrcShaders };

			addShader(pRenderer, &basicShader, &pShader);

			shaders.push_back(pShader);
		}
	}

	void Renderable::CreateRootSignatures(Renderer* pRenderer) {

		const char* pStaticSamplers[] = { "uSampler0" };
		RootSignatureDesc rootDesc = {};
		rootDesc.mStaticSamplerCount = 1;
		rootDesc.ppStaticSamplerNames = pStaticSamplers;
		rootDesc.ppStaticSamplers = &pSampler;
		rootDesc.mShaderCount = shaders.size();
		rootDesc.ppShaders = shaders.data();
		addRootSignature(pRenderer, &rootDesc, &pRootSignature);

		DescriptorBinderDesc descriptorBinderDescPlane = { pRootSignature, 0, 1 };
		addDescriptorBinder(pRenderer, &descriptorBinderDescPlane, &pDescriptorBinderPlane);
	}

	void Renderable::CreateUniformBuffers() {

		BufferLoadDesc ubDesc = {};
		ubDesc.mDesc.mDescriptors = DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		ubDesc.mDesc.mMemoryUsage = RESOURCE_MEMORY_USAGE_CPU_TO_GPU;
		ubDesc.mDesc.mSize = sizeof(UniformBlockPlane);
		ubDesc.mDesc.mFlags = BUFFER_CREATION_FLAG_PERSISTENT_MAP_BIT;
		ubDesc.pData = NULL;

		for (uint32_t i = 0; i < gImageCount; ++i)
		{
			ubDesc.ppBuffer = &pUniformBuffer[i];
			addResource(&ubDesc);
		}
		finishResourceLoading();
	}

	void Renderable::update(float deltatime, mat4& viewMat, mat4& projMat) {
		/************************************************************************/
		// Plane
		/************************************************************************/
		gUniformDataPlane.mProjectView = viewMat * projMat;
		gUniformDataPlane.mToWorldMat = mat4::identity();
	}

	void Renderable::drawUniformBuffer(GraphicsRenderer* gr, Cmd* cmd) {
		
		BufferUpdateDesc planeViewProjCbv = { pUniformBuffer[gr->gFrameIndex], &gUniformDataPlane };
		updateResource(&planeViewProjCbv);

		cmdBeginDebugMarker(cmd, 1, 0, 1, "Draw Plane");
		cmdBindPipeline(cmd, pPipeline);

		DescriptorData params[1] = {};
		params[0].pName = "uniformBlock";
		params[0].ppBuffers = &pUniformBuffer[gr->gFrameIndex];

		cmdBindDescriptors(cmd, pDescriptorBinderPlane, 1, params);
		cmdBindVertexBuffer(cmd, 1, &pPlaneVertexBuffer, NULL);
		cmdDraw(cmd, 6, 0);
		cmdEndDebugMarker(cmd);
		// DRAW THE GROUND
		//
		//cmdBeginDebugMarker(cmd, 1, 0, 1, "Draw Renderable");
		//
		//cmdBindPipeline(cmd, pPipeline);


		//cmdBindVertexBuffer(cmd, 1,&pMeshes[1]->pVertexBuffer , NULL); // TODO all meshes
		//cmdBindIndexBuffer(cmd, pMeshes[1]->pIndexBuffer, NULL);
		//
		//DescriptorData params[7] = {};
		////shadowParams[0].pName = "cbObject";
		////shadowParams[0].ppBuffers = &pUniformBufferGroundPlane;	// TODO
		////cmdBindDescriptors(cmd, pDescriptorBinderShadowPass, 1, shadowParams);

		//params[0].ppBuffers = &pUniformBuffer[gr->gFrameIndex];
		//cmdBindDescriptors(cmd, pRootSignature, 1, params);


		//cmdDrawIndexed(cmd, pMeshes[1]->mIndexCount, 0, 0);

		//cmdEndDebugMarker(cmd);

	}

	void Renderable::CreatePipelines(Renderer* pRenderer, SwapChain*	pSwapChain, RenderTarget* pRenderTargetDepth) {

		//layout and pipeline for sphere draw
		VertexLayout vertexLayout = {};
		vertexLayout.mAttribCount = 2;
		vertexLayout.mAttribs[0].mSemantic = SEMANTIC_POSITION;
		vertexLayout.mAttribs[0].mFormat = ImageFormat::RGB32F;
		vertexLayout.mAttribs[0].mBinding = 0;
		vertexLayout.mAttribs[0].mLocation = 0;
		vertexLayout.mAttribs[0].mOffset = 0;
		vertexLayout.mAttribs[1].mSemantic = SEMANTIC_NORMAL;
		vertexLayout.mAttribs[1].mFormat = ImageFormat::RGB32F;
		vertexLayout.mAttribs[1].mBinding = 0;
		vertexLayout.mAttribs[1].mLocation = 1;
		vertexLayout.mAttribs[1].mOffset = 3 * sizeof(float);

		////////GraphicsPipelineDesc pipelineSettings = { 0 };
		////////pipelineSettings.mPrimitiveTopo = PRIMITIVE_TOPO_TRI_LIST;
		////////pipelineSettings.mRenderTargetCount = 1;
		////////pipelineSettings.pDepthState = NULL; // pDepth;
		////////pipelineSettings.pColorFormats = &pSwapChain->ppSwapchainRenderTargets[0]->mDesc.mFormat;
		////////pipelineSettings.pSrgbValues = &pSwapChain->ppSwapchainRenderTargets[0]->mDesc.mSrgb;
		////////pipelineSettings.mSampleCount = pSwapChain->ppSwapchainRenderTargets[0]->mDesc.mSampleCount;
		////////pipelineSettings.mSampleQuality = pSwapChain->ppSwapchainRenderTargets[0]->mDesc.mSampleQuality;
		////////pipelineSettings.mDepthStencilFormat = pRenderTargetDepth->mDesc.mFormat;
		////////pipelineSettings.pRootSignature = pRootSignature;

		////////pipelineSettings.pShaderProgram = shaders[0];

		////////pipelineSettings.pVertexLayout = &vertexLayout;

		////////pipelineSettings.pRasterizerState = pRasterizerState;
		////////addPipeline(pRenderer, &pipelineSettings, &pPipeline);

				//layout and pipeline for skeleton draw


		PipelineDesc desc = {};
		desc.mType = PIPELINE_TYPE_GRAPHICS;
		GraphicsPipelineDesc& pipelineSettings = desc.mGraphicsDesc;
		pipelineSettings.mPrimitiveTopo = PRIMITIVE_TOPO_TRI_LIST;
		pipelineSettings.mRenderTargetCount = 1;
		pipelineSettings.pDepthState = pDepth;
		pipelineSettings.pColorFormats = &pSwapChain->ppSwapchainRenderTargets[0]->mDesc.mFormat;
		pipelineSettings.pSrgbValues = &pSwapChain->ppSwapchainRenderTargets[0]->mDesc.mSrgb;
		pipelineSettings.mSampleCount = pSwapChain->ppSwapchainRenderTargets[0]->mDesc.mSampleCount;
		pipelineSettings.mSampleQuality = pSwapChain->ppSwapchainRenderTargets[0]->mDesc.mSampleQuality;
		pipelineSettings.mDepthStencilFormat = pRenderTargetDepth->mDesc.mFormat;
		pipelineSettings.pRootSignature = pRootSignature;
		//pipelineSettings.pShaderProgram = pSkeletonShader;
		pipelineSettings.pVertexLayout = &vertexLayout;
		//pipelineSettings.pRasterizerState = pSkeletonRast;
		//addPipeline(pRenderer, &desc, &pSkeletonPipeline);

		// Update the mSkeletonPipeline pointer now that the pipeline has been loaded
		//gSkeletonBatcher.LoadPipeline(pSkeletonPipeline);

		//layout and pipeline for plane draw
		vertexLayout = {};
		vertexLayout.mAttribCount = 2;
		vertexLayout.mAttribs[0].mSemantic = SEMANTIC_POSITION;
		vertexLayout.mAttribs[0].mFormat = ImageFormat::RGBA32F;
		vertexLayout.mAttribs[0].mBinding = 0;
		vertexLayout.mAttribs[0].mLocation = 0;
		vertexLayout.mAttribs[0].mOffset = 0;
		vertexLayout.mAttribs[1].mSemantic = SEMANTIC_TEXCOORD0;
		vertexLayout.mAttribs[1].mFormat = ImageFormat::RG32F;
		vertexLayout.mAttribs[1].mBinding = 0;
		vertexLayout.mAttribs[1].mLocation = 1;
		vertexLayout.mAttribs[1].mOffset = 4 * sizeof(float);

		pipelineSettings.pDepthState = NULL;
		pipelineSettings.pRasterizerState = pRasterizerState;
		pipelineSettings.pShaderProgram = shaders[0];
		addPipeline(pRenderer, &desc, &pPipeline);

	}


}
