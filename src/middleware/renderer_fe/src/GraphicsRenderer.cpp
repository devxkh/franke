#include "GraphicsRenderer.h"


#include "../../../thirdparty/theforge/Common_3/Renderer/ResourceLoader.h"
//#include "../../../thirdparty/theforge/Common_3/OS/Core/DebugRenderer.h"
#include "../../../thirdparty/theforge/Common_3/OS/Interfaces/ILogManager.h"
#include "../../../thirdparty/theforge/Common_3/Renderer/GpuProfiler.h"

#include "../../../core/rendering/ISceneRenderable.h"

// NOTE: Make sure this is the last include in a .cpp file!
#include "../../../thirdparty/theforge/Common_3/OS/Interfaces/IMemoryManager.h"

namespace FE
{
	GraphicsRenderer::GraphicsRenderer(const tinystl::string& name)
		: m_name(name)
	{

	}

	bool GraphicsRenderer::init() {

		// window and renderer setup
		RendererDesc settings = { 0 };
		initRenderer(m_name, &settings, &pRenderer);
		//check for init success
		if (!pRenderer)
			return false;

		QueueDesc queueDesc = {};
		queueDesc.mType = CMD_POOL_DIRECT;
		addQueue(pRenderer, &queueDesc, &pGraphicsQueue);
		addCmdPool(pRenderer, pGraphicsQueue, false, &pCmdPool);
		addCmd_n(pCmdPool, false, gImageCount, &ppCmds);

		for (uint32_t i = 0; i < gImageCount; ++i)
		{
			addFence(pRenderer, &pRenderCompleteFences[i]);
			addSemaphore(pRenderer, &pRenderCompleteSemaphores[i]);
		}
		addSemaphore(pRenderer, &pImageAcquiredSemaphore);

		initResourceLoaderInterface(pRenderer, DEFAULT_MEMORY_BUDGET, true);
	//	initDebugRendererInterface(pRenderer, "TitilliumText/TitilliumText-Bold.otf", FSR_Builtin_Fonts);


		addGpuProfiler(pRenderer, pGraphicsQueue, &pGpuProfiler);


		// Figure out the max font size for the current configuration
		uint32 uiMaxFrontSize = uint32(UIMaxFontSize::UI_MAX_FONT_SIZE_512);

		// Add and initialize the fontstash
		pFontStash = conf_placement_new<Fontstash>(conf_calloc(1, sizeof(Fontstash)), this->pRenderer, (int)uiMaxFrontSize, (int)uiMaxFrontSize);

		uint32_t fontID = (uint32_t)pFontStash->defineFont("default", "TitilliumText/TitilliumText-Bold.ttf", FSR_Builtin_Fonts);
		ASSERT(fontID != -1);

		for each (auto& camera in m_cameras)
		{
			camera->CreateRasterizerStates();
			//camera->createdepth
			//camera->blendstate
			camera->CreateSamplers();
			camera->CreateShaders();
			camera->CreateRootSignatures();

			camera->LoadTextures();
			camera->CreateResources();
			camera->CreateUniformBuffers();


			//camera->InitializeUniformBuffers
		}
		
		finishResourceLoading();
		/*Plane* test = conf_placement_new<Plane>(conf_calloc(1, sizeof(Plane)));
		test->LoadModels();*/

	/*	CreateRasterizerStates();
		CreateDepthStates();
		CreateBlendStates();
		CreateSamplers();

		CreateShaders();
		CreateRootSignatures();

		CreatePBRMaps();
		LoadModels();
		LoadTextures();
		CreateResources();
		CreateUniformBuffers();*/

	//	finishResourceLoading();

		//InitializeUniformBuffers();


	//	m_gr.initRenderables();
	}	
	
	void GraphicsRenderer::update(float deltatime) {
		

		for each (auto& renderable in m_ISceneItems)
		{
			if (!renderable->isLoaded)
			{
				renderable->CreateUniformBuffers();

				renderable->CreateRasterizerStates(pRenderer);
				renderable->CreateDepthStates(pRenderer);
				renderable->CreateSamplers(pRenderer);
				renderable->CreateShaders(pRenderer);
				renderable->CreateRootSignatures(pRenderer);
				renderable->CreatePipelines(pRenderer, pSwapChain, pRenderTargetDepth);
				renderable->isLoaded = !renderable->isLoaded;
			}
		}
		finishResourceLoading();
	}
	
	void GraphicsRenderer::draw() {
		acquireNextImage(pRenderer, pSwapChain, pImageAcquiredSemaphore, NULL, &gFrameIndex);


		for each (auto& camera in m_cameras)
			camera->updateUniformBuffer();

		for each (auto& renderable in m_ISceneItems)
			renderable->updateUniformBuffer();

		// Stall if CPU is running "Swap Chain Buffer Count" frames ahead of GPU
		Fence* pNextFence = pRenderCompleteFences[gFrameIndex];
		FenceStatus fenceStatus;
		getFenceStatus(pRenderer, pNextFence, &fenceStatus);
		if (fenceStatus == FENCE_STATUS_INCOMPLETE)
			waitForFences(pRenderer, 1, &pNextFence);

		RenderTarget* pRenderTarget = pSwapChain->ppSwapchainRenderTargets[gFrameIndex];

		Semaphore* pRenderCompleteSemaphore = pRenderCompleteSemaphores[gFrameIndex];
		Fence* pRenderCompleteFence = pRenderCompleteFences[gFrameIndex];

		// simply record the screen cleaning command
		LoadActionsDesc loadActions = {};
		loadActions.mLoadActionsColor[0] = LOAD_ACTION_CLEAR;
		loadActions.mClearColorValues[0].r = 1.0f;
		loadActions.mClearColorValues[0].g = 1.0f;
		loadActions.mClearColorValues[0].b = 0.0f;
		loadActions.mClearColorValues[0].a = 0.0f;
		loadActions.mLoadActionDepth = LOAD_ACTION_CLEAR;
		loadActions.mClearDepth.depth = 1.0f;
		loadActions.mClearDepth.stencil = 0;

		Cmd* cmd = ppCmds[gFrameIndex];
		beginCmd(cmd);

		TextureBarrier barriers[] = {
			{ pRenderTarget->pTexture, RESOURCE_STATE_RENDER_TARGET },
			{ pRenderTargetDepth->pTexture, RESOURCE_STATE_DEPTH_WRITE },
		};
		cmdResourceBarrier(cmd, 0, NULL, 2, barriers, false);

		cmdBindRenderTargets(cmd, 1, &pRenderTarget, pRenderTargetDepth, &loadActions, NULL, NULL, -1, -1);
		cmdSetViewport(cmd, 0.0f, 0.0f, (float)pRenderTarget->mDesc.mWidth, (float)pRenderTarget->mDesc.mHeight, 0.0f, 1.0f);
		cmdSetScissor(cmd, 0, 0, pRenderTarget->mDesc.mWidth, pRenderTarget->mDesc.mHeight);

		for each (auto& camera in m_cameras)
			camera->drawUniformBuffer();

		for each (auto& renderable in m_ISceneItems)
			renderable->drawUniformBuffer(this, cmd);




		cmdBeginDebugMarker(cmd, 0, 1, 0, "Draw UI");
		static HiresTimer gTimer;
		gTimer.GetUSec(true);

		//drawDebugText(cmd, 8, 15, tinystl::string::format("CPU %f ms", gTimer.GetUSecAverage() / 1000.0f), &gFrameTimeDraw);
		
		//gAppUI.Draw(cmd);
		
		cmdBindRenderTargets(cmd, 0, NULL, NULL, NULL, NULL, NULL, -1, -1);
		cmdEndDebugMarker(cmd);

		barriers[0] = { pRenderTarget->pTexture, RESOURCE_STATE_PRESENT };
		cmdResourceBarrier(cmd, 0, NULL, 1, barriers, true);
		endCmd(cmd);

		queueSubmit(pGraphicsQueue, 1, &cmd, pRenderCompleteFence, 1, &pImageAcquiredSemaphore, 1, &pRenderCompleteSemaphore);
		queuePresent(pGraphicsQueue, pSwapChain, gFrameIndex, 1, &pRenderCompleteSemaphore);

	}

	bool GraphicsRenderer::Load()
	{
		if (!addSwapChain())
			return false;

		if (!addDepthBuffer())
			return false;

		m_settings.mWidth = pSwapChain->ppSwapchainRenderTargets[0]->mDesc.mWidth;
		m_settings.mHeight = pSwapChain->ppSwapchainRenderTargets[0]->mDesc.mHeight;


		for each (auto& camera in m_cameras)
			camera->CreatePipelines();

		for each (auto& renderable in m_ISceneItems)
			renderable->load();		

	}

	void GraphicsRenderer::Unload()
	{
		waitForFences(pRenderer, gImageCount, pRenderCompleteFences);

		for each (auto& camera in m_cameras)
			camera->Unload();

	/*	for each (auto& renderable in m_ISceneItems)
			renderable->unload();
		*/
		
		removeSwapChain(pRenderer, pSwapChain);
		removeRenderTarget(pRenderer, pRenderTargetDepth);
	}


	void  GraphicsRenderer::toggleVSync()
	{
		waitForFences(pRenderer, gImageCount, pRenderCompleteFences);
		::toggleVSync(pRenderer, &pSwapChain);
	}

	bool GraphicsRenderer::addSwapChain()
	{
		SwapChainDesc swapChainDesc = {};
		swapChainDesc.pWindow = pWindow;
		swapChainDesc.mPresentQueueCount = 1;
		swapChainDesc.ppPresentQueues = &pGraphicsQueue;
		swapChainDesc.mWidth = m_settings.mWidth;
		swapChainDesc.mHeight = m_settings.mHeight;
		swapChainDesc.mImageCount = gImageCount;
		swapChainDesc.mSampleCount = SAMPLE_COUNT_1;
		swapChainDesc.mColorFormat = getRecommendedSwapchainFormat(true);
		swapChainDesc.mEnableVsync = false;
		::addSwapChain(pRenderer, &swapChainDesc, &pSwapChain);

		return pSwapChain != NULL;
	}

	bool GraphicsRenderer::addDepthBuffer()
	{
		// Add depth buffer
		RenderTargetDesc depthRT = {};
		depthRT.mArraySize = 1;
		depthRT.mClearValue.depth = 1.0f;
		depthRT.mClearValue.stencil = 0;
		depthRT.mDepth = 1;
		depthRT.mFormat = ImageFormat::D32F;
		depthRT.mHeight = m_settings.mHeight;
		depthRT.mSampleCount = SAMPLE_COUNT_1;
		depthRT.mSampleQuality = 0;
		depthRT.mWidth = m_settings.mWidth;
		addRenderTarget(pRenderer, &depthRT, &pRenderTargetDepth);

		return pRenderTargetDepth != NULL;
	}
}