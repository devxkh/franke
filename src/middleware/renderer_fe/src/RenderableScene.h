#pragma once 

#include "../../../core/rendering/IRenderableScene.h"
#include "../../../core/rendering/ISceneAssetData.h"
#include "../../../core/IApp.h"
#include "../../../core/EngineConfig.h"
#include "Renderable.h"

#include "../../../thirdparty/theforge/Common_3/Renderer/ResourceLoader.h"


#include <unordered_map>
#include <string>


namespace FE
{
	/* Render static scene
	*/
	class FE_API RenderableScene : public IRenderableScene {

	public:

		int currentNodeIndex = -1;
		Renderable* mRenderable;

		RenderableScene(Renderable* renderable);
			   		
		void load(uint32_t id);

		void update(float deltaTime);

		void transformCallback(const TransformData& data);

		void sceneNodeCallback(const SceneNodeData& data);

		void textureCallback(const TextureData& data);

		void materialCallback(const MaterialData& data);

		void bufferCallback(const BufferData& data);

	};

}