#pragma once


//Renderer
#include "../../../thirdparty/theforge/Common_3/Renderer/IRenderer.h"
#include "../../../thirdparty/theforge/Common_3/ThirdParty/OpenSource/TinySTL/vector.h"
#include "../../../thirdparty/theforge/Middleware_3/Text/Fontstash.h"

#include "../../../core/EngineConfig.h"
#include "../../../core/rendering/ISceneRenderable.h"
#include "Camera.h"
#include "RendererConfig.h"
#include <memory>
#include <vector>
class GpuProfiler;

namespace FE
{
//	class ISceneRendererable;

	enum UIMaxFontSize
	{
		UI_MAX_FONT_SIZE_UNDEFINED = 0, // Undefined size, will defaults to use UI_MAX_FONT_SIZE_512
		UI_MAX_FONT_SIZE_128 = 128,  // Max font size is 12.8f
		UI_MAX_FONT_SIZE_256 = 256,  // Max font size is 25.6f
		UI_MAX_FONT_SIZE_512 = 512,  // Max font size is 51.2f
		UI_MAX_FONT_SIZE_1024 = 1024	// Max font size is 102.4f
	};

	struct UniformCamData
	{
		mat4 mProjectView;
		vec3 mCamPos;
	};

	class Uncopyable {
	public:
		Uncopyable() {}

	private:
		Uncopyable(const Uncopyable&);
		Uncopyable& operator=(const Uncopyable&);
	};


	class FE_API GraphicsRenderer : Uncopyable {

	public:

	
		GraphicsRenderer(const tinystl::string& name);

		bool init();
		void update(float deltatime);
		void draw();
		bool Load();
		void Unload();

		tinystl::string					m_name;

		bool addSwapChain();
		bool addDepthBuffer();

		void toggleVSync();
		
		////--------------------------------------------------------------------------------------------
		//// INIT FUNCTIONS
		////--------------------------------------------------------------------------------------------
		//void CreateRasterizerStates();
		//void CreateDepthStates();
		//void CreateBlendStates();
		//void CreateSamplers();

		//void CreateShaders();
		//void CreateRootSignatures();

		//void CreatePBRMaps();
		//void LoadModels();
		//void LoadTextures();
		//void CreateResources();
		//void CreateUniformBuffers();

		//void InitializeUniformBuffers();

		////--------------------------------------------------------------------------------------------
		//// LOAD FUNCTIONS
		////--------------------------------------------------------------------------------------------
		//void CreatePipelines();
		//void DestroyPipelines();

		//	   
		//void DestroyUniformBuffers();
		//void DestroyResources();
		//void DestroyTextures();
		//void DestroyModels();
		//void DestroyPBRMaps();

		//void DestroyRootSignatures();
		//void DestroyShaders();

		//void DestroySamplers();
		//void DestroyBlendStates();
		//void DestroyDepthStates();
		//void DestroyRasterizerStates();





		uint32_t			gFrameIndex = 0;

		struct Settings
		{
			/// Window width
			int32_t mWidth = -1;
			/// Window height
			int32_t mHeight = -1;
			/// Set to true if fullscreen mode has been requested
			bool	mFullScreen = false;
			/// Set to true if app wants to use an external window
			bool	mExternalWindow = false;
		} m_settings;

		Fontstash*								  pFontStash;


	//	TextDrawDesc gFrameTimeDraw = TextDrawDesc(0, 0xff00ffff, 18);

		std::vector< std::unique_ptr <ISceneRenderable> >	m_ISceneItems;
		std::vector< std::unique_ptr <Camera> >	m_cameras;
	
		GpuProfiler*		pGpuProfiler = NULL;
		WindowsDesc* pWindow;
		//--------------------------------------------------------------------------------------------
		// RENDERING PIPELINE DATA
		//--------------------------------------------------------------------------------------------
		Renderer*						pRenderer = NULL;
		Queue*							pGraphicsQueue = NULL;
		CmdPool*						pCmdPool = NULL;
		Cmd**							ppCmds = NULL;
		SwapChain*						pSwapChain = NULL;
		CmdPool*						pUICmdPool = NULL;
		Cmd**							ppUICmds = NULL; 
		RenderTarget*					pRenderTargetDepth = NULL;
		Semaphore*						pImageAcquiredSemaphore = NULL;

		Fence* 			pRenderCompleteFences[gImageCount] = { NULL };
		Semaphore* 		pRenderCompleteSemaphores[gImageCount] = { NULL };

	};

}