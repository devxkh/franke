#pragma once

#include "../../../thirdparty/theforge/Common_3/Renderer/IRenderer.h"

namespace FE
{		
	

	class Skybox {
	public:
		bool init(Renderer* pRenderer);
		bool Load();
		void Unload();

		RootSignature*	  pRootSignature = NULL;
		Shader*			 pSkyBoxDrawShader = NULL;
		Buffer*			 pSkyBoxVertexBuffer = NULL;
		Pipeline*		   pSkyBoxDrawPipeline = NULL;

		Sampler*			pSamplerSkyBox = NULL;
		Texture*			pSkyBoxTextures[6];

		RasterizerState*	pSkyboxRast = NULL;
	};

}