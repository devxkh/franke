
message("/src/middleware/network_fe start")

add_library(network_fe STATIC 
			src/YojimboMiddleware.cpp	
			src/YojimboMiddleware.h

			${DetailSourcFiles}
			)
			
set_target_properties(network_fe PROPERTIES LINKER_LANGUAGE CXX)
			
#
# add lib to solution folder 
#
set_property(TARGET network_fe PROPERTY FOLDER "1_middleware")


# add dependencies
target_link_libraries(network_fe 
			engine_core)
					   			 
			
# add header search folders
# include_directories(${PROJECT_SOURCE_DIR}/src/thirdparty/SDL/include)

#
# ----- set preprocessor constants ------------
#
#  target_compile_definitions(input_fe  
#  )