#pragma once 

#include "../../../core/IMiddleware.h"


class YojimboMiddleware : public FE::IMiddleware {

public:
	YojimboMiddleware();

	void init();

	void update(float delta);

	void destroy();
};
