#pragma once

#include "../../../core/EngineConfig.h"
#include "../../../core/IInputSystem.h"
#include "../../../thirdparty/gainput/lib/include/gainput/gainput.h"

#ifdef METAL
#ifdef TARGET_IOS
#include "../../Common_3/ThirdParty/OpenSource/gainput/lib/include/gainput/GainputIos.h"
#else
#include "../../Common_3/ThirdParty/OpenSource/gainput/lib/include/gainput/GainputMac.h"
#endif
#endif

#include <memory>
#include <vector>
#include <unordered_map>
#include <functional>
#include <iostream>

namespace FE
{
	struct FloatEvent
	{
		float x;
		float y;
		float deltax;
		float deltay;
	//	WindowDesc wndDesc;
	};
	struct BoolEvent
	{
		bool active;
		bool pressed;
	//	WindowDesc wndDesc;
	};
	class FE_API EventListener {
	public:
		typedef const std::function<void(const FloatEvent&)> floathandler_t;

		void AddFloatHandler(const uint16_t trigger, floathandler_t& h) {

			mFloatHandlerList.insert(std::make_pair(trigger, std::move(h)));

			/*handler_t handler = handlerList[2];
			handler(11);*/
		}

		typedef const std::function<void(const BoolEvent&)> boolhandler_t;

		void AddBoolHandler(const uint16_t trigger, boolhandler_t& h) {

			mBoolHandlerList.insert(std::make_pair(trigger, std::move(h)));

			/*handler_t handler = handlerList[2];
			handler(11);*/
		}

		/*void DoStuff() {

			handler_t handler = handlerList[2];
			handler(11);
		}*/

		std::unordered_map<uint16_t, floathandler_t> mFloatHandlerList;
		std::unordered_map<uint16_t, boolhandler_t> mBoolHandlerList;

	};

	class FE_API InputSystem : public IInputSystem 
	{
	public:

		uint32_t mActiveInputMap;
	//	std::vector<gainput::InputMap*> mInputMaps;

		bool isMouseCaptured() { return mIsMouseCaptured; };

		void setPointCapture(bool mouseCapture) {
			mIsMouseCaptured = mouseCapture;
#if defined(METAL) && !defined(TARGET_IOS)
			GainputMacInputView * view = (__bridge GainputMacInputView *)(pGainputView);
			[view SetMouseCapture : mouseCapture];
			view = NULL;
#endif
		}

	private:
		//Event listener for when a button's state changes
		//Will be triggered on event down, event up or event move.
		class MyDeviceButtonListener : public gainput::InputListener
		{
		public:
		
			MyDeviceButtonListener(InputSystem& inputSystem, gainput::InputManager& inputManager, int index) : inputSystem_(inputSystem), inputManager_(inputManager), index_(index) { }

			bool OnDeviceButtonBool(gainput::DeviceId deviceId, gainput::DeviceButtonId deviceButton, bool oldValue, bool newValue)
			{
				const gainput::InputDevice* device = inputManager_.GetDevice(deviceId);
				
			
				/*	if (inputSystem_.mInputMaps[inputSystem_.mActiveInputMap]->GetBool(i))
					{
						std::cout << "____________________ test_______________" << std::endl;
					}
			*/
				

				
				auto boolfloatcount = device->GetButtonType(deviceButton);

				char buttonName[64] = "";
				device->GetButtonName(deviceButton, buttonName, 64);

				for each (auto& iMap in inputSystem_.mBoolInputMaps)
				{
					
						BoolEvent boolEvent;
						boolEvent.active = iMap->GetBool(POINT_MOVE);
						boolEvent.pressed = iMap->GetBoolWasDown(POINT_MOVE);

						//iterate listeners from all controllers
						for each (auto& listener in inputSystem_.mBoolEventlisteners)
						{
							if (listener->mBoolHandlerList.find(POINT_MOVE) == listener->mBoolHandlerList.end()) {
								// not found
							}
							else {
								// found
								listener->mBoolHandlerList[POINT_MOVE](boolEvent);
							}
						}
					
				}

				std::cout << "("<< index_ << ") Device " << deviceId << " ("<< device->GetTypeName() << device->GetIndex() << ") bool button (" << deviceButton << buttonName << ") changed: " << oldValue << " -> " << newValue  << std::endl;
				return false;
			}

			bool OnDeviceButtonFloat(gainput::DeviceId deviceId, gainput::DeviceButtonId deviceButton, float oldValue, float newValue)
			{
				const gainput::InputDevice* device = inputManager_.GetDevice(deviceId);
				char buttonName[64] = "";
				device->GetButtonName(deviceButton, buttonName, 64);
				std::cout << "(" << index_ << ") Device " << deviceId << " (" << device->GetTypeName() << device->GetIndex() << ") float button (" << deviceButton << buttonName << ") changed: " << oldValue << " -> " << newValue << std::endl;
				//	SFW_LOG("(%d) Device %d (%s%d) float button (%d/%s) changed: %f -> %f\n", index_, deviceId, device->GetTypeName(), device->GetIndex(), deviceButton, buttonName, oldValue, newValue);

				// iterate all possible Events
			//	for (uint16_t eventId = 1; eventId != COUNT; eventId++)
			//	{
					//iterate all controller maps
					for each (auto& iMap in inputSystem_.mFloatInputMaps)
					{
						float deltax = iMap->GetFloatDelta(POINT_MOVEDX);
						float deltay = iMap->GetFloatDelta(POINT_MOVEDY);
						if (deltax != 0.0f || deltay != 0.0f)
						{
							FloatEvent floatEvent;
							floatEvent.x = iMap->GetFloat(POINT_MOVEDX);
							floatEvent.y = iMap->GetFloat(POINT_MOVEDY);

							//iterate listeners from all controllers
							for each (auto& listener in inputSystem_.mEventlisteners)
							{
								//if (listener->mFloatHandlerList.find(POINT_MOVEDX) == listener->mFloatHandlerList.end()) {
								//	// not found
								//}
								//else {
									// found
									floatEvent.deltax = deltax;
									floatEvent.deltay = deltay;
									listener->mFloatHandlerList[POINT_MOVEDX](floatEvent);
							///	}
							}							
						}
					}
			//	}
				

				return true;
			}

			int GetPriority() const
			{
				return index_;
			}

		private:
			

			InputSystem& inputSystem_;
			gainput::InputManager& inputManager_;
			int index_;
		};
	public:
		InputSystem();

		~InputSystem() {

		}

		void init(uint32_t width, uint32_t height);

		void update();

		void destroy();

		bool isTriggered(uint16_t inputEventId);

		bool isActive(uint16_t inputEventId);

		void updateSize(uint32_t width, uint32_t height);

		void textInputEnabled(bool enabled);
		
		char getNextCharacter();

		void getPoint(InputPoint& point);

		
		gainput::InputMap* AddBoolInputMap() {

			std::unique_ptr<gainput::InputMap> newMap = std::unique_ptr<gainput::InputMap>(new gainput::InputMap(mInputManager));
			mBoolInputMaps.push_back(std::move(newMap));
			return mBoolInputMaps.back().get();
		}

		gainput::InputMap* AddFloatInputMap() {

			std::unique_ptr<gainput::InputMap> newMap = std::unique_ptr<gainput::InputMap>(new gainput::InputMap(mInputManager));
			mFloatInputMaps.push_back(std::move(newMap));
			return mFloatInputMaps.back().get();
		}
		
		EventListener* AddEventListeners() {

			std::unique_ptr<EventListener > newListerners = std::unique_ptr<EventListener>(new EventListener());
			mEventlisteners.push_back(std::move(newListerners));
			return mEventlisteners.back().get();
		}

		EventListener* AddBoolEventListeners() {

			std::unique_ptr<EventListener > newListerners = std::unique_ptr<EventListener>(new EventListener());
			mBoolEventlisteners.push_back(std::move(newListerners));
			return mBoolEventlisteners.back().get();
		}

		std::vector<std::unique_ptr<gainput::InputMap>> mBoolInputMaps;
		std::vector<std::unique_ptr<gainput::InputMap>> mFloatInputMaps;
		std::vector<std::unique_ptr<EventListener>> mBoolEventlisteners;
		std::vector<std::unique_ptr<EventListener>> mEventlisteners;
		

#ifdef _WINDOWS
		void handleMessage(MSG& msg);
#elif defined __ANDROID__
		int32_t handleMessage(AInputEvent* msg);
#elif defined(__linux__)
		void handleMessage(XEvent& msg);
#endif

		//all the input devices we need
		gainput::DeviceId mMouseDeviceID = gainput::InvalidDeviceId;
		gainput::DeviceId mRawMouseDeviceID = gainput::InvalidDeviceId;
		gainput::DeviceId mKeyboardDeviceID = gainput::InvalidDeviceId;
		gainput::DeviceId mGamepadDeviceID = gainput::InvalidDeviceId;
		gainput::DeviceId mTouchDeviceID = gainput::InvalidDeviceId;

		gainput::InputDeviceKeyboard* mKeyboardDevice;
		
		//required to unregister the event listener
		gainput::ListenerId mDeviceInputListnerID = -1; //max uint instead of including headers for UINT_MAX on linux.

		//system vars
		bool mIsMouseCaptured = false;
		bool mVirtualKeyboardActive = false;

		/*std::unordered_map<uint32_t, std::vector<KeyMappingDescription>> mKeyMappings;
		std::unordered_map<uint32_t, std::vector<UserToDeviceMap>> mDeviceToUserMappings;*/

	//	std::vector<InputEventHandler> mInputCallbacks;

		// gainput systems
		gainput::InputManager mInputManager;


	//	gainput::InputMap mInputMap;

		// Needed to send events
		gainput::DeviceButtonSpec mButtonsDown[32];

		//input listener to gainput mainly for triggers/releases, touch and mouse
	//	DeviceInputEventListener mDeviceInputListener;
		
		MyDeviceButtonListener myDeviceButtonListener;

	

	};
}