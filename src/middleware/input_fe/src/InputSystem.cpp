#include "InputSystem.h"

namespace FE
{
	InputSystem::InputSystem()
		:
		myDeviceButtonListener(*this,mInputManager, 1){

	}

	void InputSystem::init(uint32_t width, uint32_t height)
	{
		//default input map
		mActiveInputMap = 0;

		//Set display size
		mInputManager.SetDisplaySize(width, height);
		mIsMouseCaptured = false;
		mVirtualKeyboardActive = false;

		//default device ids
		mMouseDeviceID = gainput::InvalidDeviceId;
		mRawMouseDeviceID = gainput::InvalidDeviceId;
		mKeyboardDeviceID = gainput::InvalidDeviceId;
		mGamepadDeviceID = gainput::InvalidDeviceId;
		mTouchDeviceID = gainput::InvalidDeviceId;
		mDeviceInputListnerID = -1;

		// create all necessary devices
		// TODO: check for failure.
		mMouseDeviceID = mInputManager.CreateDevice<gainput::InputDeviceMouse>();
		mRawMouseDeviceID = mInputManager.CreateDevice<gainput::InputDeviceMouse>(mMouseDeviceID + 1, gainput::InputDeviceMouse::DV_RAW);
		mKeyboardDevice = mInputManager.CreateAndGetDevice<gainput::InputDeviceKeyboard>();
		mKeyboardDeviceID = mKeyboardDevice->GetDeviceId();
		mGamepadDeviceID = mInputManager.CreateDevice<gainput::InputDevicePad>();
		mTouchDeviceID = mInputManager.CreateDevice<gainput::InputDeviceTouch>();
		//	mDeviceInputListnerID =  mInputManager.AddListener(&mDeviceInputListener);

	
	
		gainput::ListenerId myDeviceButtonListenerId = mInputManager.AddListener(&myDeviceButtonListener);

	}


	void InputSystem::textInputEnabled(bool enabled)
	{
		mKeyboardDevice->SetTextInputEnabled(enabled);
	}

	char InputSystem::getNextCharacter() {
		return mKeyboardDevice->GetNextCharacter();
	}

	void InputSystem::getPoint(InputPoint& point)
	{
		/*if (mInputMap.GetFloatDelta(POINTX) != 0.0f || mInputMap.GetFloatDelta(POINTY) != 0.0f)
		{*/
			/*point.PosX = mInputMap.GetFloat(POINTX);
			point.PosY = mInputMap.GetFloat(POINTY);
			point.DeltaX = mInputMap.GetFloatDelta(POINTX);
			point.DeltaY = mInputMap.GetFloatDelta(POINTY);*/
	//	}
	}

	/*bool InputSystem::DeviceInputEventListener::OnDeviceButtonBool(gainput::DeviceId deviceId, gainput::DeviceButtonId deviceButton, bool oldValue, bool newValue)
	{
		return GatherInputEventButton(deviceId, deviceButton, oldValue ? 1.0f : 0.0f, newValue ? 1.0f : 0.0f);
	}



	bool InputSystem::DeviceInputEventListener::OnDeviceButtonFloat(gainput::DeviceId deviceId, gainput::DeviceButtonId deviceButton, float oldValue, float newValue)
	{
		return GatherInputEventButton(deviceId, deviceButton, oldValue, newValue);
	}
*/
	void InputSystem::update()
	{
		mInputManager.Update();
	}

	void InputSystem::destroy()
	{

	}

	bool InputSystem::isTriggered(uint16_t inputEventId)
	{
		if (mBoolInputMaps[0]->IsMapped(inputEventId) && mBoolInputMaps[0]->GetBoolWasDown(inputEventId))
			return true;	
	}

	bool InputSystem::isActive(uint16_t inputEventId)
	{
		if(mBoolInputMaps[0]->IsMapped(inputEventId) && mBoolInputMaps[0]->GetBool(inputEventId))
			return true;
	}

	void InputSystem::updateSize(uint32_t width, uint32_t height)
	{
		mInputManager.SetDisplaySize(width, height);
	}

#ifdef _WINDOWS
	void InputSystem::handleMessage(MSG& msg)
	{
		mInputManager.HandleMessage(msg);
	}
#elif defined __ANDROID__
	int32_t InputSystem::handleMessage(AInputEvent* msg)
	{
		return mInputManager.HandleInput(msg);
	}
#elif defined(__linux__)
	void InputSystem::handleMessage(XEvent& msg)
	{
		mInputManager.HandleEvent(msg);
	}
#endif

}