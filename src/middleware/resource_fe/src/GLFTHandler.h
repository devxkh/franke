#pragma once 

#include "../../../core/EngineConfig.h"

#include "../../../thirdparty/theforge/Common_3/ThirdParty/OpenSource/TinySTL/vector.h"


#include <unordered_map>
#include <assert.h>
#include <vector>


namespace FE
{
	class IRenderableScene;
	class ResourceCache;
	class IResource;

	class FE_API GLTFHandler {

	public:
		GLTFHandler();
		void load();

		void PrintInfo(const std::string& pathStr, IResource* ressource);

		std::vector<uint32_t> primIndices;
		std::vector<float> normals;
		const std::vector<float> positions;



	};

}