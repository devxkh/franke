#include "GLFTHandler.h"

#include "../../../core/EngineConfig.h"
#include "../../../core/rendering/IRenderableScene.h"
#include "../../../core/ResourceCache.h"

#include "../../../thirdparty/gltfsdk/Inc/GLTFSDK/GLTF.h"
#include "../../../thirdparty/gltfsdk/Inc/GLTFSDK/GLTFResourceReader.h"
#include "../../../thirdparty/gltfsdk/Inc/GLTFSDK/GLBResourceReader.h"
#include "../../../thirdparty/gltfsdk/Inc/GLTFSDK/Deserialize.h"

#include "../../../thirdparty/gltfsdk/Inc/GLTFSDK/MeshPrimitiveUtils.h"
#include <experimental/filesystem>

#include "SharedGLTFParser/GLBGLTFConverter.h"

#include <unordered_map>
#include <assert.h>

#include <fstream>
#include <sstream>

using namespace Microsoft::glTF;


namespace FE
{
	// The glTF SDK is decoupled from all file I/O by the IStreamReader (and IStreamWriter)
 // interface(s) and the C++ stream-based I/O library. This allows the glTF SDK to be used in
 // sandboxed environments, such as WebAssembly modules and UWP apps, where any file I/O code
 // must be platform or use-case specific.
	class StreamReader : public IStreamReader
	{
	public:
		StreamReader(std::experimental::filesystem::path pathBase) : m_pathBase(std::move(pathBase))
		{
			assert(m_pathBase.has_root_path());
		}

		// Resolves the relative URIs of any external resources declared in the glTF manifest
		std::shared_ptr<std::istream> GetInputStream(const std::string& filename) const override
		{
			// In order to construct a valid stream:
			// 1. The filename argument will be encoded as UTF-8 so use filesystem::u8path to
			//    correctly construct a path instance.
			// 2. Generate an absolute path by concatenating m_pathBase with the specified filename
			//    path. The filesystem::operator/ uses the platform's preferred directory separator
			//    if appropriate.
			// 3. Always open the file stream in binary mode. The glTF SDK will handle any text
			//    encoding issues for us.
			auto streamPath = m_pathBase / std::experimental::filesystem::u8path(filename);
			auto stream = std::make_shared<std::ifstream>(streamPath, std::ios_base::binary);

			// Check if the stream has no errors and is ready for I/O operations
			if (!stream || !(*stream))
			{
				throw std::runtime_error("Unable to create a valid input stream for uri: " + filename);
			}

			return stream;
		}

	private:
		std::experimental::filesystem::path m_pathBase;
	};

	// Uses the Document class to print some basic information about various top-level glTF entities
	void PrintDocumentInfo(const Document& document)
	{
		// Asset Info
		std::cout << "Asset Version:    " << document.asset.version << "\n";
		std::cout << "Asset MinVersion: " << document.asset.minVersion << "\n";
		std::cout << "Asset Generator:  " << document.asset.generator << "\n";
		std::cout << "Asset Copyright:  " << document.asset.copyright << "\n\n";

		// Scene Info
		std::cout << "Scene Count: " << document.scenes.Size() << "\n";

		if (document.scenes.Size() > 0U)
		{
			std::cout << "Default Scene Index: " << document.GetDefaultScene().id << "\n\n";
		}
		else
		{
			std::cout << "\n";
		}

		// Entity Info
		std::cout << "Node Count:     " << document.nodes.Size() << "\n";
		std::cout << "Camera Count:   " << document.cameras.Size() << "\n";
		std::cout << "Material Count: " << document.materials.Size() << "\n\n";

		// Mesh Info
		std::cout << "Mesh Count: " << document.meshes.Size() << "\n";
		std::cout << "Skin Count: " << document.skins.Size() << "\n\n";

		// Texture Info
		std::cout << "Image Count:   " << document.images.Size() << "\n";
		std::cout << "Texture Count: " << document.textures.Size() << "\n";
		std::cout << "Sampler Count: " << document.samplers.Size() << "\n\n";

		// Buffer Info
		std::cout << "Buffer Count:     " << document.buffers.Size() << "\n";
		std::cout << "BufferView Count: " << document.bufferViews.Size() << "\n";
		std::cout << "Accessor Count:   " << document.accessors.Size() << "\n\n";

		// Animation Info
		std::cout << "Animation Count: " << document.animations.Size() << "\n\n";

		for (const auto& extension : document.extensionsUsed)
		{
			std::cout << "Extension Used: " << extension << "\n";
		}

		if (!document.extensionsUsed.empty())
		{
			std::cout << "\n";
		}

		for (const auto& extension : document.extensionsRequired)
		{
			std::cout << "Extension Required: " << extension << "\n";
		}

		if (!document.extensionsRequired.empty())
		{
			std::cout << "\n";
		}
	}

	// Uses the Document and GLTFResourceReader classes to print information about various glTF binary resources
	void PrintResourceInfo(const Document& document, const GLTFResourceReader& resourceReader)
	{
		// Use the resource reader to get each mesh primitive's position data
		for (const auto& mesh : document.meshes.Elements())
		{
			std::cout << "Mesh: " << mesh.id << "\n";

			for (const auto& meshPrimitive : mesh.primitives)
			{
				std::string accessorId;

				const int indicesID = std::stoi(meshPrimitive.indicesAccessorId);
				const Accessor& acce = document.accessors[indicesID];
				const int bufferViewID = std::stoi(acce.bufferViewId);

				const std::vector<uint32_t> indices = resourceReader.ReadBinaryData<uint32_t>(document, document.bufferViews[bufferViewID]);

				//https://github.com/ft-lab/Shade3D_GLTFConverter/blob/master/projects/GLTFConverter/source/GLTFLoader.cpp

				if (meshPrimitive.TryGetAttributeAccessorId(ACCESSOR_POSITION, accessorId))
				{
					const Accessor& accessor = document.accessors.Get(accessorId);

					const auto data = resourceReader.ReadBinaryData<float>(document, accessor);
					const auto dataByteLength = data.size() * sizeof(float);

					std::cout << "MeshPrimitive: " << dataByteLength << " bytes of position data\n";
				}

				if (meshPrimitive.TryGetAttributeAccessorId(ACCESSOR_NORMAL, accessorId))
				{
					const Accessor& accessor = document.accessors.Get(accessorId);

					const auto data = resourceReader.ReadBinaryData<float>(document, accessor);
					const auto dataByteLength = data.size() * sizeof(float);

					std::cout << "MeshPrimitive: " << dataByteLength << " bytes of normals data\n";
				}
			}

			std::cout << "\n";
		}

		// Use the resource reader to get each image's data
		for (const auto& image : document.images.Elements())
		{
			std::string filename;

			if (image.uri.empty())
			{
				assert(!image.bufferViewId.empty());

				auto& bufferView = document.bufferViews.Get(image.bufferViewId);
				auto& buffer = document.buffers.Get(bufferView.bufferId);

				filename += buffer.uri; //NOTE: buffer uri is empty if image is stored in GLB binary chunk
			}
			else if (IsUriBase64(image.uri))
			{
				filename = "Data URI";
			}
			else
			{
				filename = image.uri;
			}

			auto data = resourceReader.ReadBinaryData(document, image);

			std::cout << "Image: " << image.id << "\n";
			std::cout << "Image: " << data.size() << " bytes of image data\n";

			if (!filename.empty())
			{
				std::cout << "Image filename: " << filename << "\n\n";
			}
		}
	}
	//https://github.com/Microsoft/glTF-DXViewer/blob/6c784cbe380cd0ce303a4d6b99ca7fa066fba432/SharedGLTFParser/GLBGLTFConverter.h

	void loadMeshInfo(const Document& document, const GLTFResourceReader& resourceReader, GLTFHandler* gltfHandler)
	{
		// Use the resource reader to get each mesh primitive's position data
		for (const auto& mesh : document.meshes.Elements())
		{
			std::cout << "Mesh: " << mesh.id << "\n";

			for (const auto& meshPrimitive : mesh.primitives)
			{

				auto& exts = meshPrimitive.extensions;

				if (exts.find("KHR_draco_mesh_compression") != exts.end())
				{
					// TODO:
					// Read the BufferView and process the attributes
					// maybe factor the processing of attributes into a function
					// taking a parameter to determine whether or not to decompress

				}



				// Load Material (incl. textures)
				//auto material = gltfDoc.materials[meshPrimitive.materialId];
				//LoadMaterialNode(parser, material);

				//// Load Index Buffer...
				//LoadBufferFromAccessorId(parser, meshPrimitive.indicesAccessorId, "INDICES");
				//LoadBufferFromAccessorId(parser, meshPrimitive.positionsAccessorId, "POSITION");
				//LoadBufferFromAccessorId(parser, meshPrimitive.normalsAccessorId, "NORMAL");
				//LoadBufferFromAccessorId(parser, meshPrimitive.tangentsAccessorId, "TANGENT");
				//LoadBufferFromAccessorId(parser, meshPrimitive.uv0AccessorId, "TEXCOORD_0");

				// TODO: Load other buffers....




				//if (meshPrimitive.TryGetAttributeAccessorId(Microsoft::glTF::ACCESSOR_POSITION, accessorId))
				//{
				//	const Microsoft::glTF::Accessor& accessor = document.accessors.Get(accessorId);

				//	//###		auto outputPositions = Microsoft::glTF::MeshPrimitiveUtils::GetPositions(document, resourceReader, accessor);
				//	//###		auto outputPositions = Microsoft::glTF::MeshPrimitiveUtils::GetIndices16(document, resourceReader, accessor);


				//	const auto positions = resourceReader->ReadBinaryData<float>(document, accessor);
				//	const auto dataByteLength = positions.size() * sizeof(float);

				//	std::cout << "MeshPrimitive: " << dataByteLength << " bytes of position data\n";

				//	const auto primIndices = resourceReader->ReadBinaryData<float>(document, document.accessors.Get(meshPrimitive.indicesAccessorId));
				//	const auto normals = resourceReader->ReadBinaryData<float>(document, document.accessors.Get(meshPrimitive.GetAttributeAccessorId(Microsoft::glTF::ACCESSOR_NORMAL)));
				//	const auto uvs = resourceReader->ReadBinaryData<float>(document, document.accessors.Get(meshPrimitive.GetAttributeAccessorId(Microsoft::glTF::ACCESSOR_TEXCOORD_0)));

				std::string accessorId;

				const int indicesID = std::stoi(meshPrimitive.indicesAccessorId);
				const Accessor& acce = document.accessors[indicesID];
				const int bufferViewID = std::stoi(acce.bufferViewId);

				//gltfHandler->primIndices = resourceReader.ReadBinaryData<uint32_t>(document, document.bufferViews[bufferViewID]);
				
				//https://github.com/ft-lab/Shade3D_GLTFConverter/blob/master/projects/GLTFConverter/source/GLTFLoader.cpp
		
				if (meshPrimitive.TryGetAttributeAccessorId(ACCESSOR_POSITION, accessorId))
				{
					const Accessor& accessor = document.accessors.Get(accessorId);

					auto positions = resourceReader.ReadBinaryData<float>(document, accessor);
					const auto dataByteLength = positions.size() * sizeof(float);

					std::cout << "MeshPrimitive: " << dataByteLength << " bytes of position data\n";

				}

				if (meshPrimitive.TryGetAttributeAccessorId(ACCESSOR_NORMAL, accessorId))
				{
					const Accessor& accessor = document.accessors.Get(accessorId);

					gltfHandler->normals = resourceReader.ReadBinaryData<float>(document, accessor);
					const auto dataByteLength = gltfHandler->normals.size() * sizeof(float);

					std::cout << "MeshPrimitive: " << dataByteLength << " bytes of normals data\n";
				}

				//per interface -> set in Mesh.cpp
		//		MeshResource.MeshLoaded(positions);

				// todo mesh prim

			}

			std::cout << "\n";
		}

	}

	GLTFHandler::GLTFHandler()
		
	{

	}

	void GLTFHandler::PrintInfo(const std::string& pathStr, IResource* resource)
	{
		std::experimental::filesystem::path path = pathStr; // "F:/Projekte/coop/_neu/franke/data/assets/BoxInterleaved.glb";// argv[1U];

		if (path.is_relative())
		{
			auto pathCurrent = std::experimental::filesystem::current_path();

			// Convert the relative path into an absolute path by appending the command line argument to the current path
			pathCurrent /= path;
			pathCurrent.swap(path);
		}

		if (!path.has_filename())
		{
			throw std::runtime_error("Command line argument path has no filename");
		}

		if (!path.has_extension())
		{
			throw std::runtime_error("Command line argument path has no filename extension");
		}

		// Pass the absolute path, without the filename, to the stream reader
		auto streamReader = std::make_unique<StreamReader>(path.parent_path());

		std::experimental::filesystem::path pathFile = path.filename();
		std::experimental::filesystem::path pathFileExt = pathFile.extension();

		std::string manifest;

		auto MakePathExt = [](const std::string& ext)
		{
			return "." + ext;
		};

		std::unique_ptr<GLTFResourceReader> resourceReader;

		// If the file has a '.gltf' extension then create a GLTFResourceReader
		if (pathFileExt == MakePathExt(GLTF_EXTENSION))
		{
			auto gltfStream = streamReader->GetInputStream(pathFile.u8string()); // Pass a UTF-8 encoded filename to GetInputString
			auto gltfResourceReader = std::make_unique<GLTFResourceReader>(std::move(streamReader));

			std::stringstream manifestStream;

			// Read the contents of the glTF file into a string using a std::stringstream
			manifestStream << gltfStream->rdbuf();
			manifest = manifestStream.str();

			resourceReader = std::move(gltfResourceReader);
		}

		// If the file has a '.glb' extension then create a GLBResourceReader. This class derives
		// from GLTFResourceReader and adds support for reading manifests from a GLB container's
		// JSON chunk and resource data from the binary chunk.
		if (pathFileExt == MakePathExt(GLB_EXTENSION))
		{
			auto glbStream = streamReader->GetInputStream(pathFile.u8string()); // Pass a UTF-8 encoded filename to GetInputString
			auto glbResourceReader = std::make_unique<GLBResourceReader>(std::move(streamReader), std::move(glbStream));

			manifest = glbResourceReader->GetJson(); // Get the manifest from the JSON chunk

			resourceReader = std::move(glbResourceReader);
		}

		if (!resourceReader)
		{
			throw std::runtime_error("Command line argument path filename extension must be .gltf or .glb");
		}

		Document document;

		try
		{
			document = Deserialize(manifest);
		}
		catch (const GLTFException& ex)
		{
			std::stringstream ss;

			ss << "Microsoft::glTF::Deserialize failed: ";
			ss << ex.what();

			throw std::runtime_error(ss.str());
		}

		std::cout << "### glTF Info - " << pathFile << " ###\n\n";

		PrintDocumentInfo(document);
		PrintResourceInfo(document, *resourceReader);
		loadMeshInfo(document, *resourceReader, this);


		FE::GLTFFileData gltfFile;
		//auto doc = DeserializeJson(jsonStr);
		auto& cb = gltfFile.EventHandlers();
		FE::GLTFFileData::ParserContext context(document, cb, *resourceReader);

		//CheckExtensions(doc);
		//gltfFile.ParseDocument(context);
		auto currentScene = context.document().GetDefaultScene();
				
		function<void(const BufferData&)> bufferCallback = [resource](const BufferData& data) {
		
			std::cout << "bufferCallback" << std::endl;
			
			//createMeshBuffer(data);
			//assignToNode();
			resource->bufferCallback(data);

		};
		function<void(const MaterialData&)> materialCallback = [resource](const MaterialData& data) {
			std::cout << "materialCallback" << std::endl;
			resource->materialCallback(data);
		};
		function<void(const TextureData&)> textureCallback = [resource](const TextureData& data) {
			std::cout << "textureCallback" << std::endl;
			resource->textureCallback(data);
		};
		function<void(const TransformData&)> transformCallback = [resource](const TransformData& data) {
			std::cout << "transformCallback" << std::endl;
			resource->transformCallback(data);
		};
		function<void(const SceneNodeData&)> sceneNodeCallback = [resource](const SceneNodeData& data) {
			std::cout << "sceneNodeCallback" << std::endl;
			
			resource->sceneNodeCallback(data);
		};


		gltfFile.EventHandlers().Buffers = [bufferCallback](const BufferData& data)
		{
			bufferCallback(data);
		};

		gltfFile.EventHandlers().Textures = [textureCallback](const TextureData& data)
		{
			textureCallback(data);
		};

		gltfFile.EventHandlers().Materials = [materialCallback](const MaterialData& data)
		{
			materialCallback(data);
		};

		gltfFile.EventHandlers().Transform = [transformCallback](const TransformData& data)
		{
			transformCallback(data);
		};

		gltfFile.EventHandlers().SceneNode = [sceneNodeCallback](const SceneNodeData& data)
		{
			sceneNodeCallback(data);
		};

		
		//	Out(L"Loading scene %S", currentScene.id.c_str());
		gltfFile.LoadScene(context, currentScene);
	}

	
	void GLTFHandler::load() {


		//PrintInfo(path, document, std::move(resourceReader));
		//PrintInfo("F:/Projekte/coop/_neu/franke/data/assets/BoxInterleaved.glb",this);

	}

}