#pragma once

#include "../../../thirdparty/gltfsdk/Inc/GLTFSDK/IStreamReader.h"
#include "../../../core/rendering/ISceneAssetData.h"

#include <iostream>
#include <functional>
#include <memory>

using namespace std;

typedef long HRESULT;

namespace FE {

	HRESULT  ParseFile(shared_ptr<istream> inStr,
		const string& baseUri,
		Microsoft::glTF::IStreamReader& gltfStreamReader,
		function<void(const BufferData&)> bufferCallback,
		function<void(const MaterialData&)> materialCallback,
		function<void(const TextureData&)> textureCallback,
		function<void(const TransformData&)> transformCallback,
		function<void(const SceneNodeData&)> sceneNodeCallback);
}