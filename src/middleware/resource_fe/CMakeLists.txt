
message("/src/middleware/resource_fe start")

add_library(resource_fe STATIC 
			src/ResourceCache.cpp					
			src/GLFTHandler.cpp						src/GLFTHandler.h
			src/SharedGLTFParser/GLBGLTFConverter.h	src/SharedGLTFParser/GLBGLTFConverter.cpp
			src/SharedGLTFParser/gltfparser.h		src/SharedGLTFParser/gltfparser.cpp
			)
			
set_target_properties(resource_fe PROPERTIES LINKER_LANGUAGE CXX)
			
#
# add lib to solution folder 
#
set_property(TARGET resource_fe PROPERTY FOLDER "2_interface_impl")


# add dependencies
target_link_libraries(resource_fe 
			engine_core)
					   		
							
#
# add header search folders
#
include_directories(${PROJECT_SOURCE_DIR}/src/thirdparty/gltfsdk/Inc)
			

#
# ----- set preprocessor constants ------------
#
target_compile_definitions(resource_fe PRIVATE 
						NO_GAINPUT
						USE_MEMORY_TRACKING 
						FE_EXPORT
)