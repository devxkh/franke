# Franke V2

Frankenstein 3D Engine (MIT) Platform independent

### Tested Systems
- Windows - x64

### Building

CMakeLists.txt

### VS 2017 C++ Edit & Continue
https://docs.microsoft.com/en-us/visualstudio/debugger/edit-and-continue-visual-cpp?view=vs-2017

## Tools
Visual Studio extensions
* [SQLite/SQL Server Compact Toolbox](https://marketplace.visualstudio.com/items?itemName=ErikEJ.SQLServerCompactSQLiteToolbox)
* [Markdown Editor](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.MarkdownEditor)